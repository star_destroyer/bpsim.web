﻿using Autofac;
using BPsim.Web.Analysis.Excel;
using BPsim.Web.Analysis.Excel.Enreachers;

namespace BPsim.Web.Analysis.Autofac
{
    public class BPsimWebAnalysisModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WorkbookFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResourceWorksheetEnreacher>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<OrderWorksheetEnreacher>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<OperationWorksheetEnreacher>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AgentWorksheetEnreacher>().AsImplementedInterfaces().SingleInstance();
        }
    }
}