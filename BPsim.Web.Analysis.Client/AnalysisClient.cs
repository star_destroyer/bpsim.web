﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace BPsim.Web.Analysis.Client
{
    public class AnalysisClient : IAnalysisClient
    {
        private readonly HttpClient client;

        public AnalysisClient(Uri uri)
        {
            client = new HttpClient {BaseAddress = uri};
        }

        public async Task<byte[]> GetExcelAsync(Guid id, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"api/v1/analysis/{id}.xlsx");
            var response = await client.SendAsync(request, cancellation).ConfigureAwait(false);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsByteArrayAsync();
        }
    }
}