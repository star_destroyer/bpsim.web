using System;
using System.Threading;
using System.Threading.Tasks;

namespace BPsim.Web.Analysis.Client
{
    public interface IAnalysisClient
    {
        Task<byte[]> GetExcelAsync(Guid id, CancellationToken cancellation);
    }
}