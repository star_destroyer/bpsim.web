﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using OfficeOpenXml;

namespace BPsim.Web.Analysis.Excel.Enreachers
{
    public class AgentWorksheetEnreacher : IWorksheetEnreacher
    {
        public string Name => "Агенты";

        public void Enreach(ExcelWorksheet worksheet, ModelContract model, TaskContract task, IEnumerable<ReportContract> reports)
        {
            var agentNodes = model.Nodes.Where(x => x.Value is AgentContract).ToDictionary(x => x.Key, x => (AgentContract) x.Value);

            var nextColumnNumber = 2;
            foreach (var agent in agentNodes)
            {
                var cellRange = worksheet.Cells[1, nextColumnNumber, 1, nextColumnNumber + agent.Value.Knowledges.Count - 1];
                cellRange.Value = agent.Key;
                cellRange.Merge = true;
                nextColumnNumber += agent.Value.Knowledges.Count;
            }

            worksheet.Cells[2, 1].Value = "Такт";

            nextColumnNumber = 2;
            foreach (var knowlegesTitle in agentNodes.SelectMany(x => x.Value.Knowledges.Keys))
            {
                worksheet.Cells[2, nextColumnNumber].Value = knowlegesTitle;
                nextColumnNumber++;
            }

            var agentKnowledgeValues = agentNodes.ToDictionary(x => x.Key, x => x.Value.Knowledges.ToDictionary(y => y.Key, y => 0));

            foreach (var report in reports)
            {
                var row = report.Tick + 2;
                worksheet.Cells[row, 1].Value = report.Tick;
                if (report.Agents != null)
                {
                    foreach (var agent in report.Agents)
                    {
                        var agentKnowledgeValue = agentKnowledgeValues[agent.Key];
                        foreach (var knowledge in agent.Value)
                        {
                            agentKnowledgeValue[knowledge.Key] += knowledge.Value;
                        }
                    }
                }

                nextColumnNumber = 2;
                foreach (var knowledgeValue in agentKnowledgeValues.SelectMany(x => x.Value))
                {
                    worksheet.Cells[row, nextColumnNumber].Value = knowledgeValue.Value;
                    nextColumnNumber++;
                }
            }
        }
    }
}