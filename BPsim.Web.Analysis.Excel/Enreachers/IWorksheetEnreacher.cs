﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using OfficeOpenXml;

namespace BPsim.Web.Analysis.Excel.Enreachers
{
    public interface IWorksheetEnreacher
    {
        string Name { get; }

        void Enreach(ExcelWorksheet worksheet, ModelContract model, TaskContract task, IEnumerable<ReportContract> reports);
    }
}