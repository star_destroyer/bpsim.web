﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using OfficeOpenXml;

namespace BPsim.Web.Analysis.Excel.Enreachers
{
    public class OperationWorksheetEnreacher : IWorksheetEnreacher
    {
        public string Name => "Оперции";

        public void Enreach(ExcelWorksheet worksheet, ModelContract model, TaskContract task, IEnumerable<ReportContract> reports)
        {
            var operationNodes = model.Nodes.Where(x => x.Value is OperationContract).ToDictionary(x => x.Key, x => x.Value);
            
            var titleRow = 2;
            foreach (var title in operationNodes.Keys)
            {
                var cellRange = worksheet.Cells[1, titleRow, 1, titleRow + 1];
                cellRange.Value = title;
                cellRange.Merge = true;
                titleRow += 2;
            }

            worksheet.Cells[2, 1].Value = "Такт";

            var subTitleRow = 2;
            foreach (var title in operationNodes)
            {
                worksheet.Cells[2, subTitleRow].Value = "Состояние";
                worksheet.Cells[2, subTitleRow + 1].Value = "Количество";
                subTitleRow += 2;
            }

            var operationValue= operationNodes.ToDictionary(x => x.Key, x => (State: 0, Count: 0));

            foreach (var report in reports)
            {
                var row = report.Tick + 2;
                worksheet.Cells[row, 1].Value = report.Tick;
                if (report.Operations != null)
                {
                    foreach (var operation in report.Operations)
                    {
                        var (state, count) = operationValue[operation.Key];
                        operationValue[operation.Key] = (State: operation.Value ? 1 : 0, Count: operation.Value ? count + 1 : count);
                    }
                }

                var dataColumn = 2;
                foreach (var orderValue in operationValue)
                {
                    worksheet.Cells[row, dataColumn].Value = orderValue.Value.State;
                    worksheet.Cells[row, dataColumn + 1].Value = orderValue.Value.Count;
                    dataColumn += 2;
                }
            }
        }
    }
}