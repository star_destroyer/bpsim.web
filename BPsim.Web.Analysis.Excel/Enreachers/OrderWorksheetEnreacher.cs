﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using OfficeOpenXml;

namespace BPsim.Web.Analysis.Excel.Enreachers
{
    public class OrderWorksheetEnreacher : IWorksheetEnreacher
    {
        public string Name => "Заявки";

        public void Enreach(ExcelWorksheet worksheet, ModelContract model, TaskContract task, IEnumerable<ReportContract> reports)
        {
            var headerColumn = 1;
            foreach (var header in new[] {"Такт"}.Concat(model.Orders.Keys))
            {
                worksheet.Cells[1, headerColumn].Value = header;
                headerColumn++;
            }

            var orderValues = model.Orders.ToDictionary(x => x.Key, x => 0);

            foreach (var report in reports)
            {
                var row = report.Tick + 1;
                worksheet.Cells[row, 1].Value = report.Tick;
                if (report.Orders != null)
                {
                    foreach (var order in report.Orders)
                    {
                        orderValues[order.Key] += order.Value;
                    }
                }

                var dataColumn = 2;
                foreach (var orderValue in orderValues)
                {
                    worksheet.Cells[row, dataColumn].Value = orderValue.Value;
                    dataColumn++;
                }
            }
        }
    }
}