﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using OfficeOpenXml;

namespace BPsim.Web.Analysis.Excel.Enreachers
{
    public class ResourceWorksheetEnreacher : IWorksheetEnreacher
    {
        public string Name => "Ресурсы";
        
        public void Enreach(ExcelWorksheet worksheet, ModelContract model, TaskContract task, IEnumerable<ReportContract> reports)
        {
            var headerColumn = 1;
            foreach (var header in new[] {"Такт"}.Concat(task.SubjectArea.Resources.Keys))
            {
                worksheet.Cells[1, headerColumn].Value = header;
                headerColumn++;
            }

            var resourceValues = task.SubjectArea.Resources.ToDictionary<KeyValuePair<string, decimal[]>, string, object>(x => x.Key, x => x.Value.First());

            foreach (var report in reports)
            {
                var row = report.Tick + 1;
                worksheet.Cells[row, 1].Value = report.Tick;
                if (report.Resources != null)
                {
                    foreach (var resource in report.Resources)
                    {
                        resourceValues[resource.Key] = resource.Value;
                    }
                }

                var resourceColumn = 2;
                foreach (var resourceValue in resourceValues)
                {
                    worksheet.Cells[row, resourceColumn].Value = resourceValue.Value;
                    resourceColumn++;
                }
            }
        }
    }
}