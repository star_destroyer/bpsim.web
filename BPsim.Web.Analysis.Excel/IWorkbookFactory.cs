﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;

namespace BPsim.Web.Analysis.Excel
{
    public interface IWorkbookFactory
    {
        Task<byte[]> CreateAsync(ModelContract model, TaskContract  task, IEnumerable<ReportContract> reports, CancellationToken cancellation);
    }
}