﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Analysis.Excel.Enreachers;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using OfficeOpenXml;

namespace BPsim.Web.Analysis.Excel
{
    public class WorkbookFactory : IWorkbookFactory
    {
        private readonly IWorksheetEnreacher[] enreachers;

        public WorkbookFactory(IWorksheetEnreacher[] enreachers)
        {
            this.enreachers = enreachers;
        }

        static WorkbookFactory()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        public async Task<byte[]> CreateAsync(ModelContract model, TaskContract task, IEnumerable<ReportContract> reports, CancellationToken cancellation)
        {
            using (var excel = new ExcelPackage())
            {
                await Task.WhenAll(enreachers.Select(x =>
                {
                    var worksheet = excel.Workbook.Worksheets.Add(x.Name);
                    return Task.Run(() => x.Enreach(worksheet, model, task, reports), cancellation);
                })).ConfigureAwait(false);

                return excel.GetAsByteArray();
            }
        }
    }
}