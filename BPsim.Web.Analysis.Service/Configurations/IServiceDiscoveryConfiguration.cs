﻿using System;

namespace BPsim.Web.Analysis.Service.Configurations
{
    public interface IServiceDiscoveryConfiguration
    {
        Uri Imitation { get; }
    }
}