﻿using System;

namespace BPsim.Web.Analysis.Service.Configurations
{
    public class ServiceDiscoveryConfiguration : IServiceDiscoveryConfiguration
    {
        public Uri Imitation { get; set; }
    }
}