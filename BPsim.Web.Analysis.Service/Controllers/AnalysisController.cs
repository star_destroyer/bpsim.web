﻿using System;
using System.Threading.Tasks;
using BPsim.Web.Analysis.Excel;
using BPsim.Web.Imitation.Client;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Analysis.Service.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]", Name = "Analysis")]
    [ApiVersion("1.0")]
    public class AnalysisController : Controller
    {
        private readonly IImitationClient imitationClient;
        private readonly IWorkbookFactory workbookFactory;

        public AnalysisController(IImitationClient imitationClient,
                                  IWorkbookFactory workbookFactory)
        {
            this.imitationClient = imitationClient;
            this.workbookFactory = workbookFactory;
        }

        /// <summary>
        ///     Create .xlsx analysis report for task
        /// </summary>
        /// <param name="taskId">The model identifier</param>
        /// <returns>
        ///     A 200 Ok - .xlsx file stream
        ///     A 400 Bad Request - the task with specified taskId can't be analysed
        ///     A 404 Not Found - the task with specified taskId doesn't exists 
        /// </returns>
        /// <response code="200">.xlsx file stream</response>
        /// <response code="400">the task with specified taskId can't be analysed</response>
        /// <response code="404">the task with specified taskId doesn't exists</response>
        [HttpGet("{taskId}.xlsx")]
        public async Task<IActionResult> Get(Guid taskId)
        {
            var task = await imitationClient.GetTaskAsync(taskId, HttpContext.RequestAborted).ConfigureAwait(false);

            if (task == null)
                return new NotFoundObjectResult($"Task with id {taskId} doesn't exists");

            if (task.Data.State == TaskStateContract.Open || task.Data.State == TaskStateContract.Pending || task.Data.State == TaskStateContract.Scheduled)
            {
                return new BadRequestObjectResult($"Task can't be analysed, becouse it is in {task.Data.State} state");
            }

            var model = await imitationClient.GetModelAsync(task.Data.ModelId, HttpContext.RequestAborted).ConfigureAwait(false);
            var reports = await imitationClient.SelectReportsAsync(taskId, HttpContext.RequestAborted).ConfigureAwait(false);
            var xlsxStream = await workbookFactory.CreateAsync(model.Data, task.Data, reports, HttpContext.RequestAborted).ConfigureAwait(false);
            return new FileContentResult(xlsxStream, "application/octet-stream") {FileDownloadName = $"{taskId}.xlsx"};
        }
    }
}