﻿using System;

namespace BPsim.Web.Analysis.Service
{
    public class Settings
    {
        public Topology Topology { get; set; }
    }
    
    public class Topology
    {
        public Uri ImitationUri { get; set; }
    }
}