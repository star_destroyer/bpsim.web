﻿using System;
using System.Linq;
using System.Net.Http;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using BPsim.Web.Analysis.Autofac;
using BPsim.Web.Analysis.Service.Configurations;
using BPsim.Web.Imitation.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace BPsim.Web.Analysis.Service
{
    public class Startup
    {
        private readonly IConfigurationRoot configuration;
        
        private Settings Settings => configuration.Get<Settings>();

        public Startup(IHostingEnvironment hostingEnvironment)
        {
            configuration = new ConfigurationBuilder()
                            .SetBasePath(hostingEnvironment.ContentRootPath)
                            .AddJsonFile("appsettings.json")
                            .AddJsonFile($"appsettings.{hostingEnvironment.EnvironmentName}.json", true)
                            .AddEnvironmentVariables()
                            .Build();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services
                .AddSingleton<IConfiguration>(configuration)
                .AddRouting(options =>
                {
                    options.AppendTrailingSlash = true;
                    options.LowercaseUrls = true;
                })
                .AddApiVersioning(x =>
                {
                    x.AssumeDefaultVersionWhenUnspecified = true;
                    x.DefaultApiVersion = new ApiVersion(1, 0);
                })
                .AddSwagger()
                .AddMvcCore()
                .AddApiExplorer()
                .AddAuthorization()
                .AddFormatterMappings()
                .AddDataAnnotations()
                .AddJsonFormatters()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                    options.SerializerSettings.Converters.Add(new NodeContractJsonConverter());
                    options.SerializerSettings.Converters.Add(new RuleContractJsonConverter());
                })
                .AddCors(options =>
                {
                    options.AddPolicy(
                                      "AllowAny",
                                      x1 => x1
                                            .AllowAnyOrigin()
                                            .AllowAnyMethod()
                                            .AllowAnyHeader());
                })
                .AddVersionedApiExplorer();

            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule<BPsimWebAnalysisModule>();

            builder.Register(x => new ImitationClient(Settings.Topology.ImitationUri))
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.Register(x => x.Resolve<IConfiguration>().GetSection("ServiceDiscovery").Get<ServiceDiscoveryConfiguration>())
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.Register(x => new HttpClient()).AsSelf().SingleInstance();
            

            return new AutofacServiceProvider(builder.Build());
        }

        public void Configure(IApplicationBuilder application)
        {
            application
                .UseCors("AllowAny")
                .UseMvc()
                .UseSwagger()
                .UseSwaggerUI(options =>
                {
                    var provider = application.ApplicationServices.GetService<IApiVersionDescriptionProvider>();
                    foreach (var apiVersionDescription in provider
                                                          .ApiVersionDescriptions
                                                          .OrderByDescending(x => x.ApiVersion))
                    {
                        options.SwaggerEndpoint(
                                                $"/swagger/{apiVersionDescription.GroupName}/swagger.json",
                                                $"Version {apiVersionDescription.ApiVersion}");
                    }
                });
        }
    }
}