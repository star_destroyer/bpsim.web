﻿using System;
using Serilog;

namespace BPsim.Web.Analysis.Service.Utils
{
    public static class LoggerExtensions
    {
        public static TimedOperation BeginTimedOperation(this ILogger logger, string messageTemplate)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            if (messageTemplate == null)
            {
                throw new ArgumentNullException(nameof(messageTemplate));
            }

            return new TimedOperation(logger, messageTemplate, Array.Empty<object>());
        }

        public static TimedOperation BeginTimedOperation(this ILogger logger, string messageTemplate, params object[] args)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            if (messageTemplate == null)
            {
                throw new ArgumentNullException(nameof(messageTemplate));
            }

            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            return new TimedOperation(logger, messageTemplate, args);
        }
    }
}