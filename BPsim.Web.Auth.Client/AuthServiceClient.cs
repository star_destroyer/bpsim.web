﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BPsim.Web.Auth.Client
{
    public class AuthServiceClient : IAuthServiceClient
    {
        private readonly HttpClient client;

        public AuthServiceClient(Uri authUri)
        {
            client = new HttpClient {BaseAddress = authUri};
        }

        public async Task<RegistrationResult> RegisterAsync(Credentials credentials)
        {
            var content = new StringContent(JsonConvert.SerializeObject(credentials), Encoding.UTF8, "application/json");
            using (var response = await client.PostAsync("user", content).ConfigureAwait(false))
            {
                var body = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                if (!response.IsSuccessStatusCode)
                {
                    return new RegistrationResult {Success = false, Message = body};
                }

                var json = JObject.Parse(body);
                return new RegistrationResult {Success = true, Id = Guid.Parse(json["id"].ToString())};
            }
        }

        public async Task<AuthorizationResult> AuthorizeAsync(Credentials credentials)
        {
            var content = new StringContent(JsonConvert.SerializeObject(credentials), Encoding.UTF8, "application/json");
            using (var response = await client.PostAsync("authorization", content).ConfigureAwait(false))
            {
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                    return new AuthorizationResult {Success = false};

                var body = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    return new AuthorizationResult {Success = false, Message = body};
                }

                response.EnsureSuccessStatusCode();

                return new AuthorizationResult {Success = true, Token = body};
            }
        }

        public async Task<User> GetAsync(string token, Guid id)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"user/{id}")
            {
                Headers = {{"Authorization", $"Bearer {token}"}}
            };
            using (var response = await client.SendAsync(request).ConfigureAwait(false))
            {
                if (response.StatusCode == HttpStatusCode.BadRequest)
                {
                    return null;
                }

                var body = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                response.EnsureSuccessStatusCode();
                return JsonConvert.DeserializeObject<User>(body);
            }
        }
    }
}