﻿namespace BPsim.Web.Auth.Client
{
    public class Credentials
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public string Validate()
        {
            if (string.IsNullOrEmpty(Login))
            {
                return "Login is empty";
            }

            if (string.IsNullOrEmpty(Password))
            {
                return "Password is empty";
            }

            return null;
        }
    }
}