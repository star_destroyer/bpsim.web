﻿using System;
using System.Threading.Tasks;

namespace BPsim.Web.Auth.Client
{
    public interface IAuthServiceClient
    {
        Task<RegistrationResult> RegisterAsync(Credentials credentials);
        Task<AuthorizationResult> AuthorizeAsync(Credentials credentials);
        Task<User> GetAsync(string token, Guid id);
    }
}