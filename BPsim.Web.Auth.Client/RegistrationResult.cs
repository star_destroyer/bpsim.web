using System;

namespace BPsim.Web.Auth.Client
{
    public class RegistrationResult
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public Guid Id { get; set; }
    }
}