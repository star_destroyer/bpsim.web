using System;

namespace BPsim.Web.Auth.Client
{
    public class User
    {
        public Guid Id { get; set; }

        public string Login { get; set; }
    }
}