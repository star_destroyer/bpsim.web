﻿using Autofac;

namespace BPsim.Web.Auth.DataAccess.Autofac
{
    public class BPsimWebAuthDataAccessModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<UserRepository>().AsImplementedInterfaces().SingleInstance();
        }
    }
}