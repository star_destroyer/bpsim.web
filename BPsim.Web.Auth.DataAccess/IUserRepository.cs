﻿using System;
using System.Threading.Tasks;

namespace BPsim.Web.Auth.DataAccess
{
    public interface IUserRepository
    {
        Task<User> GetUserAsync(Guid id);
        Task<User> GetUserAsync(string login);

        Task<User> PutUserAsync(User user);
//		Task<IEnumerable<User>> GetUsersAsync();
//		Task<User> UpdatePasswordAsync(Guid id, string passwordHash);
    }
}