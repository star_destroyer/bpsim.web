﻿using System;

namespace BPsim.Web.Auth.DataAccess
{
    public class User
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
    }
}