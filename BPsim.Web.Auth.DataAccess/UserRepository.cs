﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace BPsim.Web.Auth.DataAccess
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoDatabase database;

        public UserRepository(IMongoDatabase database)
        {
            this.database = database;
        }

        public async Task<User> GetUserAsync(Guid id)
        {
            var collection = database.GetCollection<User>("users");
            var userCursor = await collection.FindAsync(Builders<User>.Filter.Eq(u => u.Id, id));
            return await userCursor.FirstOrDefaultAsync();
        }

        public async Task<User> GetUserAsync(string login)
        {
            var collection = database.GetCollection<User>("users");
            var userCursor = await collection.FindAsync(Builders<User>.Filter.Eq(u => u.Login, login));
            return await userCursor.FirstOrDefaultAsync();
        }

        public async Task<User> PutUserAsync(User user)
        {
            var collection = database.GetCollection<User>("users");
            await collection.InsertOneAsync(user);
            return user;
        }
    }
}