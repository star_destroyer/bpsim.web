using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BPsim.Web.Auth.Client;
using BPsim.Web.Auth.DataAccess;
using BPsim.Web.Auth.Passwords;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace BPsim.Web.Auth.Controllers
{
    [Route("[controller]")]
    public class AuthorizationController : Controller
    {
        private const string signingSecurityKey = "0d5b3235a8b403c3dab9c3f4f65c07fcalskd234n1k41230"; //TODO

        private readonly IUserRepository userRepository;
        private readonly IPasswordHashProvider passwordHashProvider;

        public AuthorizationController(IUserRepository userRepository,
                                       IPasswordHashProvider passwordHashProvider)
        {
            this.userRepository = userRepository;
            this.passwordHashProvider = passwordHashProvider;
        }

        [HttpPost]
        public async Task<ActionResult> Authorize([FromBody] Credentials credentials)
        {
            var validation = credentials.Validate();
            if (validation != null)
            {
                return BadRequest(validation);
            }
            
            var user = await userRepository.GetUserAsync(credentials.Login);
            if (user == null)
            {
                return BadRequest($"User with login {credentials.Login} not found");
            }

            if (!passwordHashProvider.IsValidPassword(credentials.Password, user.PasswordHash))
            {
                return Unauthorized();
            }

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var key = Encoding.UTF8.GetBytes(signingSecurityKey);

            var token = new JwtSecurityToken(
                issuer: "BPsim.Web",
                audience: "BPsim.Web User",
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256
                )
            );

            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}