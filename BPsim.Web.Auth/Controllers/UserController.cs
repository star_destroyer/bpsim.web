﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using BPsim.Web.Auth.Client;
using BPsim.Web.Auth.DataAccess;
using BPsim.Web.Auth.Passwords;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using User = BPsim.Web.Auth.DataAccess.User;
using UserContract = BPsim.Web.Auth.Client.User;

namespace BPsim.Web.Auth.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly ILogger log;
        private readonly IPasswordHashProvider passwordHashProvider;
        private readonly IUserRepository userRepository;

        public UserController(
            IUserRepository userRepository,
            IPasswordHashProvider passwordHashProvider,
            ILoggerFactory loggerFactory)
        {
            this.userRepository = userRepository;
            this.passwordHashProvider = passwordHashProvider;
            log = loggerFactory.CreateLogger(typeof(UserController));
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] Credentials credentials)
        {
            if (credentials == null || string.IsNullOrEmpty(credentials.Login) || string.IsNullOrEmpty(credentials.Password))
            {
                return BadRequest();
            }

            var existingUser = await userRepository.GetUserAsync(credentials.Login);

            if (existingUser != null)
            {
                return BadRequest($"User with login {credentials.Login} already exists");
            }

            var passwordHash = passwordHashProvider.MakeHash(credentials.Password);
            log.LogDebug($"Password hash calculated: {passwordHash}");
            var user = await userRepository.PutUserAsync(new User
            {
                Id = Guid.NewGuid(),
                Login = credentials.Login,
                PasswordHash = passwordHash
            });

            return StatusCode(201, new {user.Id});
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetAsync([FromRoute]Guid id)
        {
            var currentId = Guid.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);

            if (id != currentId)
            {
                return Forbid();
            }

            var user = await userRepository.GetUserAsync(id);

            if (user == null)
            {
                return BadRequest("User does not exists");
            }

            return Ok(new UserContract
            {
                Id = user.Id,
                Login = user.Login
            });
        }
    }
}