﻿namespace BPsim.Web.Auth.Passwords
{
    public interface IPasswordHashProvider
    {
        string MakeHash(string password);
        bool IsValidPassword(string password, string savedHash);
    }
}