﻿using System;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using BPsim.Web.Auth.DataAccess.Autofac;
using BPsim.Web.Auth.Passwords;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;

namespace BPsim.Web.Auth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            const string signingSecurityKey = "0d5b3235a8b403c3dab9c3f4f65c07fcalskd234n1k41230";
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingSecurityKey));

            services.AddMvc();
            services.AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = "JwtBearer";
                        options.DefaultChallengeScheme = "JwtBearer";
                    })
                    .AddJwtBearer("JwtBearer", options =>
                    {
                        var parameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = signingKey,
                            ValidateIssuer = true,
                            ValidIssuer = "BPsim.Web",
                            ValidateAudience = true,
                            ValidAudience = "BPsim.Web User",
                            ClockSkew = TimeSpan.FromDays(1)
                        };

                        options.TokenValidationParameters = parameters;
                    });
            
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterLogger();

            builder.Register(x => new MongoClient(Configuration.GetConnectionString("Mongo")).GetDatabase("BPsimWebAuth"))
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<PasswordHashProvider>()
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterModule<BPsimWebAuthDataAccessModule>();

            return new AutofacServiceProvider(builder.Build());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}