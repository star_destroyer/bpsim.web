﻿using Autofac;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BPsim.Web.Data.Access.Mongo.Autofac
{
    public class BPsimWebDataAccessMongoModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(x =>
                   {
                       var configuration = x.Resolve<IConfiguration>();
                       return new MongoClient(configuration.GetConnectionString("MongoDB"));
                   })
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.Register(x => x.Resolve<IMongoClient>().GetDatabase("BPsimWebImitation",
                                                                        new MongoDatabaseSettings
                                                                        {
                                                                            GuidRepresentation = GuidRepresentation.CSharpLegacy
                                                                        }))
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<ModelRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TaskRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ReportRepository>().AsImplementedInterfaces().SingleInstance();
        }
    }
}