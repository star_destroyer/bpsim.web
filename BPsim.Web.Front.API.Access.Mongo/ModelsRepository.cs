﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BPsim.Web.Front.Types;
using BPsim.Web.Front.Types.DTOs;
using MongoDB.Driver;

namespace BPsim.Web.Front.API.Access.Mongo
{
    public class ModelsRepository : IModelsRepository
    {
        private readonly IMongoDatabase database;

        public ModelsRepository(IMongoDatabase database)
        {
            this.database = database;
        }

        public async Task<Model> GetModelAsync(string userId, Guid id)
        {
            var collection = database.GetCollection<Model>("models");
            var filter = Builders<Model>.Filter.Eq(m => m.Id, id)
                         & Builders<Model>.Filter.Eq(m => m.UserId, userId);
            var findResult = await collection.FindAsync(filter);
            return await findResult.FirstOrDefaultAsync();
        }

        public async Task<Model> AddOrUpdateAsync(string userId, Model model)
        {
            var collection = database.GetCollection<Model>("models");
            var filter = Builders<Model>.Filter.Eq(m => m.Id, model.Id)
                         & Builders<Model>.Filter.Eq(m => m.UserId, userId);
	        await collection.ReplaceOneAsync(filter, model, new UpdateOptions {IsUpsert = true});
	        return model;
        }

        public async Task<IEnumerable<ModelSummary>> GetModelsAsync(string userId)
        {
            var collection = database.GetCollection<Model>("models");
            var findResult = await collection
                .Find(Builders<Model>.Filter.Eq(m => m.UserId, userId))
                .Project<ModelSummary>(Builders<Model>.Projection.Include(m => m.Id).Include(m => m.Name))
                .ToCursorAsync();
            return findResult.ToEnumerable();
        }
    }
}