﻿using System;

// ReSharper disable ClassNeverInstantiated.Global
namespace BPsim.Web.Front.API.Appsettings
{
    public class Settings
    {
        public Topology Topology { get; set; }
    }

    public class Topology
    {
        public Uri AuthUri { get; set; }

        public Uri ImitationUri { get; set; }

        public Uri AnalysisUri { get; set; }
    }
}