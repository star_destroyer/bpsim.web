using System;
using System.Threading.Tasks;
using BPsim.Web.Analysis.Client;
using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Front.API.Controllers
{
    [Route("analysis")]
    public class AnalysisController : Controller
    {
        private readonly IAnalysisClient analysisClient;

        public AnalysisController(IAnalysisClient analysisClient)
        {
            this.analysisClient = analysisClient;
        }

        [HttpGet("{id}.xlsx")]
        public async Task<ActionResult> GetExcelAsync([FromRoute] Guid id)
        {
            var content = await analysisClient.GetExcelAsync(id, HttpContext.RequestAborted);
            return new FileContentResult(content, "application/octet-stream");
        }
    }
}