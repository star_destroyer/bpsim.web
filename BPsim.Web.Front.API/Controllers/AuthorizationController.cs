﻿using System.Threading.Tasks;
using BPsim.Web.Auth.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Front.API.Controllers
{
    [AllowAnonymous]
    [Route("[controller]")]
    public class AuthorizationController : Controller
    {
        private readonly IAuthServiceClient authClient;

        public AuthorizationController(IAuthServiceClient authClient)
        {
            this.authClient = authClient;
        }

        [HttpPost]
        public async Task<IActionResult> AuthorizeAsync([FromBody] Credentials credentials)
        {
            var result = await authClient.AuthorizeAsync(credentials);
            if (result.Success)
            {
                return Ok(result.Token);
            }

            if (string.IsNullOrEmpty(result.Message))
            {
                return Unauthorized();
            }

            return BadRequest(result.Message);
        }
    }
}