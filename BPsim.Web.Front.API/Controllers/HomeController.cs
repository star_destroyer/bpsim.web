﻿using System.Text;
using BPsim.Web.Front.API.Utility;
using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Front.API.Controllers
{
    [Route("/")]
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var assets = WebpackAssets.GetPath("BPsim.Web");
            var html = new StringBuilder("<!doctype html>")
                       .Append("<html>")
                       .Append("<head>")
                       .Append("<meta charset=\"utf-8\">")
                       .Append("<title>The HTML5 Herald</title>")
                       .Append("<body>")
                       .Append("<div id=\"root\"></div>")
                       .Append($"<script src=\"{assets}\"></script>")
                       .Append("</body>")
                       .Append("</html>")
                       .ToString();
            return new ContentResult
            {
                Content = html,
                ContentType = "text/html"
            };
        }
    }
}