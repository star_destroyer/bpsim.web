using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Client;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Front.API.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class ModelController : Controller
    {
        private readonly IImitationClient imitationClient;

        public ModelController(IImitationClient imitationClient)
        {
            this.imitationClient = imitationClient;
        }

        [HttpGet]
        public async Task<ActionResult> SelectAsync()
        {
            var result = await imitationClient.SelectModelsAsync(HttpContext.RequestAborted).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetAsync([FromRoute] Guid id)
        {
            var result = await imitationClient.GetModelAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync([FromQuery] string name)
        {
            var emptyModelContract = new ModelContract
            {
                Name = name,
                Nodes = new Dictionary<string, INodeContract>(),
                Orders = new Dictionary<string, IDictionary<string, string>>(),
                Resources = new Dictionary<string, decimal[]>()
            };
            var model = await imitationClient.CreateModelAsync(emptyModelContract, HttpContext.RequestAborted);
            return Ok(model);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> SaveAsync([FromRoute] Guid id, [FromBody] ModelContract model)
        {
            await imitationClient.SaveModelAsync(id, model, HttpContext.RequestAborted).ConfigureAwait(false);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] Guid id)
        {
            await imitationClient.DeleteModelAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);
            return Ok();
        }

        [HttpGet("{id}/task")]
        public async Task<ActionResult> SelectTaskAsync([FromRoute] Guid id)
        {
            var result = await imitationClient.SelectTaskAsync(id, HttpContext.RequestAborted);
            return Ok(result.OrderByDescending(x => x.Timestamp));
        }

        [HttpPost("{id}/task")]
        public async Task<ActionResult> StartTask([FromRoute] Guid id, [FromBody] CreateTaskContract taskContract)
        {
            var result = await imitationClient.StartImitationAsync(id, taskContract, HttpContext.RequestAborted);
            return Ok(result.Id);
        }
    }
}