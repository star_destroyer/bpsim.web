using System;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Client;
using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Front.API.Controllers
{
    [Route("task")]
    public class TaskController : Controller
    {
        private readonly IImitationClient imitationClient;

        public TaskController(IImitationClient imitationClient)
        {
            this.imitationClient = imitationClient;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get([FromRoute] Guid id)
        {
            var result = await imitationClient.GetTaskAsync(id, HttpContext.RequestAborted);
            return Ok(result);
        }
    }
}