using System;
using System.Security.Claims;
using System.Threading.Tasks;
using BPsim.Web.Auth.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Front.API.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly IAuthServiceClient authServiceClient;

        public UserController(IAuthServiceClient authServiceClient)
        {
            this.authServiceClient = authServiceClient;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> RegisterAsync([FromBody] Credentials credentials)
        {
            var result = await authServiceClient.RegisterAsync(credentials);
            return result.Success ? StatusCode(201, result.Id) : BadRequest(result.Message);
        }

        [HttpGet]
        public async Task<ActionResult> GetCurrentUser()
        {
            var token = HttpContext.Request.Headers["Authorization"][0].Split(" ")[1];
            var id = Guid.Parse(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var user = await authServiceClient.GetAsync(token, id);
            return user != null ? (ActionResult) Ok(user) : BadRequest("User does not exists");
        }
    }
}