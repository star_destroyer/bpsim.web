﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;

namespace BPsim.Web.Front.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                          .UseStartup<Startup>()
                          .UseUrls("http://*:16090")
                          .UseSerilog((hostContext, configuration) => configuration
                                                                      .ReadFrom.Configuration(hostContext.Configuration))
                          .Build();
        }
    }
}