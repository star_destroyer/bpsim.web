﻿using System;
using System.Text;
using BPsim.Web.Analysis.Client;
using BPsim.Web.Auth.Client;
using BPsim.Web.Front.API.Appsettings;
using BPsim.Web.Imitation.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace BPsim.Web.Front.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private Settings Settings => Configuration.Get<Settings>();

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            const string signingSecurityKey = "0d5b3235a8b403c3dab9c3f4f65c07fcalskd234n1k41230";
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingSecurityKey));

            services.AddMvc()
                    .AddJsonOptions(options =>
                    {
                        options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                        options.SerializerSettings.Converters.Add(new NodeContractJsonConverter());
                        options.SerializerSettings.Converters.Add(new RuleContractJsonConverter());
                    });
            
            services.AddCors();

            services.AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = "JwtBearer";
                        options.DefaultChallengeScheme = "JwtBearer";
                    })
                    .AddJwtBearer("JwtBearer", options =>
                    {
                        var parameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = signingKey,
                            ValidateIssuer = true,
                            ValidIssuer = "BPsim.Web",
                            ValidateAudience = true,
                            ValidAudience = "BPsim.Web User",
                            ClockSkew = TimeSpan.FromDays(1)
                        };

                        options.TokenValidationParameters = parameters;
                    });

            services.AddSingleton<IAuthServiceClient>(x => new AuthServiceClient(Settings.Topology.AuthUri));
            services.AddSingleton<IImitationClient>(x => new ImitationClient(Settings.Topology.ImitationUri));
            services.AddSingleton<IAnalysisClient>(x => new AnalysisClient(Settings.Topology.AnalysisUri));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}