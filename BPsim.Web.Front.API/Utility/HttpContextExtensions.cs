﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace BPsim.Web.Front.API.Utility
{
    public static class HttpContextExtensions
    {
	    public static string GetUserId(this HttpContext context)
	    {
		    return context.User.FindFirstValue("uid");
	    }
    }
}
