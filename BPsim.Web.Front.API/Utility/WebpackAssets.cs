﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace BPsim.Web.Front.API.Utility
{
    public static class WebpackAssets
    {
        private static Dictionary<string, string> dictionary;
        private static Dictionary<string, string> Dictionary => dictionary ?? (dictionary = ReadEntries());

        public static string GetPath(string entry)
        {
            return Dictionary[entry];
        }

        private static Dictionary<string, string> ReadEntries()
        {
            var path = GetConfigPath();
            var json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }

        private static string GetConfigPath() => Path.Combine(GetProjectDirectory(), "wwwroot", "webpack-assets.json");

        private static string GetProjectDirectory() => Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6), "..", "..", "..");
    }
}