﻿using System;
using System.Collections.Generic;
using BPsim.Web.Front.Types.DTOs.Nodes;

namespace BPsim.Web.Front.Types.DTOs
{
    public class Model
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public IEnumerable<Node> Nodes { get; set; }
        public IEnumerable<Rule> Rules { get; set; }
        public string ProjectTree { get; set; }
    }
}