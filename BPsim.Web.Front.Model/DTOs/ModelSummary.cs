using System;

namespace BPsim.Web.Front.Types.DTOs
{
    public class ModelSummary
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}