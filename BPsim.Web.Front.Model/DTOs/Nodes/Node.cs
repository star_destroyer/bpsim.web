﻿using System.Collections.Generic;

namespace BPsim.Web.Front.Types.DTOs.Nodes
{
    public class Node
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public NodeType Type { get; set; }
	    public long? MinValue { get; set; }
	    public long? MaxValue { get; set; }
	    public long? Value { get; set; }
	    public IEnumerable<long> IncomingRules { get; set; }
	    public IEnumerable<long> OutgoingRules { get; set; }
		public string Duration { get; set; }
	    public IEnumerable<long> Rules { get; set; }
	    public IEnumerable<long> GlobalRules { get; set; }
	    public IEnumerable<long> Knowledges { get; set; }
    }
}