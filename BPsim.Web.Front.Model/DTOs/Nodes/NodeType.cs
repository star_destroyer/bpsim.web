﻿namespace BPsim.Web.Front.Types.DTOs.Nodes
{
    public enum NodeType
    {
        Folder = 0,
        Agent = 1,
        Operation = 2,
        Resource = 3,
        Knowledge = 4
    }
}