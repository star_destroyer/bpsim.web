﻿namespace BPsim.Web.Front.Types.DTOs
{
    public class Rule
    {
        public long Id { get; set; }
        public string Text { get; set; }
    }
}