﻿using System;

namespace BPsim.Web.Front.Types.DTOs
{
    public class User
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
    }
}