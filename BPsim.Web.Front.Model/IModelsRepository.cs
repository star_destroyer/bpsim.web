﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BPsim.Web.Front.Types.DTOs;

namespace BPsim.Web.Front.Types
{
    public interface IModelsRepository
    {
        Task<Model> GetModelAsync(string userId, Guid id);
        Task<Model> AddOrUpdateAsync(string userId, Model model);
        Task<IEnumerable<ModelSummary>> GetModelsAsync(string userId);
    }
}