﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BPsim.Web.Front.Types.DTOs;

namespace BPsim.Web.Front.Types
{
    public interface IUsersRepository
    {
        Task<User> GetUserAsync(Guid id);
        Task<User> GetUserAsync(string login);
        Task<IEnumerable<User>> GetUsersAsync();
        Task<User> PutUserAsync(User user);
        Task<User> UpdatePasswordAsync(Guid id, string passwordHash);
    }
}