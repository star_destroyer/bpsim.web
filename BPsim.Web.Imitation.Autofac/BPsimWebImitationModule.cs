﻿using Autofac;
using BPsim.Web.Imitation.Builders;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Factories;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation.Autofac
{
    public class BPsimWebImitationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Imitator>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<OperationExecutor>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AgentExecutor>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RuleExecutor>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<KnowledgeExecutor>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<Reporter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ContextFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ConfigurationBuilder>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ImitationEngine>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ExpressionExecutor>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ExpressionParser>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ExpressionFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ExpressionRuleFactory>().AsImplementedInterfaces().SingleInstance();
        }
    }
}