﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Service.Contracts;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;

namespace BPsim.Web.Imitation.Client
{
    public interface IImitationClient
    {
        Task<Contract<ModelSummary>[]> SelectModelsAsync(CancellationToken cancellation);

        Task<Contract<TaskContract>> GetTaskAsync(Guid id, CancellationToken cancellation);

        Task<Contract<ModelContract>> GetModelAsync(Guid id, CancellationToken cancellation);

        Task<Contract<ModelContract>> CreateModelAsync(ModelContract model, CancellationToken cancellation);

        Task SaveModelAsync(Guid id, ModelContract model, CancellationToken cancellation);

        Task DeleteModelAsync(Guid id, CancellationToken cancellation);

        Task<Contract<TaskContract>[]> SelectTaskAsync(Guid id, CancellationToken cancellation);

        Task<Contract<TaskContract>> StartImitationAsync(Guid id, CreateTaskContract taskContract, CancellationToken cancellation);

        Task<ReportContract[]> SelectReportsAsync(Guid taskId, CancellationToken cancellation);
    }
}