﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Service.Contracts;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using Newtonsoft.Json;

namespace BPsim.Web.Imitation.Client
{
    public class ImitationClient : IImitationClient
    {
        private readonly HttpClient httpClient;

        public ImitationClient(Uri uri)
        {
            httpClient = new HttpClient {BaseAddress = uri};
        }

        public async Task<Contract<ModelSummary>[]> SelectModelsAsync(CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/model");
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    return null;

                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<Contract<ModelSummary>[]>(json);
            }
        }

        public async Task<Contract<TaskContract>> GetTaskAsync(Guid id, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/task/{id}");
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    return null;

                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<Contract<TaskContract>>(json);
            }
        }

        public async Task<Contract<ModelContract>> GetModelAsync(Guid id, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/model/{id}");
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    return null;

                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<Contract<ModelContract>>(json, new NodeContractJsonConverter(), new RuleContractJsonConverter());
            }
        }

        public async Task<Contract<ModelContract>> CreateModelAsync(ModelContract model, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"/api/v1/model")
            {
                Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
            };
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<Contract<ModelContract>>(json, new NodeContractJsonConverter(), new RuleContractJsonConverter());
            }
        }

        public async Task SaveModelAsync(Guid id, ModelContract model, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, $"/api/v1/model/{id}")
            {
                Content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")
            };
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();
            }
        }

        public async Task DeleteModelAsync(Guid id, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, $"/api/v1/model/{id}");
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();
            }
        }

        public async Task<Contract<TaskContract>[]> SelectTaskAsync(Guid id, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/model/{id}/task");
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Contract<TaskContract>[]>(json);
            }
        }

        public async Task<Contract<TaskContract>> StartImitationAsync(Guid id, CreateTaskContract taskContract, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"/api/v1/model/{id}/task")
            {
                Content = new StringContent(JsonConvert.SerializeObject(taskContract), Encoding.UTF8, "application/json")
            };
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Contract<TaskContract>>(json);
            }
        }

        public async Task<ReportContract[]> SelectReportsAsync(Guid taskId, CancellationToken cancellation)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"/api/v1/task/{taskId}/report");
            using (var response = await httpClient.SendAsync(request, HttpCompletionOption.ResponseContentRead, cancellation).ConfigureAwait(false))
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                    return null;

                response.EnsureSuccessStatusCode();

                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                return JsonConvert.DeserializeObject<ReportContract[]>(json);
            }
        }
    }
}