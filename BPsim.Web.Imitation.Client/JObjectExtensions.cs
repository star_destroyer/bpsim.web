﻿using System;
using Newtonsoft.Json.Linq;

namespace BPsim.Web.Imitation.Client
{
    public static class JObjectExtensions
    {
        public static bool IsFieldExists(this JObject jObject, string name, JTokenType type)
        {
            return jObject.TryGetValue(name, StringComparison.InvariantCultureIgnoreCase, out var token) && token.Type == type;
        }
    }
}