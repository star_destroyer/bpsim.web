﻿using System;
using BPsim.Web.Imitation.Service.Contracts.Model;
using Newtonsoft.Json.Linq;

namespace BPsim.Web.Imitation.Client
{
    public class NodeContractJsonConverter : InheritanceSensitiveJsonConverter<INodeContract>
    {
        protected override INodeContract Create(Type objectType, JObject jObject)
        {
            if (jObject.IsFieldExists("Knowledges", JTokenType.Object))
            {
                return new AgentContract();
            }

            if (jObject.IsFieldExists("Duration", JTokenType.String))
            {
                return new OperationContract();
            }

            throw new ArgumentOutOfRangeException(nameof(objectType));
        }
    }
}