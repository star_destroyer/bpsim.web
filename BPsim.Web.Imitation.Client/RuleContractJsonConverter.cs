﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Service.Contracts.Model.Rules;
using Newtonsoft.Json.Linq;

namespace BPsim.Web.Imitation.Client
{
    public class RuleContractJsonConverter : InheritanceSensitiveJsonConverter<IRuleContract>
    {
        private static readonly IDictionary<RuleType, Func<IRuleContract>> stringCreators = new Dictionary<RuleType, Func<IRuleContract>>
        {
            {RuleType.ExpressionRule, () => new ExpressionRuleContract()},
            {RuleType.CreateOrderRule, () => new CreateOrderRuleContract()},
            {RuleType.RemoveOrderRule, () => new RemoveOrderRuleContract()},
            {RuleType.SelectOrderRule, () => new SelectOrderRuleContract()},
            {RuleType.SendOrderRule, () => new SendOrderRuleContract()}
        };

        protected override IRuleContract Create(Type objectType, JObject jObject)
        {
            if (!jObject.TryGetValue("Type", StringComparison.InvariantCultureIgnoreCase, out var token))
            {
                throw new ArgumentOutOfRangeException(nameof(jObject));
            }

            if (Enum.TryParse<RuleType>(token.ToString(), out var value) && stringCreators.TryGetValue(value, out var creator))
            {
                return creator.Invoke();
            }

            throw new ArgumentOutOfRangeException(nameof(token));
        }
    }
}