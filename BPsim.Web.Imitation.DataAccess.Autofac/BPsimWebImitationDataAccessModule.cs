﻿using Autofac;

namespace BPsim.Web.Imitation.DataAccess.Autofac
{
    public class BPsimWebImitationDataAccessModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<ModelRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TaskRepository>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ReportRepository>().AsImplementedInterfaces().SingleInstance();
        }
    }
}