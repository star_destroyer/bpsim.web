﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace BPsim.Web.Imitation.DataAccess.Data
{
    public class Entity<TData> : Entity
    {
        public TData Data { get; set; }
    }

    public class Entity
    {
        public Guid Id { get; set; }

        public int Revision { get; set; }

        [BsonSerializer(typeof(TimestampLongSerializer))]
        public long Timestamp { get; set; }
    }
}