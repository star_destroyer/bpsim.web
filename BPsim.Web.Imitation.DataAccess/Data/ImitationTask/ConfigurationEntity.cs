﻿using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.ImitationTask
{
    [DataContract]
    public class ConfigurationEntity
    {
        [DataMember]
        public string EndingRule { get; set; }

        [DataMember]
        public ImitationStep[] Steps { get; set; }
    }
}