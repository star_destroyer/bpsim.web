﻿namespace BPsim.Web.Imitation.DataAccess.Data.ImitationTask
{
    public enum ImitationStep
    {
        Free,
        Busy,
        Agents
    }
}