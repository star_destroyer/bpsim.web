﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.ImitationTask
{
    [DataContract]
    public class SubjectAreaEntity
    {
        [DataMember]
        public IDictionary<string, decimal[]> Resources { get; set; }
    }
}