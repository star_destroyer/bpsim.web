﻿using System;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.ImitationTask
{
    [DataContract]
    public class TaskEntity
    {
        [DataMember]
        public Guid ModelId { get; set; }

        [DataMember]
        public int ModelRevision { get; set; }

        [DataMember]
        public TaskState State { get; set; }

        [DataMember]
        public SubjectAreaEntity SubjectArea { get; set; }

        [DataMember]
        public ConfigurationEntity Configuration { get; set; }

        [DataMember]
        public ReportEntity[] Reports { get; set; }
    }
}