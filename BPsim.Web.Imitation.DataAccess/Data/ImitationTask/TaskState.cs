﻿namespace BPsim.Web.Imitation.DataAccess.Data.ImitationTask
{
    public enum TaskState
    {
        Open = 0,
        Scheduled = 1,
        Pending = 2,
        Failed = 3,
        Successfull = 4,
        Aborted = 5,
        Deleted = 6
    }
}