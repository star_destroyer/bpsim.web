﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;

namespace BPsim.Web.Imitation.DataAccess.Data.Model
{
    [DataContract]
    public class AgentEntity : INodeEntity
    {
        [DataMember]
        public IRuleEntity[] GlobalRules { get; set; }

        [DataMember]
        public IDictionary<string, KnowledgeEntity> Knowledges { get; set; }
    }
}