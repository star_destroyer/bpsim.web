﻿using System.Runtime.Serialization;
using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;

namespace BPsim.Web.Imitation.DataAccess.Data.Model
{
    [DataContract]
    public class KnowledgeEntity
    {
        [DataMember]
        public IRuleEntity[] Rules { get; set; }
    }
}