﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.Model
{
    [DataContract]
    public class ModelEntity
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public IDictionary<string, decimal[]> Resources { get; set; }

        [DataMember]
        public IDictionary<string, IDictionary<string, string>> Orders { get; set; }

        [DataMember]
        public IDictionary<string, INodeEntity> Nodes { get; set; }
    }
}