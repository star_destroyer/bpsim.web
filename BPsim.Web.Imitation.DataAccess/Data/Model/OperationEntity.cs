﻿using System.Runtime.Serialization;
using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;

namespace BPsim.Web.Imitation.DataAccess.Data.Model
{
    [DataContract]
    public class OperationEntity : INodeEntity
    {
        [DataMember]
        public IRuleEntity[] InRules { get; set; }

        [DataMember]
        public IRuleEntity[] OutRules { get; set; }

        [DataMember]
        public string Duration { get; set; }
    }
}