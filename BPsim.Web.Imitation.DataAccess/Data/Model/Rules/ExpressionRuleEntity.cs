﻿using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.Model.Rules
{
    [DataContract]
    public class ExpressionRuleEntity : IRuleEntity
    {
        [DataMember]
        public string Expression { get; set; }
    }
}