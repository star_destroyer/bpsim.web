﻿using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.Model.Rules
{
    [DataContract]
    public class RemoveOrderRuleEntity : IRuleEntity
    {
        [DataMember]
        public string OrderName { get; set; }
    }
}