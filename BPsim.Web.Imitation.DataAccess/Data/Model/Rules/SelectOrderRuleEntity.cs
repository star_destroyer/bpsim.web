﻿using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.Model.Rules
{
    [DataContract]
    public class SelectOrderRuleEntity : IRuleEntity
    {
        [DataMember]
        public string OrderName { get; set; }

        [DataMember]
        public string TargetName { get; set; }
    }
}