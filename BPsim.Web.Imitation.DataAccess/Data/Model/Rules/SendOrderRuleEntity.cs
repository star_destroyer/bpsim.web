﻿using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.DataAccess.Data.Model.Rules
{
    [DataContract]
    public class SendOrderRuleEntity : IRuleEntity
    {
        [DataMember]
        public string OrderName { get; set; }

        [DataMember]
        public string TargetName { get; set; }
    }
}