﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess.Data;
using BPsim.Web.Imitation.DataAccess.Data.Model;

namespace BPsim.Web.Imitation.DataAccess
{
    public interface IModelRepository
    {
        Task<Entity<ModelEntity>> CreateAsync(ModelEntity entity, CancellationToken cancellation);

        Task<Entity<ModelEntity>> UpdateAsync(Guid id, ModelEntity entity, CancellationToken cancellation);

        Task<Entity<ModelEntity>> RemoveAsync(Guid id, CancellationToken cancellation);

        Task<Entity<ModelEntity>[]> SelectAsync(int skip, int limit, CancellationToken cancellation);

        Task<Entity<ModelEntity>> GetAsync(Guid id, CancellationToken cancellation);
    }
}