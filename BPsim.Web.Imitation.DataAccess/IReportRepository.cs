﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;

namespace BPsim.Web.Imitation.DataAccess
{
    public interface IReportRepository
    {
        Task SaveAsync(Guid taskId, ReportEntity report, CancellationToken cancellation);

        Task SaveAsync(Guid taskId, IEnumerable<ReportEntity> reports, CancellationToken cancellation);

        Task<ReportEntity[]> SelectAsync(Guid taskId, CancellationToken cancellation);
    }
}