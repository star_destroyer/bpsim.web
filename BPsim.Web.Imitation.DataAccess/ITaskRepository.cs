﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess.Data;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;

namespace BPsim.Web.Imitation.DataAccess
{
    public interface ITaskRepository
    {
        Task<Entity<TaskInfoProjection>> GetInfoAsync(Guid id, CancellationToken cancellation);

        Task<Entity<TaskEntity>> GetAsync(Guid id, CancellationToken cancellation);

        Task<Entity<TaskInfoProjection>> ScheduleAsync(CancellationToken cancellation);

        Task<Entity<TaskInfoProjection>> SetTaskState(Guid id, TaskState state, CancellationToken cancellation);

        Task<Entity<TaskInfoProjection>[]> SelectAsync(int skip, int limit, CancellationToken cancellation);

        Task<Entity<TaskInfoProjection>[]> SelectAsync(Guid modelId, CancellationToken cancellation);

        Task<Entity<TaskInfoProjection>> CreateAsync(TaskEntity entity, CancellationToken cancellation);

        Task<Entity<TaskInfoProjection>> DeleteAsync(Guid id, CancellationToken cancellation);
    }
}