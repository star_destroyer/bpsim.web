﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess.Data;
using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace BPsim.Web.Imitation.DataAccess
{
    public class ModelRepository : IModelRepository
    {
        private readonly IMongoCollection<Entity<ModelEntity>> collection;

        public ModelRepository(IMongoDatabase database)
        {
            collection = database.GetCollection<Entity<ModelEntity>>("Models");
        }

        static ModelRepository()
        {
            BsonClassMap.RegisterClassMap<OperationEntity>();
            BsonClassMap.RegisterClassMap<AgentEntity>();
            BsonClassMap.RegisterClassMap<ExpressionRuleEntity>();
            BsonClassMap.RegisterClassMap<CreateOrderRuleEntity>();
            BsonClassMap.RegisterClassMap<RemoveOrderRuleEntity>();
            BsonClassMap.RegisterClassMap<SelectOrderRuleEntity>();
            BsonClassMap.RegisterClassMap<SendOrderRuleEntity>();
        }

        private static FilterDefinitionBuilder<Entity<ModelEntity>> Filter => Builders<Entity<ModelEntity>>.Filter;
        private static UpdateDefinitionBuilder<Entity<ModelEntity>> Update => Builders<Entity<ModelEntity>>.Update;

        public async Task<Entity<ModelEntity>> CreateAsync(ModelEntity entity, CancellationToken cancellation)
        {
            var savingEntity = new Entity<ModelEntity>
            {
                Id = Guid.NewGuid(),
                Revision = 1,
                Data = entity
            };

            var options = new FindOneAndUpdateOptions<Entity<ModelEntity>, Entity<ModelEntity>>
            {
                IsUpsert = true,
                ReturnDocument = ReturnDocument.After
            };

            var savedEntity = await collection.FindOneAndUpdateAsync(Filter.Eq(x => x.Id, savingEntity.Id),
                                                                     Update.Set(x => x.Data, savingEntity.Data)
                                                                           .Inc(x => x.Revision, 1)
                                                                           .CurrentDate(x => x.Timestamp, UpdateDefinitionCurrentDateType.Timestamp),
                                                                     options,
                                                                     cancellation)
                                              .ConfigureAwait(false);
            return savedEntity;
        }

        public async Task<Entity<ModelEntity>> UpdateAsync(Guid id, ModelEntity entity, CancellationToken cancellation)
        {
            var savingEntity = new Entity<ModelEntity>
            {
                Id = id,
                Data = entity
            };

            var options = new FindOneAndUpdateOptions<Entity<ModelEntity>, Entity<ModelEntity>>
            {
                ReturnDocument = ReturnDocument.After
            };

            var savedEntity = await collection.FindOneAndUpdateAsync(Filter.Eq(x => x.Id, savingEntity.Id),
                                                                     Update.Set(x => x.Data, savingEntity.Data)
                                                                           .Inc(x => x.Revision, 1)
                                                                           .CurrentDate(x => x.Timestamp, UpdateDefinitionCurrentDateType.Timestamp),
                                                                     options,
                                                                     cancellation)
                                              .ConfigureAwait(false);
            return savedEntity;
        }

        public async Task<Entity<ModelEntity>> RemoveAsync(Guid id, CancellationToken cancellation)
        {
            return await collection.FindOneAndDeleteAsync(Filter.Eq(x => x.Id, id), null, cancellation).ConfigureAwait(false);
        }

        public async Task<Entity<ModelEntity>[]> SelectAsync(int skip, int limit, CancellationToken cancellation)
        {
            var options = new FindOptions<Entity<ModelEntity>>
            {
                Skip = skip,
                Limit = limit
            };

            var asyncCursor = await collection.FindAsync(Filter.Empty, options, cancellation).ConfigureAwait(false);
            var entities = await asyncCursor.ToListAsync(cancellation).ConfigureAwait(false);
            return entities.ToArray();
        }

        public async Task<Entity<ModelEntity>> GetAsync(Guid id, CancellationToken cancellation)
        {
            var asyncCursor = await collection.FindAsync(Filter.Eq(x => x.Id, id), null, cancellation).ConfigureAwait(false);
            return await asyncCursor.FirstOrDefaultAsync(cancellation);
        }
    }
}