﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess.Data;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using MongoDB.Driver;

namespace BPsim.Web.Imitation.DataAccess
{
    public class ReportRepository : IReportRepository
    {
        private readonly IMongoCollection<Entity<TaskEntity>> collection;

        public ReportRepository(IMongoDatabase database)
        {
            collection = database.GetCollection<Entity<TaskEntity>>("Tasks");
        }

        private static FilterDefinitionBuilder<Entity<TaskEntity>> Filter => Builders<Entity<TaskEntity>>.Filter;
        private static UpdateDefinitionBuilder<Entity<TaskEntity>> Update => Builders<Entity<TaskEntity>>.Update;
        private static ProjectionDefinitionBuilder<Entity<TaskEntity>> Projection => Builders<Entity<TaskEntity>>.Projection;

        public Task SaveAsync(Guid taskId, ReportEntity report, CancellationToken cancellation)
        {
            return SaveAsync(taskId, new[] {report}, cancellation);
        }

        public async Task SaveAsync(Guid taskId, IEnumerable<ReportEntity> reports, CancellationToken cancellation)
        {
            await collection.UpdateOneAsync(Filter.Eq(x => x.Id, taskId),
                                            Update.PushEach(x => x.Data.Reports, reports)
                                                  .Inc(x => x.Revision, 1)
                                                  .CurrentDate(x => x.Timestamp, UpdateDefinitionCurrentDateType.Timestamp),
                                            null,
                                            cancellation).ConfigureAwait(false);
        }

        public async Task<ReportEntity[]> SelectAsync(Guid taskId, CancellationToken cancellation)
        {

            var asyncCursor = await collection.FindAsync(Filter.Eq(x => x.Id, taskId), null, cancellation).ConfigureAwait(false);
            var first = await asyncCursor.FirstOrDefaultAsync(cancellation).ConfigureAwait(false);
            return first?.Data.Reports;
        }
    }
}