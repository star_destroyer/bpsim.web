﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess.Data;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using MongoDB.Driver;

namespace BPsim.Web.Imitation.DataAccess
{
    public class TaskRepository : ITaskRepository
    {
        private readonly IMongoCollection<Entity<TaskEntity>> collection;

        public TaskRepository(IMongoDatabase database)
        {
            collection = database.GetCollection<Entity<TaskEntity>>("Tasks");
            collection.Indexes.CreateOne(Index.Ascending(x => x.Data.ModelId));
            collection.Indexes.CreateOne(Index.Ascending(x => x.Data.State));
        }

        private static FilterDefinitionBuilder<Entity<TaskEntity>> Filter => Builders<Entity<TaskEntity>>.Filter;
        private static UpdateDefinitionBuilder<Entity<TaskEntity>> Update => Builders<Entity<TaskEntity>>.Update;
        private static ProjectionDefinitionBuilder<Entity<TaskEntity>> Projection => Builders<Entity<TaskEntity>>.Projection;
        private static IndexKeysDefinitionBuilder<Entity<TaskEntity>> Index => Builders<Entity<TaskEntity>>.IndexKeys;

        public async Task<Entity<TaskInfoProjection>> GetInfoAsync(Guid id, CancellationToken cancellation)
        {
            var options = new FindOptions<Entity<TaskEntity>, Entity<TaskInfoProjection>>
            {
                Projection = Projection.Exclude(x => x.Data.Reports)
            };

            var asyncCursor = await collection.FindAsync(Filter.Eq(x => x.Id, id), options, cancellation).ConfigureAwait(false);

            return await asyncCursor.FirstOrDefaultAsync(cancellation).ConfigureAwait(false);
        }

        public async Task<Entity<TaskEntity>> GetAsync(Guid id, CancellationToken cancellation)
        {
            var asyncCursor = await collection.FindAsync(Filter.Eq(x => x.Id, id), null, cancellation).ConfigureAwait(false);

            return await asyncCursor.FirstOrDefaultAsync(cancellation).ConfigureAwait(false);
        }

        public async Task<Entity<TaskInfoProjection>> ScheduleAsync(CancellationToken cancellation)
        {
            var options = new FindOneAndUpdateOptions<Entity<TaskEntity>, Entity<TaskInfoProjection>>
            {
                Projection = Projection.Exclude(x => x.Data.Reports),
                ReturnDocument = ReturnDocument.After
            };

            var task = await collection.FindOneAndUpdateAsync(Filter.Eq(x => x.Data.State, TaskState.Open),
                                                              Update.Set(x => x.Data.State, TaskState.Scheduled)
                                                                    .Inc(x => x.Revision, 1)
                                                                    .CurrentDate(x => x.Timestamp, UpdateDefinitionCurrentDateType.Timestamp),
                                                              options,
                                                              cancellation)
                                       .ConfigureAwait(false);

            return task;
        }

        public async Task<Entity<TaskInfoProjection>> SetTaskState(Guid id, TaskState state, CancellationToken cancellation)
        {
            var options = new FindOneAndUpdateOptions<Entity<TaskEntity>, Entity<TaskInfoProjection>>
            {
                Projection = Projection.Exclude(x => x.Data.Reports),
                ReturnDocument = ReturnDocument.After
            };

            var task = await collection.FindOneAndUpdateAsync(Filter.Eq(x => x.Id, id),
                                                              Update.Set(x => x.Data.State, state)
                                                                    .Inc(x => x.Revision, 1)
                                                                    .CurrentDate(x => x.Timestamp, UpdateDefinitionCurrentDateType.Timestamp),
                                                              options,
                                                              cancellation)
                                       .ConfigureAwait(false);

            return task;
        }

        public async Task<Entity<TaskInfoProjection>[]> SelectAsync(int skip, int limit, CancellationToken cancellation)
        {
            var options = new FindOptions<Entity<TaskEntity>, Entity<TaskInfoProjection>>
            {
                Skip = skip,
                Limit = limit,
                Projection = Projection.Exclude(x => x.Data.Reports)
            };

            var asyncCursor = await collection.FindAsync(Filter.Empty, options, cancellation).ConfigureAwait(false);
            var entities = await asyncCursor.ToListAsync(cancellation).ConfigureAwait(false);
            return entities.ToArray();
        }

        public async Task<Entity<TaskInfoProjection>[]> SelectAsync(Guid modelId, CancellationToken cancellation)
        {
            var options = new FindOptions<Entity<TaskEntity>, Entity<TaskInfoProjection>>
            {
                Projection = Projection.Exclude(x => x.Data.Reports)
            };

            var asyncCursor = await collection.FindAsync(Filter.Eq(x => x.Data.ModelId, modelId), options, cancellation).ConfigureAwait(false);
            var entities = await asyncCursor.ToListAsync(cancellation).ConfigureAwait(false);
            return entities.ToArray();
        }

        public async Task<Entity<TaskInfoProjection>> CreateAsync(TaskEntity entity, CancellationToken cancellation)
        {
            var savingEntity = new Entity<TaskEntity>
            {
                Id = Guid.NewGuid(),
                Revision = 1,
                Data = entity
            };

            var options = new FindOneAndUpdateOptions<Entity<TaskEntity>, Entity<TaskInfoProjection>>
            {
                IsUpsert = true,
                ReturnDocument = ReturnDocument.After,
                Projection = Projection.Exclude(x => x.Data.Reports)
            };

            var savedEntity = await collection.FindOneAndUpdateAsync(Filter.Eq(x => x.Id, savingEntity.Id),
                                                                     Update.Set(x => x.Data, savingEntity.Data)
                                                                           .Inc(x => x.Revision, 1)
                                                                           .CurrentDate(x => x.Timestamp, UpdateDefinitionCurrentDateType.Timestamp),
                                                                     options,
                                                                     cancellation)
                                              .ConfigureAwait(false);
            return savedEntity;
        }

        public async Task<Entity<TaskInfoProjection>> DeleteAsync(Guid id, CancellationToken cancellation)
        {
            var options = new FindOneAndDeleteOptions<Entity<TaskEntity>, Entity<TaskInfoProjection>>
            {
                Projection = Projection.Exclude(x => x.Data.Reports)
            };
            return await collection.FindOneAndDeleteAsync(Filter.Eq(x => x.Id, id), options, cancellation).ConfigureAwait(false);
        }
    }
}