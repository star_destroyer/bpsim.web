﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace BPsim.Web.Imitation.DataAccess
{
    public class TimestampLongSerializer : SerializerBase<long>
    {
        private const long TicksPerSecond = 10000000;
        private const long SecondsUpTo1970 = 62135596800;
        private const long TicksUpTo1970 = SecondsUpTo1970 * TicksPerSecond;

        public override long Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var currentBsonType = context.Reader.GetCurrentBsonType();
            switch (currentBsonType)
            {
                case BsonType.Int64:
                    return context.Reader.ReadInt64();
                case BsonType.Timestamp:
                    return ToTicks(context.Reader.ReadTimestamp());
                default:
                    throw new Exception($"Cannot deserialize Date from BsonType {currentBsonType}.");
            }
        }

        public override void Serialize(BsonSerializationContext context, BsonSerializationArgs args, long value)
        {
            context.Writer.WriteTimestamp(FromTicks(value));
        }

        private static long ToTicks(long bsonTimestamp)
        {
            if (bsonTimestamp == 0)
            {
                return 0;
            }

            var seconds = (bsonTimestamp >> 32) + SecondsUpTo1970;
            var ticks = bsonTimestamp & 0xFFFFFFFF;
            return seconds * TicksPerSecond + ticks;
        }

        private static long FromTicks(long ticks)
        {
            if (ticks < TicksUpTo1970)
            {
                return 0;
            }

            var seconds = ticks / TicksPerSecond - SecondsUpTo1970;
            var increment = ticks % TicksPerSecond;
            return (seconds << 32) | increment;
        }
    }
}