﻿using System;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts
{
    [DataContract]
    public class Contract<TData> : Contract
    {
        [DataMember]
        public TData Data { get; set; }

        public static Contract<TData> Create<TData>(Guid id, int revision, long timestamp, TData data)
        {
            return Create(id, revision, new DateTimeOffset(new DateTime(timestamp)), data);
        }

        public static Contract<TData> Create<TData>(Guid id, int revision, DateTimeOffset timestamp, TData data)
        {
            return new Contract<TData>
            {
                Id = id,
                Revision = revision,
                Timestamp = timestamp,
                Data = data
            };
        }
    }

    [DataContract]
    public class Contract
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public int Revision { get; set; }

        [DataMember]
        public DateTimeOffset Timestamp { get; set; }

        public static Contract Create(Guid id, int revision, long timestamp)
        {
            return Create(id, revision, new DateTimeOffset(new DateTime(timestamp)));
        }

        public static Contract Create(Guid id, int revision, DateTimeOffset timestamp)
        {
            return new Contract
            {
                Id = id,
                Revision = revision,
                Timestamp = timestamp
            };
        }
    }
}