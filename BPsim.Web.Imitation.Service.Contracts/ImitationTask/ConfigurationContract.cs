﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.ImitationTask
{
    [DataContract]
    public class ConfigurationContract
    {
        private ImitationStepContact[] steps;

        [DataMember]
        [Required]
        public string EndingRule { get; set; }

        [DataMember]
        public ImitationStepContact[] Steps { get => steps ?? (steps = new[] {ImitationStepContact.FreeNodes, ImitationStepContact.Agents, ImitationStepContact.BusyNodes}); set => steps = value; }
    }
}