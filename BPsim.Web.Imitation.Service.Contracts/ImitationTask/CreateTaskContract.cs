﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.ImitationTask
{
    [DataContract]
    public class CreateTaskContract
    {
        private SubjectAreaContract subjectArea;

        [DataMember]
        [Required]
        public ConfigurationContract Configuration { get; set; }

        [DataMember]
        public SubjectAreaContract SubjectArea { get => subjectArea ?? (subjectArea = new SubjectAreaContract()); set => subjectArea = value; }
    }
}