﻿namespace BPsim.Web.Imitation.Service.Contracts.ImitationTask
{
    public enum ImitationStepContact
    {
        FreeNodes,
        BusyNodes,
        Agents
    }
}