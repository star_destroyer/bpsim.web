﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.ImitationTask
{
    [DataContract]
    public class SubjectAreaContract
    {
        private IDictionary<string, decimal[]> resources;

        [DataMember]
        [ResourceValidation]
        public IDictionary<string, decimal[]> Resources { get => resources ?? (resources = new Dictionary<string, decimal[]>()); set => resources = value; }
    }
}