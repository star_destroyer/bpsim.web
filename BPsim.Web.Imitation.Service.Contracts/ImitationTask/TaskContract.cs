﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.ImitationTask
{
    [DataContract]
    public class TaskContract
    {
        [DataMember]
        [Required]
        public Guid ModelId { get; set; }

        [DataMember]
        [Required]
        public int ModelRevision { get; set; }

        [DataMember]
        public TaskStateContract State { get; set; }

        [DataMember]
        public ConfigurationContract Configuration { get; set; }

        [DataMember]
        public SubjectAreaContract SubjectArea { get; set; }
    }
}