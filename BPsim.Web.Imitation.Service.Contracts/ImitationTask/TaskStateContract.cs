﻿namespace BPsim.Web.Imitation.Service.Contracts.ImitationTask
{
    public enum TaskStateContract
    {
        Open = 0,
        Scheduled = 1,
        Pending = 2,
        Failed = 3,
        Successfull = 4,
        Aborted = 5,
        Deleted = 6
    }
}