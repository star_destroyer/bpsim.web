﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using BPsim.Web.Imitation.Service.Contracts.Model.Rules;

namespace BPsim.Web.Imitation.Service.Contracts.Model
{
    [DataContract]
    public class AgentContract : INodeContract
    {
        [DataMember]
        [Required]
        public IRuleContract[] GlobalRules { get; set; }

        [DataMember]
        [Required]
        public IDictionary<string, KnowledgeContract> Knowledges { get; set; }
    }
}