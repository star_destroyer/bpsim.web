﻿using System.Runtime.Serialization;
using BPsim.Web.Imitation.Service.Contracts.Model.Rules;

namespace BPsim.Web.Imitation.Service.Contracts.Model
{
    [DataContract]
    public class KnowledgeContract
    {
        [DataMember]
        public IRuleContract[] Rules { get; set; }
    }
}