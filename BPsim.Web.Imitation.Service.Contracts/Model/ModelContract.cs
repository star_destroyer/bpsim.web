﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.Model
{
    [DataContract]
    public class ModelContract
    {
        [DataMember]
        [Required]
        public string Name { get; set; }

        [DataMember]
        [Required]
        [ResourceValidation]
        public IDictionary<string, decimal[]> Resources { get; set; }

        [DataMember]
        [Required]
        public IDictionary<string, IDictionary<string, string>> Orders { get; set; }

        [DataMember]
        [Required]
        public IDictionary<string, INodeContract> Nodes { get; set; }
    }
}