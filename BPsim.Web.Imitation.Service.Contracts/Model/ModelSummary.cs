﻿using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.Model
{
    [DataContract]
    public class ModelSummary
    {
        [DataMember]
        public string Name { get; set; }
    }
}