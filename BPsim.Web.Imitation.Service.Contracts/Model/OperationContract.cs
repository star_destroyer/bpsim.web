﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using BPsim.Web.Imitation.Service.Contracts.Model.Rules;

namespace BPsim.Web.Imitation.Service.Contracts.Model
{
    [DataContract]
    public class OperationContract : INodeContract
    {
        [DataMember]
        public IRuleContract[] InRules { get; set; }

        [DataMember]
        public IRuleContract[] OutRules { get; set; }

        [DataMember]
        [Required]
        public string Duration { get; set; }
    }
}