﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.Model.Rules
{
    [DataContract]
    public class ExpressionRuleContract : IRuleContract
    {
        [DataMember]
        [Required]
        public string Expression { get; set; }

        [DataMember]
        [Required]
        public RuleType Type => RuleType.ExpressionRule;
    }
}