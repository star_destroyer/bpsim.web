﻿namespace BPsim.Web.Imitation.Service.Contracts.Model.Rules
{
    public interface IRuleContract
    {
        RuleType Type { get; }
    }
}