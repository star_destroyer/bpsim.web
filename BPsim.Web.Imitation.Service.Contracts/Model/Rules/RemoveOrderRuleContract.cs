﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.Model.Rules
{
    [DataContract]
    public class RemoveOrderRuleContract : IRuleContract
    {
        [DataMember]
        [Required]
        public string OrderName { get; set; }

        [DataMember]
        [Required]
        public RuleType Type => RuleType.RemoveOrderRule;
    }
}