﻿namespace BPsim.Web.Imitation.Service.Contracts.Model.Rules
{
    public enum RuleType
    {
        ExpressionRule,
        CreateOrderRule,
        RemoveOrderRule,
        SelectOrderRule,
        SendOrderRule
    }
}