﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.Model.Rules
{
    [DataContract]
    public class SelectOrderRuleContract : IRuleContract
    {
        [DataMember]
        [Required]
        public string OrderName { get; set; }

        [DataMember]
        public string TargetName { get; set; }

        [DataMember]
        [Required]
        public RuleType Type => RuleType.SelectOrderRule;
    }
}