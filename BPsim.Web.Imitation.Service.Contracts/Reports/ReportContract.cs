﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BPsim.Web.Imitation.Service.Contracts.Reports
{
    [DataContract]
    public class ReportContract
    {
        [DataMember]
        public int Tick { get; set; }

        [DataMember]
        public IDictionary<string, decimal> Resources { get; set; }

        [DataMember]
        public IDictionary<string, int> Orders { get; set; }

        [DataMember]
        public IDictionary<string, bool> Operations { get; set; }

        [DataMember]
        public IDictionary<string, IDictionary<string, int>> Agents { get; set; }
    }
}