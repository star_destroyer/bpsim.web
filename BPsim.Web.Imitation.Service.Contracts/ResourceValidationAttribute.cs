﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BPsim.Web.Imitation.Service.Contracts
{
    public class ResourceValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var resources = (IDictionary<string, decimal[]>) value;
            foreach (var resource in resources)
            {
                var resourceValues = resource.Value;
                if (resourceValues.Length < 1 || resourceValues.Length > 3)
                {
                    return new ValidationResult("Invalid resource values range. Values array can be from 1 to 3 length", new[] {resource.Key});
                }

                if (resourceValues.Length > 1 && resourceValues[0] < resourceValues[1])
                {
                    return new ValidationResult("Invalid resource values range. First value must be greater or equal second value", new[] {resource.Key});
                }

                if (resourceValues.Length == 2 && resourceValues[0] > resourceValues[3])
                {
                    return new ValidationResult("Invalid resource values range. First value must be less or equal third value", new[] {resource.Key});
                }
            }

            return ValidationResult.Success;
        }
    }
}