﻿using Microsoft.AspNetCore.Mvc;

namespace BPsim.Web.Imitation.Service.Controllers
{
    [Route("")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : ControllerBase
    {
        [HttpGet("", Name = "HomeGetIndex")]
        public IActionResult Index()
        {
            return RedirectPermanent("/swagger");
        }
    }
}