﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Model;
using BPsim.Web.Imitation.Service.Converters.ImitationTask;
using BPsim.Web.Imitation.Service.Converters.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Net.Http.Headers;

namespace BPsim.Web.Imitation.Service.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]", Name = "Models")]
    [ApiVersion("1.0")]
    public class ModelController : Controller
    {
        private readonly IModelConverter modelConverter;
        private readonly IModelRepository modelRepository;
        private readonly ITaskRepository taskRepository;
        private readonly ITaskConverter taskConverter;
        private readonly IConfigurationConverter configurationConverter;

        public ModelController(IModelConverter modelConverter,
                               IModelRepository modelRepository,
                               ITaskRepository taskRepository,
                               ITaskConverter taskConverter,
                               IConfigurationConverter configurationConverter)
        {
            this.modelConverter = modelConverter;
            this.modelRepository = modelRepository;
            this.taskRepository = taskRepository;
            this.taskConverter = taskConverter;
            this.configurationConverter = configurationConverter;
        }

        /// <summary>
        ///     Returns an Allow HTTP header with the allowed HTTP methods.
        /// </summary>
        /// <returns>A 200 OK response.</returns>
        /// <response code="200">The allowed HTTP methods.</response>
        [HttpOptions]
        public IActionResult Options()
        {
            HttpContext.Response.Headers.AppendCommaSeparatedValues(HeaderNames.Allow,
                                                                    HttpMethods.Get,
                                                                    HttpMethods.Options,
                                                                    HttpMethods.Post);
            return Ok();
        }

        /// <summary>
        ///     Returns an Allow HTTP header with the allowed HTTP methods for a model with the specified unique identifier.
        /// </summary>
        /// <param name="id">The model identifier</param>
        /// <returns>A 200 OK response.</returns>
        /// <response code="200">The allowed HTTP methods.</response>
        [HttpOptions("{id}")]
        public IActionResult Options([FromRoute] Guid id)
        {
            HttpContext.Response.Headers.AppendCommaSeparatedValues(HeaderNames.Allow,
                                                                    HttpMethods.Get,
                                                                    HttpMethods.Options,
                                                                    HttpMethods.Put,
                                                                    HttpMethods.Delete);
            return Ok();
        }

        /// <summary>
        ///     Returns an Allow HTTP header with the allowed HTTP methods for interation between model and task with the specified
        ///     unique model identifier.
        /// </summary>
        /// <param name="id">The model identifier</param>
        /// <returns>A 200 OK response.</returns>
        /// <response code="200">The allowed HTTP methods.</response>
        [HttpOptions("{id}/task")]
        public IActionResult TaskOptions([FromRoute] Guid id)
        {
            HttpContext.Response.Headers.AppendCommaSeparatedValues(HeaderNames.Allow,
                                                                    HttpMethods.Get,
                                                                    HttpMethods.Options,
                                                                    HttpMethods.Post);
            return Ok();
        }

        /// <summary>
        ///     Creates a new model.
        /// </summary>
        /// <param name="model">The model to create.</param>
        /// <returns>
        ///     A 200 Ok response containing the newly created model or a 400 Bad Request if the model is
        ///     invalid.
        /// </returns>
        /// <response code="200">The model was created.</response>
        /// <response code="400">The model is invalid.</response>
        [HttpPost("")]
        [ProducesResponseType(typeof(Contract<ModelContract>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        public async Task<Contract<ModelContract>> Post([FromBody] ModelContract model)
        {
            var modelEntity = modelConverter.ToEntity(model);
            var result = await modelRepository.CreateAsync(modelEntity, HttpContext.RequestAborted).ConfigureAwait(false);
            return Contract<ModelContract>.Create(result.Id, result.Revision, result.Timestamp, modelConverter.ToContract(result.Data));
        }

        /// <summary>
        ///     Updates a existing model
        /// </summary>
        /// <param name="id">The model identifier</param>
        /// <param name="contract">The model to update</param>
        /// <returns>
        ///     A 200 Ok response containing the newly updated model, a 400 Bad Request if the model is
        ///     invalid or a 404 Not Found response if the model doesn't exist
        /// </returns>
        /// <response code="200">The model was created.</response>
        /// <response code="400">The model is invalid.</response>
        /// <response code="404">The model doesn't exist.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Contract<ModelContract>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put([FromRoute] Guid id, [FromBody] ModelContract contract)
        {
            var entity = modelConverter.ToEntity(contract);

            var result = await modelRepository.UpdateAsync(id, entity, HttpContext.RequestAborted).ConfigureAwait(false);

            if (result == null)
            {
                return new NotFoundResult();
            }

            var savedContract = Contract<ModelContract>.Create(result.Id, result.Revision, result.Timestamp, modelConverter.ToContract(result.Data));
            return new ObjectResult(savedContract);
        }

        /// <summary>
        ///     Select several models by shift
        /// </summary>
        /// <param name="skip">Optional. The number of model that needs skip. 0 by default</param>
        /// <param name="limit">Optional. The number of model that needs take after skip. 10 by default</param>
        /// <returns>A 200 Ok response containing array of models in shift</returns>
        /// <response code="200">The array of models.</response>
        [HttpGet("")]
        [ProducesResponseType(typeof(Contract<ModelContract>[]), StatusCodes.Status200OK)]
        public async Task<Contract<ModelSummary>[]> Get([FromQuery] int skip = 0, [FromQuery] int limit = 10)
        {
            var entities = await modelRepository.SelectAsync(skip, limit, HttpContext.RequestAborted).ConfigureAwait(false);

            return entities.Select(x => Contract<ModelContract>.Create(x.Id, x.Revision, x.Timestamp, new ModelSummary{ Name = x.Data.Name})).ToArray();
        }

        /// <summary>
        ///     Get model by identifier
        /// </summary>
        /// <param name="id">The model identifier</param>
        /// <returns>A 200 Ok response containing the model or a 404 Not Found response if the model doesn't exist</returns>
        /// <response code="200">The model was created.</response>
        /// <response code="404">The model doesn't exist.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Contract<ModelContract>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            var entity = await modelRepository.GetAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);

            if (entity == null)
            {
                return new NotFoundResult();
            }

            var contract = Contract<ModelContract>.Create(entity.Id, entity.Revision, entity.Timestamp, modelConverter.ToContract(entity.Data));

            return new ObjectResult(contract);
        }

        /// <summary>
        ///     Delete model by identifier
        /// </summary>
        /// <param name="id">The model identifier</param>
        /// <returns>A 200 Ok response containing the newly deleted model or a 404 Not Found response if the model doesn't exist</returns>
        /// <response code="200">The model was created.</response>
        /// <response code="404">The model doesn't exist.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Contract<ModelContract>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var result = await modelRepository.RemoveAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);

            if (result == null)
            {
                return new NotFoundResult();
            }

            var contract = Contract<ModelContract>.Create(result.Id, result.Revision, result.Timestamp, modelConverter.ToContract(result.Data));

            return new ObjectResult(contract);
        }

        /// <summary>
        ///     Create new imitation task for model.
        /// </summary>
        /// <param name="id">The model identifier</param>
        /// <param name="subjectArea">The subject area that will be use in imitation process</param>
        /// <returns>
        ///     A 200 Ok response containing the newly created task, a 400 Bad Request if the model is
        ///     invalid or a 404 Not Found response if the model doesn't exist
        /// </returns>
        /// <response code="200">The task was created.</response>
        /// <response code="400">The model is invalid.</response>
        /// <response code="404">The model doesn't exist.</response>
        [HttpPost("{id}/task")]
        [ProducesResponseType(typeof(Contract<TaskContract>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ModelStateDictionary), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromRoute] Guid id, [FromBody] CreateTaskContract contract)
        {
            var model = await modelRepository.GetAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);
            if (model == null)
            {
                return new NotFoundResult();
            }

            if (contract.SubjectArea.Resources.Any(x => !model.Data.Resources.ContainsKey(x.Key)))
            {
                return new BadRequestResult();
            }

            var task = new TaskEntity
            {
                ModelId = model.Id,
                ModelRevision = model.Revision,
                State = TaskState.Open,
                Configuration = configurationConverter.ToEntity(contract.Configuration),
                SubjectArea = new SubjectAreaEntity
                {
                    Resources = model.Data.Resources.ToDictionary(x => x.Key, x => contract.SubjectArea.Resources.TryGetValue(x.Key, out var value) ? value : x.Value)
                },
                Reports = Array.Empty<ReportEntity>()
            };

            var savedTask = await taskRepository.CreateAsync(task, HttpContext.RequestAborted).ConfigureAwait(false);
            var taskContract = Contract<TaskContract>.Create(savedTask.Id, savedTask.Revision, savedTask.Timestamp, taskConverter.ToContract(savedTask.Data));
            return new ObjectResult(taskContract);
        }

        /// <summary>
        ///     Select all imitation tasks for specified model
        /// </summary>
        /// <param name="id">The model identifier</param>
        /// <returns>A 200 Ok response containing array of imitation tasks</returns>
        /// <response code="200">The array of imitation tasks.</response>
        [HttpGet("{id}/task")]
        [ProducesResponseType(typeof(Contract<TaskContract>[]), StatusCodes.Status200OK)]
        public async Task<Contract<TaskContract>[]> GetTasks([FromRoute] Guid id)
        {
            var tasks = await taskRepository.SelectAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);

            return tasks.Select(x => Contract<TaskContract>.Create(x.Id, x.Revision, x.Timestamp, taskConverter.ToContract(x.Data))).ToArray();
        }
    }
}