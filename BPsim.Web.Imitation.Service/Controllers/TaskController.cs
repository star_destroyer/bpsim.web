﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess;
using BPsim.Web.Imitation.Service.Contracts;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Reports;
using BPsim.Web.Imitation.Service.Converters.ImitationTask;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace BPsim.Web.Imitation.Service.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]", Name = "Tasks")]
    [ApiVersion("1.0")]
    public class TaskController : Controller
    {
        private readonly ITaskRepository taskRepository;
        private readonly ITaskConverter taskConverter;
        private readonly IReportRepository reportRepository;

        public TaskController(ITaskRepository taskRepository, ITaskConverter taskConverter, IReportRepository reportRepository)
        {
            this.taskRepository = taskRepository;
            this.taskConverter = taskConverter;
            this.reportRepository = reportRepository;
        }

        /// <summary>
        ///     Returns an Allow HTTP header with the allowed HTTP methods.
        /// </summary>
        /// <returns>A 200 OK response.</returns>
        /// <response code="200">The allowed HTTP methods.</response>
        [HttpOptions]
        public IActionResult Options()
        {
            HttpContext.Response.Headers.AppendCommaSeparatedValues(HeaderNames.Allow,
                                                                    HttpMethods.Get,
                                                                    HttpMethods.Options);
            return Ok();
        }

        /// <summary>
        ///     Returns an Allow HTTP header with the allowed HTTP methods for a task with the specified unique identifier.
        /// </summary>
        /// <param name="id">The task identifier</param>
        /// <returns>A 200 OK response.</returns>
        /// <response code="200">The allowed HTTP methods.</response>
        [HttpOptions("{id}")]
        public IActionResult Options([FromRoute] Guid id)
        {
            HttpContext.Response.Headers.AppendCommaSeparatedValues(HeaderNames.Allow,
                                                                    HttpMethods.Get,
                                                                    HttpMethods.Options,
                                                                    HttpMethods.Delete);
            return Ok();
        }

        /// <summary>
        ///     Select several tasks by shift
        /// </summary>
        /// <param name="skip">Optional. The number of tasks that needs skip. 0 by default</param>
        /// <param name="limit">Optional. The number of tasks that needs take after skip. 10 by default</param>
        /// <returns>A 200 Ok response containing array of tasks in shift</returns>
        /// <response code="200">The array of models.</response>
        [HttpGet("")]
        [ProducesResponseType(typeof(Contract<TaskContract>[]), StatusCodes.Status200OK)]
        public async Task<Contract<TaskContract>[]> Get([FromQuery] int skip = 0, [FromQuery] int limit = 10)
        {
            var tasks = await taskRepository.SelectAsync(skip, limit, HttpContext.RequestAborted).ConfigureAwait(false);

            return tasks.Select(x => Contract<TaskContract>.Create(x.Id, x.Revision, x.Timestamp, taskConverter.ToContract(x.Data))).ToArray();
        }

        /// <summary>
        ///     Get task by identifier
        /// </summary>
        /// <param name="id">The task identifier</param>
        /// <returns>A 200 Ok response containing the task or a 404 Not Found response if the task doesn't exist</returns>
        /// <response code="200">The task was created.</response>
        /// <response code="404">The task doesn't exist.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Contract<TaskContract>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            var task = await taskRepository.GetInfoAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);

            if (task == null)
            {
                return new NotFoundResult();
            }

            var contract = Contract<TaskContract>.Create(task.Id, task.Revision, task.Timestamp, taskConverter.ToContract(task.Data));

            return new ObjectResult(contract);

        }

        /// <summary>
        ///     Delete task by identifier
        /// </summary>
        /// <param name="id">The task identifier</param>
        /// <returns>A 200 Ok response containing the newly deleted task or a 404 Not Found response if the task doesn't exist</returns>
        /// <response code="200">The task was created.</response>
        /// <response code="404">The task doesn't exist.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Contract<TaskContract>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var task = await taskRepository.DeleteAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);

            if (task == null)
            {
                return new NotFoundResult();
            }

            var contract = Contract<TaskContract>.Create(task.Id, task.Revision, task.Timestamp, taskConverter.ToContract(task.Data));

            return new ObjectResult(contract);
        }
        
        /// <summary>
        ///     Select reports by task identifier
        /// </summary>
        /// <param name="id">The task identifier</param>
        /// <returns>A 200 Ok response containing the task reports or a 404 Not Found response if the task doesn't exist</returns>
        /// <response code="200">The reports.</response>
        /// <response code="404">The task doesn't exist.</response>
        [HttpGet("{id}/report")]
        [ProducesResponseType(typeof(ReportContract[]), StatusCodes.Status200OK)]
        public async Task<IActionResult> SelectReports([FromRoute] Guid id)
        {
            var reports = await reportRepository.SelectAsync(id, HttpContext.RequestAborted).ConfigureAwait(false);
            if (reports == null)
            {
                return new NotFoundResult();
            }

            return new ObjectResult(reports);
        }
    }
}