﻿using Autofac;
using BPsim.Web.Imitation.Service.Converters.ImitationTask;
using BPsim.Web.Imitation.Service.Converters.Model;
using BPsim.Web.Imitation.Service.Converters.Reports;

namespace BPsim.Web.Imitation.Service.Converters
{
    public class ConvertersModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RuleConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<KnowledgeConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<NodeConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ModelConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TaskStateConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<SubjectAreaConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<TaskConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ImitationStepConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ConfigurationConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ReportConverter>().AsImplementedInterfaces().SingleInstance();
        }
    }
}