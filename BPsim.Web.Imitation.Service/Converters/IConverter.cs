﻿namespace BPsim.Web.Imitation.Service.Converters
{
    public interface IConverter<TContract, TEntity>
    {
        TEntity ToEntity(TContract contract);

        TContract ToContract(TEntity entity);
    }
}