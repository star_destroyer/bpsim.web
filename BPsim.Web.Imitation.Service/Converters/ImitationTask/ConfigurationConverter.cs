﻿using System.Linq;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;

namespace BPsim.Web.Imitation.Service.Converters.ImitationTask
{
    public class ConfigurationConverter : IConfigurationConverter
    {
        private readonly IImitationStepConverter imitationStepConverter;

        public ConfigurationConverter(IImitationStepConverter imitationStepConverter)
        {
            this.imitationStepConverter = imitationStepConverter;
        }

        public ConfigurationEntity ToEntity(ConfigurationContract contract)
        {
            return new ConfigurationEntity
            {
                EndingRule = contract.EndingRule,
                Steps = contract.Steps.Select(x => imitationStepConverter.ToEntity(x)).ToArray()
            };
        }

        public ConfigurationContract ToContract(ConfigurationEntity entity)
        {
            return new ConfigurationContract
            {
                EndingRule = entity.EndingRule,
                Steps = entity.Steps.Select(x => imitationStepConverter.ToContract(x)).ToArray()
            };
        }
    }
}