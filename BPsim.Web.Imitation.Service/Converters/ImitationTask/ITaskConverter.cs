﻿using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;

namespace BPsim.Web.Imitation.Service.Converters.ImitationTask
{
    public interface ITaskConverter : IConverter<TaskContract, TaskInfoProjection>
    {
    }
}