﻿using System;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;

namespace BPsim.Web.Imitation.Service.Converters.ImitationTask
{
    public class ImitationStepConverter : IImitationStepConverter
    {
        public ImitationStep ToEntity(ImitationStepContact contract)
        {
            switch (contract)
            {
                case ImitationStepContact.FreeNodes:
                    return ImitationStep.Free;
                case ImitationStepContact.BusyNodes:
                    return ImitationStep.Busy;
                case ImitationStepContact.Agents:
                    return ImitationStep.Agents;
                default:
                    throw new ArgumentOutOfRangeException(nameof(contract), contract, null);
            }
        }

        public ImitationStepContact ToContract(ImitationStep entity)
        {
            switch (entity)
            {
                case ImitationStep.Free:
                    return ImitationStepContact.FreeNodes;
                case ImitationStep.Busy:
                    return ImitationStepContact.BusyNodes;
                case ImitationStep.Agents:
                    return ImitationStepContact.Agents;
                default:
                    throw new ArgumentOutOfRangeException(nameof(entity), entity, null);
            }
        }
    }
}