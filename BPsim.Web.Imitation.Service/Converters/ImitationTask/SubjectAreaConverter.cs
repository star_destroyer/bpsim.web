﻿using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;

namespace BPsim.Web.Imitation.Service.Converters.ImitationTask
{
    public class SubjectAreaConverter : ISubjectAreaConverter
    {
        public SubjectAreaEntity ToEntity(SubjectAreaContract contract)
        {
            return new SubjectAreaEntity
            {
                Resources = contract.Resources
            };
        }

        public SubjectAreaContract ToContract(SubjectAreaEntity entity)
        {
            return new SubjectAreaContract
            {
                Resources = entity.Resources
            };
        }
    }
}