﻿using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;

namespace BPsim.Web.Imitation.Service.Converters.ImitationTask
{
    public class TaskConverter : ITaskConverter
    {
        private readonly ITaskStateConverter taskStateConverter;
        private readonly ISubjectAreaConverter subjectAreaConverter;
        private readonly IConfigurationConverter configurationConverter;

        public TaskConverter(ITaskStateConverter taskStateConverter, ISubjectAreaConverter subjectAreaConverter, IConfigurationConverter configurationConverter)
        {
            this.taskStateConverter = taskStateConverter;
            this.subjectAreaConverter = subjectAreaConverter;
            this.configurationConverter = configurationConverter;
        }

        public TaskInfoProjection ToEntity(TaskContract contract)
        {
            return new TaskInfoProjection
            {
                ModelId = contract.ModelId,
                ModelRevision = contract.ModelRevision,
                State = taskStateConverter.ToEntity(contract.State),
                Configuration = configurationConverter.ToEntity(contract.Configuration),
                SubjectArea = subjectAreaConverter.ToEntity(contract.SubjectArea)
            };
        }

        public TaskContract ToContract(TaskInfoProjection entity)
        {
            return new TaskContract
            {
                ModelId = entity.ModelId,
                ModelRevision = entity.ModelRevision,
                State = taskStateConverter.ToContract(entity.State),
                Configuration = configurationConverter.ToContract(entity.Configuration),
                SubjectArea = subjectAreaConverter.ToContract(entity.SubjectArea)
            };
        }
    }
}