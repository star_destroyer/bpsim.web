﻿using System;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.ImitationTask;

namespace BPsim.Web.Imitation.Service.Converters.ImitationTask
{
    public class TaskStateConverter : ITaskStateConverter
    {
        public TaskState ToEntity(TaskStateContract contract)
        {
            switch (contract)
            {
                case TaskStateContract.Open:
                    return TaskState.Open;
                case TaskStateContract.Scheduled:
                    return TaskState.Scheduled;
                case TaskStateContract.Pending:
                    return TaskState.Pending;
                case TaskStateContract.Failed:
                    return TaskState.Failed;
                case TaskStateContract.Successfull:
                    return TaskState.Successfull;
                case TaskStateContract.Aborted:
                    return TaskState.Aborted;
                case TaskStateContract.Deleted:
                    return TaskState.Deleted;
                default:
                    throw new ArgumentOutOfRangeException(nameof(contract), contract, null);
            }
        }

        public TaskStateContract ToContract(TaskState entity)
        {
            switch (entity)
            {
                case TaskState.Open:
                    return TaskStateContract.Open;
                case TaskState.Scheduled:
                    return TaskStateContract.Scheduled;
                case TaskState.Pending:
                    return TaskStateContract.Pending;
                case TaskState.Failed:
                    return TaskStateContract.Failed;
                case TaskState.Successfull:
                    return TaskStateContract.Successfull;
                case TaskState.Aborted:
                    return TaskStateContract.Aborted;
                case TaskState.Deleted:
                    return TaskStateContract.Deleted;
                default:
                    throw new ArgumentOutOfRangeException(nameof(entity), entity, null);
            }
        }
    }
}