﻿using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Service.Contracts.Model;

namespace BPsim.Web.Imitation.Service.Converters.Model
{
    public interface IModelConverter : IConverter<ModelContract, ModelEntity>
    {
    }
}