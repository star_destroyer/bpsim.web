﻿using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Service.Contracts.Model;

namespace BPsim.Web.Imitation.Service.Converters.Model
{
    public interface INodeConverter : IConverter<INodeContract, INodeEntity>
    {
    }
}