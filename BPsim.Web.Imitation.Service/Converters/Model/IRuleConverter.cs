﻿using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;
using BPsim.Web.Imitation.Service.Contracts.Model.Rules;

namespace BPsim.Web.Imitation.Service.Converters.Model
{
    public interface IRuleConverter : IConverter<IRuleContract, IRuleEntity>
    {
    }
}