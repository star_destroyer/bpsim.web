﻿using System.Linq;
using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Service.Contracts.Model;

namespace BPsim.Web.Imitation.Service.Converters.Model
{
    public class KnowledgeConverter : IKnowledgeConverter
    {
        private readonly IRuleConverter ruleConverter;

        public KnowledgeConverter(IRuleConverter ruleConverter)
        {
            this.ruleConverter = ruleConverter;
        }

        public KnowledgeEntity ToEntity(KnowledgeContract contract)
        {
            return new KnowledgeEntity
            {
                Rules = contract.Rules.Select(ruleConverter.ToEntity).ToArray(),
            };
        }

        public KnowledgeContract ToContract(KnowledgeEntity entity)
        {
            return new KnowledgeContract
            {
                Rules = entity.Rules.Select(ruleConverter.ToContract).ToArray(),
            };
        }
    }
}