﻿using System.Linq;
using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Service.Contracts.Model;

namespace BPsim.Web.Imitation.Service.Converters.Model
{
    public class ModelConverter : IModelConverter
    {
        private readonly INodeConverter nodeConverter;

        public ModelConverter(INodeConverter nodeConverter)
        {
            this.nodeConverter = nodeConverter;
        }

        public ModelEntity ToEntity(ModelContract contract)
        {
            return new ModelEntity
            {
                Name = contract.Name,
                Resources = contract.Resources,
                Orders = contract.Orders,
                Nodes = contract.Nodes.ToDictionary(x => x.Key, x => nodeConverter.ToEntity(x.Value))
            };
        }

        public ModelContract ToContract(ModelEntity entity)
        {
            return new ModelContract
            {
                Name = entity.Name,
                Resources = entity.Resources,
                Orders = entity.Orders,
                Nodes = entity.Nodes.ToDictionary(x => x.Key, x => nodeConverter.ToContract(x.Value))
            };
        }
    }
}