﻿using System;
using System.Linq;
using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Service.Contracts.Model;

namespace BPsim.Web.Imitation.Service.Converters.Model
{
    public class NodeConverter : INodeConverter
    {
        private readonly IRuleConverter ruleConverter;
        private readonly IKnowledgeConverter knowledgeConverter;

        public NodeConverter(IRuleConverter ruleConverter,
                             IKnowledgeConverter knowledgeConverter)
        {
            this.ruleConverter = ruleConverter;
            this.knowledgeConverter = knowledgeConverter;
        }

        public INodeEntity ToEntity(INodeContract contract)
        {
            switch (contract)
            {
                case AgentContract agentContract:
                    return new AgentEntity
                    {
                        GlobalRules = agentContract.GlobalRules.Select(ruleConverter.ToEntity).ToArray(),
                        Knowledges = agentContract.Knowledges.ToDictionary(x => x.Key, x => knowledgeConverter.ToEntity(x.Value))
                    };
                case OperationContract operationContract:
                    return new OperationEntity
                    {
                        InRules = operationContract.InRules.Select(ruleConverter.ToEntity).ToArray(),
                        OutRules = operationContract.OutRules.Select(ruleConverter.ToEntity).ToArray(),
                        Duration = operationContract.Duration
                    };
                default:
                    throw new ArgumentOutOfRangeException(nameof(contract));
            }
        }

        public INodeContract ToContract(INodeEntity entity)
        {
            switch (entity)
            {
                case AgentEntity agentEntity:
                    return new AgentContract
                    {
                        GlobalRules = agentEntity.GlobalRules.Select(ruleConverter.ToContract).ToArray(),
                        Knowledges = agentEntity.Knowledges.ToDictionary(x => x.Key, x => knowledgeConverter.ToContract(x.Value))
                    };
                case OperationEntity operationEntity:
                    return new OperationContract
                    {
                        InRules = operationEntity.InRules.Select(ruleConverter.ToContract).ToArray(),
                        OutRules = operationEntity.OutRules.Select(ruleConverter.ToContract).ToArray(),
                        Duration = operationEntity.Duration
                    };
                default:
                    throw new ArgumentOutOfRangeException(nameof(entity));
            }
        }
    }
}