﻿using System;
using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;
using BPsim.Web.Imitation.Service.Contracts.Model.Rules;

namespace BPsim.Web.Imitation.Service.Converters.Model
{
    public class RuleConverter : IRuleConverter
    {
        public IRuleEntity ToEntity(IRuleContract contract)
        {
            switch (contract)
            {
                case ExpressionRuleContract expressionRuleContract:
                    return new ExpressionRuleEntity {Expression = expressionRuleContract.Expression};
                case CreateOrderRuleContract createRuleContract:
                    return new CreateOrderRuleEntity {OrderName = createRuleContract.OrderName};
                case RemoveOrderRuleContract removeRuleContract:
                    return new RemoveOrderRuleEntity {OrderName = removeRuleContract.OrderName};
                case SelectOrderRuleContract selectRuleContract:
                    return new SelectOrderRuleEntity {OrderName = selectRuleContract.OrderName, TargetName = selectRuleContract.TargetName};
                case SendOrderRuleContract sendRuleContract:
                    return new SendOrderRuleEntity {OrderName = sendRuleContract.OrderName, TargetName = sendRuleContract.TargetName};
                default:
                    throw new ArgumentOutOfRangeException(nameof(contract));
            }
        }

        public IRuleContract ToContract(IRuleEntity entity)
        {
            switch (entity)
            {
                case ExpressionRuleEntity expressionRuleEntity:
                    return new ExpressionRuleContract {Expression = expressionRuleEntity.Expression};
                case CreateOrderRuleEntity createRuleEntity:
                    return new CreateOrderRuleContract {OrderName = createRuleEntity.OrderName};
                case RemoveOrderRuleEntity removeRuleEntity:
                    return new RemoveOrderRuleContract {OrderName = removeRuleEntity.OrderName};
                case SelectOrderRuleEntity selectRuleEntity:
                    return new SelectOrderRuleContract {OrderName = selectRuleEntity.OrderName, TargetName = selectRuleEntity.TargetName};
                case SendOrderRuleEntity sendRuleEntity:
                    return new SendOrderRuleContract {OrderName = sendRuleEntity.OrderName, TargetName = sendRuleEntity.TargetName};
                default:
                    throw new ArgumentOutOfRangeException(nameof(entity));
            }
        }
    }
}