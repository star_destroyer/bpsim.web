﻿using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Reports;

namespace BPsim.Web.Imitation.Service.Converters.Reports
{
    public interface IReportConverter: IConverter<ReportContract, ReportEntity>
    {
        
    }
}