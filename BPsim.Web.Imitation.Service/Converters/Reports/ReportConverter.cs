﻿using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Contracts.Reports;

namespace BPsim.Web.Imitation.Service.Converters.Reports
{
    public class ReportConverter : IReportConverter
    {
        public ReportEntity ToEntity(ReportContract contract)
        {
            return new ReportEntity
            {
                Tick = contract.Tick,
                Agents = contract.Agents,
                Operations = contract.Operations,
                Orders = contract.Orders,
                Resources = contract.Resources
            };
        }

        public ReportContract ToContract(ReportEntity entity)
        {
            return new ReportContract
            {
                Tick = entity.Tick,
                Agents = entity.Agents,
                Operations = entity.Operations,
                Orders = entity.Orders,
                Resources = entity.Resources
            };
        }
    }
}