﻿using System.Collections.Generic;
using System.Threading;
using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Executing.Contexts;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IImitationContextFactory
    {
        IContext Create(IDictionary<string, decimal[]> resources,
                        IDictionary<string, IDictionary<string, string>> orders,
                        IDictionary<string, INodeEntity> nodes,
                        CancellationToken cancellation);
    }
}