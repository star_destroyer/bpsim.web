﻿using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IImitationPipelineStepConverter
    {
        ImitationPipelineStep ToPipelineStep(ImitationStep entity);
    }
}