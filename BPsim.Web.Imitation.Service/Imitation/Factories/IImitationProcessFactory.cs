﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IImitationProcessFactory
    {
        Task CreateImitation(Guid taskId, TaskInfoProjection taskInfo, CancellationToken cancellation);
    }
}