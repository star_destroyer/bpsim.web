﻿using System;
using System.Collections.Generic;
using System.Threading;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.DataAccess.Data.Model;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IImitationTaskFactory
    {
        ImitationTask Create(Guid id,
                             Guid modelId,
                             IDictionary<string, decimal[]> resources,
                             IDictionary<string, IDictionary<string, string>> orders,
                             IDictionary<string, INodeEntity> nodes,
                             ConfigurationEntity configurationEntity,
                             CancellationToken cancellation);
    }
}