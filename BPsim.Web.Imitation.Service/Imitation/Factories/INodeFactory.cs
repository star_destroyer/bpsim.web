﻿using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface INodeFactory
    {
        Node Create(string name, INodeEntity nodeEntity);
    }
}