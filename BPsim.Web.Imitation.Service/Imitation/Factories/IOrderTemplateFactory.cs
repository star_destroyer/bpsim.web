﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IOrderTemplateFactory
    {
        OrderTemplate Create(IDictionary<string, string> order);
    }
}