﻿using System;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IReportHandlerFactory
    {
        ReportHandler Create(Guid taskId);
    }
}