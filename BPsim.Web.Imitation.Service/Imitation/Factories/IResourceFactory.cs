﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IResourceFactory
    {
        Resource Create(decimal[] values);
    }
}