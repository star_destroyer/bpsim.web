﻿using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;
using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public interface IRuleFactory
    {
        IRule Create(IRuleEntity ruleEntity);
    }
}