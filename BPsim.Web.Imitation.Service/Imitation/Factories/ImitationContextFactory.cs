﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Dispatchers;
using BPsim.Web.Imitation.Dispatchers.Nodes;
using BPsim.Web.Imitation.Dispatchers.Relationships;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class ImitationContextFactory : IImitationContextFactory
    {
        private readonly INodeFactory nodeFactory;
        private readonly IOrderTemplateFactory orderTemplateFactory;
        private readonly IResourceFactory resourceFactory;

        public ImitationContextFactory(INodeFactory nodeFactory,
                                       IOrderTemplateFactory orderTemplateFactory,
                                       IResourceFactory resourceFactory)
        {
            this.nodeFactory = nodeFactory;
            this.orderTemplateFactory = orderTemplateFactory;
            this.resourceFactory = resourceFactory;
        }

        public IContext Create(IDictionary<string, decimal[]> resources,
                               IDictionary<string, IDictionary<string, string>> orders,
                               IDictionary<string, INodeEntity> nodes,
                               CancellationToken cancellation)
        {
            return new Context
            {
                CancellationToken = cancellation,
                NodeDispatcher = new NodeDispatcher(nodes.Select(x => nodeFactory.Create(x.Key, x.Value)).ToArray()),
                OrderDispatcher = new OrderDispatcher(orders.Keys.ToArray()),
                OrderOwningDispatcher = new OrderOwningDispatcher(nodes.Keys.ToArray()),
                OrderTemplateDispatcher = new OrderTemplateDispatcher(orders.ToDictionary(x => x.Key, x => orderTemplateFactory.Create(x.Value))),
                ResourceDispatcher = new ResourceDispatcher(resources.ToDictionary(x => x.Key, x => resourceFactory.Create(x.Value))),
                Reporter = new Reporter()
            };
        }
    }
}