﻿using System;
using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class ImitationPipelineStepConverter : IImitationPipelineStepConverter
    {
        public ImitationPipelineStep ToPipelineStep(ImitationStep entity)
        {
            switch (entity)
            {
                case ImitationStep.Free:
                    return ImitationPipelineStep.ExecuteFreeNode;
                case ImitationStep.Busy:
                    return ImitationPipelineStep.ExecuteBusyNode;
                case ImitationStep.Agents:
                    return ImitationPipelineStep.ExecuteAgents;
                default:
                    throw new ArgumentOutOfRangeException(nameof(entity), entity, null);
            }
        }
    }
}