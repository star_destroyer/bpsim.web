﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Service.Utils;
using Serilog;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class ImitationProcessFactory : IImitationProcessFactory
    {
        private readonly ILogger logger;
        private readonly ITaskRepository taskRepository;
        private readonly IModelRepository modelRepository;
        private readonly IImitationTaskFactory imitationTaskFactory;
        private readonly IImitationEngine imitationEngine;
        private readonly IReportHandlerFactory reportHandlerFactory;

        public ImitationProcessFactory(ILogger logger,
                                       ITaskRepository taskRepository,
                                       IModelRepository modelRepository,
                                       IImitationTaskFactory imitationTaskFactory,
                                       IImitationEngine imitationEngine,
                                       IReportHandlerFactory reportHandlerFactory)
        {
            this.logger = logger;
            this.taskRepository = taskRepository;
            this.modelRepository = modelRepository;
            this.imitationTaskFactory = imitationTaskFactory;
            this.imitationEngine = imitationEngine;
            this.reportHandlerFactory = reportHandlerFactory;
        }

        public Task CreateImitation(Guid taskId, TaskInfoProjection taskInfo, CancellationToken cancellation)
        {
            return Task.Run(
                            async () =>
                            {
                                using (logger.BeginTimedOperation("Run task {TaskId}", taskId))
                                using (cancellation.Register(() => taskRepository.SetTaskState(taskId, TaskState.Aborted, CancellationToken.None)))
                                {
                                    try
                                    {
                                        await taskRepository.SetTaskState(taskId, TaskState.Pending, cancellation).ConfigureAwait(false);
                                        var model = await modelRepository.GetAsync(taskInfo.ModelId, cancellation).ConfigureAwait(false);

                                        if (model.Revision != taskInfo.ModelRevision)
                                        {
                                            throw new ArgumentOutOfRangeException($"Task model revision and model revision don't equals. Task Revision: {taskInfo.ModelRevision} != Model Revision: {model.Revision}");
                                        }

                                        var imitationTask = imitationTaskFactory.Create(taskId, model.Id, taskInfo.SubjectArea.Resources, model.Data.Orders, model.Data.Nodes, taskInfo.Configuration, cancellation);

                                        using (logger.BeginTimedOperation("Imitate task {TaskId}", taskId))
                                        using (var reportHandler = reportHandlerFactory.Create(taskId))
                                        {
                                            await imitationEngine.Run(imitationTask, reportHandler.OnReportAsync, cancellation).ConfigureAwait(false);
                                        }

                                        await taskRepository.SetTaskState(taskId, TaskState.Successfull, cancellation).ConfigureAwait(false);
                                    } catch (Exception e)
                                    {
                                        logger.Error(e, "Error occurred when imitate task {TaskId}", taskId);
                                        await taskRepository.SetTaskState(taskId, TaskState.Failed, cancellation).ConfigureAwait(false);
                                        throw;
                                    }
                                }
                            },
                            cancellation);
        }
    }
}