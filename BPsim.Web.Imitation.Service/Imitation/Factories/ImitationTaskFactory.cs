﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using BPsim.Web.Imitation.Builders;
using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.DataAccess.Data.Model;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class ImitationTaskFactory : IImitationTaskFactory
    {
        private readonly IImitationContextFactory imitationContextFactory;
        private readonly IExpressionFactory expressionFactory;
        private readonly IImitationPipelineStepConverter pipelineStepConverter;

        public ImitationTaskFactory(IImitationContextFactory imitationContextFactory,
                                    IExpressionFactory expressionFactory,
                                    IImitationPipelineStepConverter pipelineStepConverter)
        {
            this.imitationContextFactory = imitationContextFactory;
            this.expressionFactory = expressionFactory;
            this.pipelineStepConverter = pipelineStepConverter;
        }

        public ImitationTask Create(Guid id,
                                    Guid modelId,
                                    IDictionary<string, decimal[]> resources,
                                    IDictionary<string, IDictionary<string, string>> orders,
                                    IDictionary<string, INodeEntity> nodes,
                                    ConfigurationEntity configurationEntity,
                                    CancellationToken cancellation)
        {
            return new ImitationTask
            {
                Id = id,
                ModelId = modelId,
                Context = imitationContextFactory.Create(resources, orders, nodes, cancellation),
                Configuration = new ImitationConfiguration
                {
                    EndExpression = expressionFactory.Create(configurationEntity.EndingRule),
                    PipelineSteps = configurationEntity.Steps.Select(pipelineStepConverter.ToPipelineStep).ToArray()
                }
            };
        }
    }
}