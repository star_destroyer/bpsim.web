﻿using System;
using System.Linq;
using BPsim.Web.Imitation.DataAccess.Data.Model;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Factories;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class NodeFactory : INodeFactory
    {
        private readonly IRuleFactory ruleFactory;
        private readonly IExpressionRuleFactory expressionRuleFactory;

        public NodeFactory(IRuleFactory ruleFactory,
                           IExpressionRuleFactory expressionRuleFactory)
        {
            this.ruleFactory = ruleFactory;
            this.expressionRuleFactory = expressionRuleFactory;
        }

        public Node Create(string name, INodeEntity nodeEntity)
        {
            switch (nodeEntity)
            {
                case AgentEntity agentEntity:
                    return new Agent(name)
                    {
                        GlobalRules = agentEntity.GlobalRules.Select(x => ruleFactory.Create(x)).ToArray(),
                        Knowledges = agentEntity.Knowledges.ToDictionary(x => x.Key,
                                                                         x => new Knowledge
                                                                         {
                                                                             Rules = x.Value.Rules.Select(y => ruleFactory.Create(y)).ToArray(),
                                                                         }),
                        IsManyOwner = true //TODO (byTimo) этого нет в контракте
                    };
                case OperationEntity operationEntity:
                    return new Operation(name)
                    {
                        InRules = operationEntity.InRules.Select(x => ruleFactory.Create(x)).ToArray(),
                        OutRules = operationEntity.OutRules.Select(x => ruleFactory.Create(x)).ToArray(),
                        Duration = expressionRuleFactory.Create(operationEntity.Duration) as ExpressionRule,
                        IsManyOwner = true //TODO (byTimo) этого нет в контракте
                    };
                default:
                    throw new ArgumentOutOfRangeException(nameof(nodeEntity), nodeEntity, "");
            }
        }
    }
}