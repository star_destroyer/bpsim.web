﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class OrderTemplateFactory : IOrderTemplateFactory
    {
        public OrderTemplate Create(IDictionary<string, string> order)
        {
            return new OrderTemplate
            {
                DefaultProps = order.ToDictionary(x => x.Key, x => decimal.Parse(x.Value, NumberStyles.Any, NumberFormatInfo.InvariantInfo)),
                IsManyOwnered = false //TODO (byTimo) этого нет в контракте
            };
        }
    }
}