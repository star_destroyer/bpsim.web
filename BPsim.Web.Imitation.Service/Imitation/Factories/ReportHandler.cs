﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.DataAccess;
using BPsim.Web.Imitation.DataAccess.Data.ImitationTask;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class ReportHandler : IDisposable
    {
        private readonly Guid taskId;
        private readonly IReportRepository reportRepository;
        private readonly List<ReportEntity> cachedReport;

        public ReportHandler(Guid taskId, IReportRepository reportRepository, int cachedReportCount)
        {
            this.taskId = taskId;
            this.reportRepository = reportRepository;
            cachedReport = new List<ReportEntity>(cachedReportCount);
        }

        public async Task OnReportAsync(int tick, Report report, CancellationToken cancellation)
        {
            cachedReport.Add(new ReportEntity
            {
                Tick = tick,
                Operations = report.Operations,
                Agents = report.Agents,
                Orders = report.Orders,
                Resources = report.Resources
            });

            if (cachedReport.Count == cachedReport.Capacity)
            {
                await FlushAsync(cancellation).ConfigureAwait(false);
            }
        }

        public void Dispose()
        {
            FlushAsync(CancellationToken.None).GetAwaiter().GetResult();
        }

        private async Task FlushAsync(CancellationToken cancellation)
        {
            await reportRepository.SaveAsync(taskId, cachedReport, cancellation).ConfigureAwait(false);
            cachedReport.Clear();
        }
    }
}