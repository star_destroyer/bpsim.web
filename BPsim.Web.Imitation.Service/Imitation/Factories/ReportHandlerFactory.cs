﻿using System;
using BPsim.Web.Imitation.DataAccess;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class ReportHandlerFactory : IReportHandlerFactory
    {
        private readonly IReportRepository reportRepository;
        private readonly IImitationSchedulerConfiguration schedulerConfiguration;

        public ReportHandlerFactory(IReportRepository reportRepository,
                                    IImitationSchedulerConfiguration schedulerConfiguration)
        {
            this.reportRepository = reportRepository;
            this.schedulerConfiguration = schedulerConfiguration;
        }

        public ReportHandler Create(Guid taskId)
        {
            return new ReportHandler(taskId, reportRepository, schedulerConfiguration.CachedReportCount);
        }
    }
}