﻿using System;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    //TODO (byTimo) а если bool
    public class ResourceFactory : IResourceFactory
    {
        public Resource Create(decimal[] values)
        {
            switch (values.Length)
            {
                case 1:
                    return new Resource(values[0]);
                case 2:
                    return new Resource(values[0], values[1]);
                case 3:
                    return new Resource(values[0], values[1], values[2]);
                default:
                    throw new ArgumentOutOfRangeException(nameof(values), values, "");
            }
        }
    }
}