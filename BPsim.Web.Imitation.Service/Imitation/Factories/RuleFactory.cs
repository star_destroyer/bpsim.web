﻿using System;
using BPsim.Web.Imitation.DataAccess.Data.Model.Rules;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Factories;

namespace BPsim.Web.Imitation.Service.Imitation.Factories
{
    public class RuleFactory : IRuleFactory
    {
        private readonly IExpressionRuleFactory expressionRuleFactory;

        public RuleFactory(IExpressionRuleFactory expressionRuleFactory)
        {
            this.expressionRuleFactory = expressionRuleFactory;
        }

        public IRule Create(IRuleEntity ruleEntity)
        {
            switch (ruleEntity)
            {
                case CreateOrderRuleEntity createOrderRuleEntity:
                    return new CreateOrderRule(createOrderRuleEntity.OrderName);
                case ExpressionRuleEntity expressionRuleEntity:
                    return expressionRuleFactory.Create(expressionRuleEntity.Expression);
                case RemoveOrderRuleEntity removeOrderRuleEntity:
                    return new RemoveOrderRule(removeOrderRuleEntity.OrderName);
                case SelectOrderRuleEntity selectOrderRuleEntity:
                    return new SelectOrderRule(selectOrderRuleEntity.OrderName, selectOrderRuleEntity.TargetName);
                case SendOrderRuleEntity sendOrderRuleEntity:
                    return new SendOrderRule(sendOrderRuleEntity.OrderName, sendOrderRuleEntity.TargetName);
                default:
                    throw new ArgumentOutOfRangeException(nameof(ruleEntity), ruleEntity, "");
            }
        }
    }
}