﻿using System.Threading;

namespace BPsim.Web.Imitation.Service.Imitation
{
    public interface IImitationScheduler
    {
        void RunAsync(CancellationToken cancellation);
    }
}