﻿namespace BPsim.Web.Imitation.Service.Imitation
{
    public interface IImitationSchedulerConfiguration
    {
        int MaxImitationProcessCount { get; }

        double SchedulingInterval { get; }

        int CachedReportCount { get; }
    }
}