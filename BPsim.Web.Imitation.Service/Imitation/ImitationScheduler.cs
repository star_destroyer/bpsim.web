﻿using System.Threading;
using BPsim.Web.Imitation.DataAccess;
using BPsim.Web.Imitation.Service.Imitation.Factories;
using BPsim.Web.Imitation.Service.Utils;
using Serilog;
using Timer = System.Timers.Timer;

namespace BPsim.Web.Imitation.Service.Imitation
{
    public class ImitationScheduler : IImitationScheduler
    {
        private readonly ILogger logger;
        private readonly IImitationSchedulerConfiguration configuration;
        private readonly ITaskRepository taskRepository;
        private readonly IImitationProcessFactory imitationProcessFactory;

        private CancellationTokenSource internalCancellationTokenSource;
        private Timer scheduleTimer;
        private int imitationCount;

        public ImitationScheduler(ILogger logger,
                                  IImitationSchedulerConfiguration configuration,
                                  ITaskRepository taskRepository,
                                  IImitationProcessFactory imitationProcessFactory)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.taskRepository = taskRepository;
            this.imitationProcessFactory = imitationProcessFactory;
        }

        public void RunAsync(CancellationToken cancellation)
        {
            internalCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellation);

            scheduleTimer = new Timer
            {
                Interval = configuration.SchedulingInterval,
                AutoReset = false,
                Enabled = false
            };

            scheduleTimer.Elapsed += async (sender, args) =>
            {
                using (logger.BeginTimedOperation("Schedule imitation"))
                {
                    while (imitationCount < configuration.MaxImitationProcessCount)
                    {
                        var task = await taskRepository.ScheduleAsync(internalCancellationTokenSource.Token).ConfigureAwait(false);
                        if (task == null)
                        {
                            break;
                        }

                        logger.Information("Schedule task {TaskId}", task.Id);
#pragma warning disable 4014
                        imitationProcessFactory.CreateImitation(task.Id, task.Data, cancellation)
                                               .ContinueWith(t => Interlocked.Decrement(ref imitationCount), cancellation);
#pragma warning restore 4014
                        Interlocked.Increment(ref imitationCount);
                    }

                    scheduleTimer.Start();
                }
            };

            cancellation.Register(x => ((Timer) x).Dispose(), scheduleTimer, false);

            scheduleTimer.Start();
        }
    }
}