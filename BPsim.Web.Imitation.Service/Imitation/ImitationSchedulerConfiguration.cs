﻿namespace BPsim.Web.Imitation.Service.Imitation
{
    public class ImitationSchedulerConfiguration : IImitationSchedulerConfiguration
    {
        public int MaxImitationProcessCount { get; set; }

        public double SchedulingInterval { get; set; }

        public int CachedReportCount { get; set; }
    }
}