﻿using System;
using System.IO;
using System.Text;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace BPsim.Web.Imitation.Service
{
    public sealed class Program
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Log.Logger = new LoggerConfiguration()
                         .MinimumLevel.Debug()
                         .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                         .Enrich.FromLogContext()
                         .WriteTo.Console()
                         .CreateLogger();

            Log.Information("Starting web host");

            try
            {
                var configuration = new ConfigurationBuilder()
                                    .AddEnvironmentVariables()
                                    .AddCommandLine(args)
                                    .Build();

                var host = new WebHostBuilder()
                           .UseConfiguration(configuration)
                           .UseContentRoot(Directory.GetCurrentDirectory())
                           .ConfigureServices(services => services.AddAutofac())
                           .UseKestrel(
                                       options =>
                                       {
                                           // Do not add the Server HTTP header when using the Kestrel Web Server.
                                           options.AddServerHeader = false;
                                       })
                           .UseIISIntegration()
                           .UseStartup<Startup>()
                           .UseUrls("http://*:16092")
                           .UseSerilog()
                           .Build();

                host.Run();
            } catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                throw ex;
            } finally
            {
                Log.CloseAndFlush();
            }
        }
    }
}