﻿using System;
using System.Linq;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacSerilogIntegration;
using BPsim.Web.Analysis.Autofac;
using BPsim.Web.Imitation.Autofac;
using BPsim.Web.Imitation.Client;
using BPsim.Web.Imitation.DataAccess.Autofac;
using BPsim.Web.Imitation.Service.Converters;
using BPsim.Web.Imitation.Service.Imitation;
using BPsim.Web.Imitation.Service.Imitation.Factories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace BPsim.Web.Imitation.Service
{
    public class Startup : IStartup
    {
        private readonly IConfigurationRoot configuration;

        public Startup(IHostingEnvironment hostingEnvironment)
        {
            configuration = new ConfigurationBuilder()
                            .SetBasePath(hostingEnvironment.ContentRootPath)
                            .AddJsonFile("appsettings.json")
                            .AddJsonFile($"appsettings.{hostingEnvironment.EnvironmentName}.json", true)
                            .AddEnvironmentVariables()
                            .Build();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services
                .AddSingleton<IConfiguration>(configuration)
                .AddRouting(options =>
                {
                    options.AppendTrailingSlash = true;
                    options.LowercaseUrls = true;
                })
                .AddApiVersioning(x =>
                {
                    x.AssumeDefaultVersionWhenUnspecified = true;
                    x.DefaultApiVersion = new ApiVersion(1, 0);
                })
                .AddSwagger()
                .AddMvcCore()
                .AddApiExplorer()
                .AddAuthorization()
                .AddFormatterMappings()
                .AddDataAnnotations()
                .AddJsonFormatters()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                    options.SerializerSettings.Converters.Add(new NodeContractJsonConverter());
                    options.SerializerSettings.Converters.Add(new RuleContractJsonConverter());
                })
                .AddCors(options => options.AddPolicy("AllowAny", x1 => x1.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()))
                .AddVersionedApiExplorer();

            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.Register(x => new MongoClient(configuration.GetConnectionString("Mongo")).GetDatabase("BPsimWebImitation"))
                   .AsImplementedInterfaces()
                   .SingleInstance();
            
            builder.RegisterModule<BPsimWebImitationModule>();
            builder.RegisterModule<BPsimWebAnalysisModule>();
            builder.RegisterModule<BPsimWebImitationDataAccessModule>();
            builder.RegisterModule<ConvertersModule>();

            builder.RegisterLogger();

            builder.Register(x => x.Resolve<IConfiguration>().GetSection("Imitation").Get<ImitationSchedulerConfiguration>())
                   .AsImplementedInterfaces()
                   .SingleInstance();

            builder.RegisterType<ImitationScheduler>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ImitationProcessFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ImitationTaskFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ImitationContextFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<NodeFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<OrderTemplateFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ResourceFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RuleFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ImitationPipelineStepConverter>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<ReportHandlerFactory>().AsImplementedInterfaces().SingleInstance();

            return new AutofacServiceProvider(builder.Build());
        }

        public void Configure(IApplicationBuilder application)
        {
            application
                .UseCors("AllowAny")
                .UseMvc()
                .UseSwagger()
                .UseSwaggerUI(options =>
                {
                    var provider = application.ApplicationServices.GetService<IApiVersionDescriptionProvider>();
                    foreach (var apiVersionDescription in provider
                                                          .ApiVersionDescriptions
                                                          .OrderByDescending(x => x.ApiVersion))
                    {
                        options.SwaggerEndpoint(
                                                $"/swagger/{apiVersionDescription.GroupName}/swagger.json",
                                                $"Version {apiVersionDescription.ApiVersion}");
                    }
                });

            var lifetime = application.ApplicationServices.GetRequiredService<IApplicationLifetime>();
            var imitationScheduler = application.ApplicationServices.GetRequiredService<IImitationScheduler>();
            imitationScheduler.RunAsync(lifetime.ApplicationStopping);
        }
    }
}