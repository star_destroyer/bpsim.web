﻿using System.Reflection;
using Boilerplate.AspNetCore.Swagger;
using Boilerplate.AspNetCore.Swagger.SchemaFilters;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace BPsim.Web.Imitation.Service
{
    public static class SwaggerExtensions
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            return services.AddSwaggerGen(options =>
            {
                var assembly = typeof(Startup).GetTypeInfo().Assembly;
                var assemblyProduct = assembly.GetCustomAttribute<AssemblyProductAttribute>().Product;
                var assemblyDescription = assembly.GetCustomAttribute<AssemblyDescriptionAttribute>().Description;

                options.DescribeAllEnumsAsStrings();
                options.DescribeAllParametersInCamelCase();
                options.DescribeStringEnumsInCamelCase();

                options.IncludeXmlCommentsIfExists(assembly);

                options.SchemaFilter<ModelStateDictionarySchemaFilter>();

                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                foreach (var apiVersionDescription in provider.ApiVersionDescriptions)
                {
                    var info = new Info
                    {
                        Title = $"{assemblyProduct} (Version {apiVersionDescription.ApiVersion})",
                        Description = apiVersionDescription.IsDeprecated ? $"{assemblyDescription} This API version has been deprecated." : assemblyDescription,
                        Version = apiVersionDescription.ApiVersion.ToString()
                    };
                    options.SwaggerDoc(apiVersionDescription.GroupName, info);
                }
            });
        }
    }
}