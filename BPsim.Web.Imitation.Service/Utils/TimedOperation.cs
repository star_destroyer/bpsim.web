﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Serilog;
using Serilog.Events;

namespace BPsim.Web.Imitation.Service.Utils
{
    public sealed class TimedOperation : IDisposable
    {
        private readonly ILogger logger;
        private readonly string messageTemplate;
        private readonly object[] args;
        private readonly Stopwatch stopwatch;
        private readonly List<KeyValuePair<string, object>> properties;

        private LogEventLevel defaultLevel = LogEventLevel.Information;
        private TimeSpan? exceedThreshold;
        private LogEventLevel exceedLevel = LogEventLevel.Warning;

        public TimedOperation(ILogger logger, string messageTemplate, object[] args)
        {
            this.logger = logger;
            this.messageTemplate = messageTemplate;
            this.args = args;

            properties = new List<KeyValuePair<string, object>>();
            stopwatch = Stopwatch.StartNew();
        }

        public TimedOperation NotifyIfExceed(TimeSpan threshold, LogEventLevel level = LogEventLevel.Warning)
        {
            exceedThreshold = threshold;
            exceedLevel = level;
            return this;
        }

        public TimedOperation Attach(string name, object value)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            properties.Add(new KeyValuePair<string, object>(name, value));
            return this;
        }

        public TimedOperation WithLevel(LogEventLevel level)
        {
            defaultLevel = level;
            return this;
        }

        public void Dispose()
        {
            stopwatch.Stop();

            var logLevel = exceedThreshold.HasValue && stopwatch.Elapsed >= exceedThreshold.Value ? exceedLevel : defaultLevel;
            var propertiesTemplate = properties.Count == 0 ? string.Empty : $"({string.Join(", ", properties.Select(x => $"{{{x.Key}}}"))})";
            var propertiesArgs = properties.Select(x => x.Value);
            logger.Write(logLevel, $"{messageTemplate}{propertiesTemplate} completed in {{ElapsedMs}} ms", args.Concat(propertiesArgs).Concat(new object[] {stopwatch.ElapsedMilliseconds}).ToArray());
        }
    }
}