﻿using BPsim.Web.Imitation.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Builders
{
    [TestFixture]
    public class ExpressionFactoryTests
    {
        private static readonly IExpressionFactory factory = new ExpressionFactory(new ExpressionParser());
        
        [Test]
        public void Create_ExpressionWithotResourcesAndOrders_ReturnExpressionInfo()
        {
            var expressionInfo = factory.Create("1");
            
            Assert.That(expressionInfo.ResourceValueProviders, Is.Empty);
            Assert.That(expressionInfo.OrderValueProviders, Is.Empty);
            Assert.That(expressionInfo.Script, Is.EqualTo("1"));
        }
        
        [Test]
        public void Create_ExpressionWithOneResource_ReturnExpressionInfo()
        {
            var expressionInfo = factory.Create("@\"x\"");
            
            Assert.That(expressionInfo.Script, Is.EqualTo("r_x"));
            Assert.That(expressionInfo.ResourceValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.ResourceValueProviders.ContainsKey("x"));
        }
        
        [Test]
        public void Create_ExpressionWithTwoResource_ReturnExpressionInfo()
        {
            var expressionInfo = factory.Create("@\"x\" + @\"y\" * @\"x\"");
            
            Assert.That(expressionInfo.Script, Is.EqualTo("r_x + r_y * r_x"));
            Assert.That(expressionInfo.ResourceValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.ResourceValueProviders.Count, Is.EqualTo(2));
            Assert.That(expressionInfo.ResourceValueProviders.ContainsKey("x"));
            Assert.That(expressionInfo.ResourceValueProviders.ContainsKey("y"));
        }
        
        [Test]
        public void Create_ExpressionWithOneOrder_ReturnExpressionInfo()
        {
            var expressionInfo = factory.Create("#\"x\".y");
            
            Assert.That(expressionInfo.Script, Is.EqualTo("o_x_y"));
            Assert.That(expressionInfo.OrderValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders.ContainsKey("x"));
            Assert.That(expressionInfo.OrderValueProviders["x"], Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders["x"].ContainsKey("y"));
        }
        
        [Test]
        public void Create_ExpressionWithOneOrderAndTwoProps_ReturnExpressionInfo()
        {
            var expressionInfo = factory.Create("#\"x\".y + #\"x\".\"y\" * #\"x\".z");
            
            Assert.That(expressionInfo.Script, Is.EqualTo("o_x_y + o_x_y * o_x_z"));
            Assert.That(expressionInfo.OrderValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders.Count, Is.EqualTo(1));
            Assert.That(expressionInfo.OrderValueProviders.ContainsKey("x"));
            Assert.That(expressionInfo.OrderValueProviders["x"], Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders["x"].Count, Is.EqualTo(2));
            Assert.That(expressionInfo.OrderValueProviders["x"].ContainsKey("y"));
            Assert.That(expressionInfo.OrderValueProviders["x"].ContainsKey("z"));
        }
        
        [Test]
        public void Create_ExpressionWithOneOrderAndOneResource_ReturnExpressionInfo()
        {
            var expressionInfo = factory.Create("#\"x\".y + @\"x\"");
            
            Assert.That(expressionInfo.Script, Is.EqualTo("o_x_y + r_x"));
            Assert.That(expressionInfo.ResourceValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.ResourceValueProviders.Count, Is.EqualTo(1));
            Assert.That(expressionInfo.ResourceValueProviders.ContainsKey("x"));
            Assert.That(expressionInfo.OrderValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders.Count, Is.EqualTo(1));
            Assert.That(expressionInfo.OrderValueProviders.ContainsKey("x"));
            Assert.That(expressionInfo.OrderValueProviders["x"], Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders["x"].Count, Is.EqualTo(1));
            Assert.That(expressionInfo.OrderValueProviders["x"].ContainsKey("y"));
        }
        
        [Test]
        public void Create_ReplaceSpaces_ReturnExpressionInfo()
        {
            var expressionInfo = factory.Create("#\"x 1\".\"y 4\" + @\"x x\"");
            
            Assert.That(expressionInfo.Script, Is.EqualTo("o_x_1_y_4 + r_x_x"));
            Assert.That(expressionInfo.ResourceValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.ResourceValueProviders.Count, Is.EqualTo(1));
            Assert.That(expressionInfo.ResourceValueProviders.ContainsKey("x x"));
            Assert.That(expressionInfo.OrderValueProviders, Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders.Count, Is.EqualTo(1));
            Assert.That(expressionInfo.OrderValueProviders.ContainsKey("x 1"));
            Assert.That(expressionInfo.OrderValueProviders["x 1"], Is.Not.Empty);
            Assert.That(expressionInfo.OrderValueProviders["x 1"].Count, Is.EqualTo(1));
            Assert.That(expressionInfo.OrderValueProviders["x 1"].ContainsKey("y 4"));
        }
    }
}