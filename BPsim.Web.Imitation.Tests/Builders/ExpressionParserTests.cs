﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Builders
{
    [TestFixture]
    public class ExpressionParserTests
    {
        private static readonly IExpressionParser parser = new ExpressionParser();

        [TestCase("1", 1, TestName = "Build_1_Return1")]
        [TestCase("1+1", 2, TestName = "Build_1And1_Return2")]
        [TestCase("1-1", 0, TestName = "Build_1Substract1_Return0")]
        [TestCase("2+2*2", 6, TestName = "Build_OperationOrder_Return6")]
        [TestCase("(2+2)*2", 8, TestName = "Build_ParenthesesReorderPriority_Return8")]
        [TestCase("10/((12 - (5 + 5))*1)", 5, TestName = "Build_ParenthesesReorderPriority_Return5")]
        [TestCase("1.0", 1, TestName = "Build_1.0_Return1")]
        [TestCase("1.0+1.5", 2.5, TestName = "Build_1.0And1.5_Return3.5")]
        [TestCase("(1.0+1.5)", 2.5, TestName = "Build_1.0And1.5_Return3.5")]
        public void ParseDecimal(string line, object expected)
        {
            var expression = parser.Parse(line);

            Assert.That(expression.Invoke(new Dictionary<string, decimal>()), Is.EqualTo(expected));
        }

        [TestCaseSource(nameof(ParseDecimalWithParameters_Cases))]
        public void ParseDecimalWithParameters(string line, IDictionary<string, decimal> parameters, object expected)
        {
            var expression = parser.Parse(line);

            Assert.That(expression.Invoke(parameters), Is.EqualTo(expected));
        }

        private static IEnumerable<TestCaseData> ParseDecimalWithParameters_Cases()
        {
            yield return new TestCaseData("x", new Dictionary<string, decimal> {{"x", 1}}, 1)
                {TestName = "Build_xIs1_Return1"};

            yield return new TestCaseData("y+x", new Dictionary<string, decimal> {{"x", 1}, {"y", 2}}, 3)
                {TestName = "Build_yIs2AndxIs1_Return3"};
        }

        [TestCase("1 == 1", true, TestName = "Parse_1Equal1_ReturnTrue")]
        [TestCase("1 != 1", false, TestName = "Parse_1NotEqual1_ReturnFalse")]
        [TestCase("1 < 2", true, TestName = "Parse_1Less2_ReturnTrue")]
        [TestCase("1 <= 1", true, TestName = "Parse_1LessOrEqual1_ReturnTrue")]
        [TestCase("1 > 2", false, TestName = "Parse_1Greater2_ReturnFasle")]
        [TestCase("1 >= 2", false, TestName = "Parse_1GreaterOrEqual2_ReturnFalse")]
        [TestCase("1 + 1 >= 25/10", false, TestName = "Parse_ComplexExpression_ReturnFalse")]
        public void ParseBoolean(string line, object expected)
        {
            var expression = parser.Parse(line);

            Assert.That(expression.Invoke(new Dictionary<string, decimal>()), Is.EqualTo(expected));
        }

        [TestCase("Random(1, 5)", TestName = "Parse_Random1And5_ReturnRandom")]
        public void Parse_UserRandom(string line)
        {
            var expression = parser.Parse(line);
            
            Console.WriteLine(expression.Invoke(new Dictionary<string, decimal>()));
        }
    }
}