﻿using Autofac;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Factories;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Builders
{
    [TestFixture]
    public class ExpressionRuleFactoryTests
    {
        private static readonly IExpressionRuleFactory expressionRuleFactory = Create.Container.Resolve<IExpressionRuleFactory>();
        
        [Test]
        public void Create_AssignResource_AssignmentRule()
        {
            var rule = expressionRuleFactory.Create("@\"x\" = 100");
            
            Assert.That(rule, Is.TypeOf(typeof(AssignmentRule)));
        }

        [Test]
        public void Create_AssignOrder_AssignmentRule()
        {
            var rule = expressionRuleFactory.Create("#\"x\".a = 100");
            
            Assert.That(rule, Is.TypeOf(typeof(AssignmentRule)));   
        }
        
        [Test]
        public void Create_BooleanExpression_ExpressionRule()
        {
            var rule = expressionRuleFactory.Create("#\"x\".a == 100");
            
            Assert.That(rule, Is.TypeOf(typeof(ExpressionRule)));   
        }
    }
}