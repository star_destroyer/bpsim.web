﻿using Autofac;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing
{
    [TestFixture]
    public class AgentExecutorTests
    {
        [Test]
        public void Case()
        {
            var operation = Create.Operation
                                  .AddInRule(Create.Rule.CreateOrder("z"))
                                  .Done("o");

            var agent = Create.Agent
                              .AddKonwledge("lol", x => x.AddRule(Create.Rule.SelecteOrder("z", "o")).AddRule(Create.Rule.RemoveOrder("z")))
                              .Done("a");

            var context = Create.Context
                                .AddNode(operation)
                                .AddNode(agent)
                                .AddOrderTemplate("z")
                                .Done();

            var operationExecutor = Create.Container.Resolve<IOperationExecutor>();
            var agentExecutor = Create.Container.Resolve<IAgentExecutor>();

            operationExecutor.ExecuteAsFree(context, operation);
            agentExecutor.Execute(context, agent);
            operationExecutor.ExecuteAsBusy(context, operation);
            
            Assert.That(context.OrderOwningDispatcher.GetLocked("o"), Is.Null);
        }

        [Test]
        public void Execute_CreateOrderInGlobalRule_OneKnowledgeExecuted()
        {
            var agentExecutor = Create.Container.Resolve<IAgentExecutor>();
            var agent = Create.Agent
                              .AddGlobalRule(Create.Rule.CreateOrder("a"))
                              .AddKonwledge("first", knowledge => knowledge.AddRule(Create.Rule.Assignment("a", "t", "1")))
                              .Done("agent");

            var context = Create.Context
                                .AddNode(agent)
                                .AddOrderTemplate("a", Create.Template.WithProp("t", 0).Done())
                                .Done();

            var result = agentExecutor.Execute(context, agent);
            Assert.That(result.IsOk);
            Assert.That(context.OrderDispatcher.Get("a", 0).Props["t"].Value, Is.EqualTo(1d));
        }

        [Test]
        public void Execute_CreateOrderInGlobalRuleTwoKnowledge_OneKnowledgeExecuted()
        {
            var agentExecutor = Create.Container.Resolve<IAgentExecutor>();
            var agent = Create.Agent
                              .AddGlobalRule(Create.Rule.CreateOrder("a"))
                              .AddKonwledge("first", knowledge => knowledge.AddRule(Create.Rule.Assignment("a", "t", "1")))
                              .AddKonwledge("second", knowledge => knowledge.AddRule(Create.Rule.Assignment("a", "t", "2")))
                              .Done("agent");

            var context = Create.Context
                                .AddNode(agent)
                                .AddOrderTemplate("a", Create.Template.WithProp("t", 0).Done())
                                .Done();

            var result = agentExecutor.Execute(context, agent);
            Assert.That(result.IsOk);
            Assert.That(context.OrderDispatcher.Get("a", 0).Props["t"].Value, Is.EqualTo(2d));
        }
    }
}