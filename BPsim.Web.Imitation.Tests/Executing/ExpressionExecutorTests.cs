﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Structure;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing
{
    [TestFixture]
    public class ExpressionExecutorTests
    {
        private static readonly IExpressionExecutor executor = new ExpressionExecutor();

        [Test]
        public void Execute_ExpressionWithoutParameters_ReturnResult()
        {
            var expression = Create.Expression("1");

            var result = executor.Execute(expression, new Dictionary<string, Resource>(), new Dictionary<string, Order>());

            Assert.That(result.IsOk);
            Assert.That(result.Result, Is.EqualTo(1));
        }

        [Test]
        public void Execute_ExpressionWithResource_ReturnResult()
        {
            var expression = Create.Expression("@\"x\"");
            var resources = new Dictionary<string, Resource>
            {
                {"x", Create.Resource(1)}
            };

            var result = executor.Execute(expression, resources, new Dictionary<string, Order>());

            Assert.That(result.IsOk);
            Assert.That(result.Result, Is.EqualTo(1));
        }

        [Test]
        public void Execute_ExpressionWithTwoResources_ReturnResult()
        {
            var expression = Create.Expression("@\"x\" + @\"y\"");
            var resources = new Dictionary<string, Resource>
            {
                {"x", Create.Resource(1)},
                {"y", Create.Resource(3)}
            };

            var result = executor.Execute(expression, resources, new Dictionary<string, Order>());

            Assert.That(result.IsOk);
            Assert.That(result.Result, Is.EqualTo(4));
        }

        [Test]
        public void Execute_ExpressionWithOrderAndOneParameter_ReturnResult()
        {
            var expression = Create.Expression("#\"x\".\"y\"");
            var orders = new Dictionary<string, Order>
            {
                {"x", Create.Order.WithProp("y", 1).Done(0)},
            };

            var result = executor.Execute(expression, new Dictionary<string, Resource>(), orders);

            Assert.That(result.IsOk);
            Assert.That(result.Result, Is.EqualTo(1));
        }

        [Test]
        public void Execute_ExpressionWithOrderAndTwoParameter_ReturnResult()
        {
            var expression = Create.Expression("#\"x 1\".\"y 1\" + #\"x 1\".a");
            var orders = new Dictionary<string, Order>
            {
                {"x 1", Create.Order.WithProp("y 1", 1).WithProp("a", 10).Done(0)},
            };

            var result = executor.Execute(expression, new Dictionary<string, Resource>(), orders);

            Assert.That(result.IsOk);
            Assert.That(result.Result, Is.EqualTo(11));
        }

        [Test]
        public void Case()
        {
            var expression = Create.Expression("@\"2-cage mill\" == 0");
            var resources = new Dictionary<string, Resource>
            {
                {"2-cage mill", Create.Resource(0)}
            };

            var result = executor.Execute(expression, resources, new Dictionary<string, Order>());
            
            Assert.That(result.IsOk);
        }
    }
}