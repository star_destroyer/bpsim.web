using Autofac;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing
{
    [TestFixture]
    public class OperationExecutorTests
    {
        [Test]
        public void ExecuteDurationRuleWithOrder()
        {
            var operation = Create.Operation
                                  .AddInRule(Create.Rule.CreateOrder("z"))
                                  .AddInRule(Create.Rule.Assignment("z", "a", "10"))
                                  .Done("o", Create.Rule.Expression(@"#""z"".""a"""));

            var context = Create.Context
                                .AddOrderTemplate("z", Create.Template.WithProp("a", 10).Done())
                                .AddNode(operation)
                                .Done();

            var executor = Create.Container.Resolve<IOperationExecutor>();

            var result = executor.ExecuteAsFree(context, operation);

            Assert.That(result.IsOk);
        }
    }
}