﻿using System;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing
{
    public class RuleExecutorTests
    {
        private const string NodeName = "n";
        private const string OrderName = "z";

        private static readonly IRuleExecutor executor = new RuleExecutor();

        [Test]
        public void CanExecute_TwoCreateRules_IsNotOk()
        {
            var ruleContext = Create.Context
                                    .AddNode(Create.Operation.Done(NodeName))
                                    .AddOrderTemplate(OrderName)
                                    .ForRule.Done(NodeName);
            var firstCreateOrderRule = Create.Rule.CreateOrder(OrderName);
            var secondCreateOrderRule = Create.Rule.CreateOrder(OrderName);

            var firstResult = firstCreateOrderRule.CanExecute(ruleContext);
            var secondResult = secondCreateOrderRule.CanExecute(ruleContext);

            Assert.That(firstResult.IsOk, Is.True);
            Assert.That(secondResult.IsOk, Is.False);
        }

        [Test]
        public void CanExecute_NoRules_IsOk()
        {
            var ruleContext = Create.Context.ForRule.Done(NodeName);

            var result = executor.CanExecute(ruleContext, Array.Empty<Rule>());

            Assert.That(result.IsOk, Is.True);
        }

        [Test]
        public void CanExecute_OneRuleIsFail_IsNotOk()
        {
            var ruleContext = Create.Context.ForRule.Done(NodeName);
            var fakeRule = Create.Rule.Fake(() => Result.Fail("Fail"), () => null);

            var result = executor.CanExecute(ruleContext, new[] {fakeRule});

            Assert.That(result.IsOk, Is.False);
            Assert.That(result.Error, Is.EqualTo("Правила не могут быть выполнены\r\n -> Fail"));
        }

        [Test]
        public void CanExecute_SeveralRulesIsOk_IsOk()
        {
            var ruleContext = Create.Context.ForRule.Done(NodeName);
            var rules = new[]
            {
                Create.Rule.Fake(Result.Ok, Result.Ok),
                Create.Rule.Fake(Result.Ok, Result.Ok),
                Create.Rule.Fake(Result.Ok, Result.Ok)
            };

            var result = executor.CanExecute(ruleContext, rules);

            Assert.That(result.IsOk, Is.True);
        }

        [Test]
        public void CanExecute_SeveralRulesIsOkButOneFail_IsNotOk()
        {
            var ruleContext = Create.Context.ForRule.Done(NodeName);
            var rules = new[]
            {
                Create.Rule.Fake(Result.Ok, Result.Ok),
                Create.Rule.Fake(() => Result.Fail("Fail"), Result.Ok),
                Create.Rule.Fake(Result.Ok, Result.Ok)
            };

            var result = executor.CanExecute(ruleContext, rules);

            Assert.That(result.IsOk, Is.False);
            Assert.That(result.Error, Is.EqualTo("Правила не могут быть выполнены\r\n -> Fail"));
        }

        [Test]
        public void Execute_SeveralRulesIsOk_IsOk()
        {
            var ruleContext = Create.Context.ForRule.Done(NodeName);
            var rules = new[]
            {
                Create.Rule.Fake(Result.Ok, Result.Ok),
                Create.Rule.Fake(Result.Ok, Result.Ok),
                Create.Rule.Fake(Result.Ok, Result.Ok)
            };

            var result = executor.Execute(ruleContext, rules);

            Assert.That(result.IsOk, Is.True);
        }
    }
}