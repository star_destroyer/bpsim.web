﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing.Rules
{
    [TestFixture]
    public class CreateOrderRuleTests
    {
        private const string OrderName = "z";
        private const string NodeName = "n";

        [TestCaseSource(nameof(CanExecute_Fail_Cases))]
        public void CanExecute_Fail(RuleContext context, string errorMessage)
        {
            var rule = Create.Rule.CreateOrder(OrderName);

            var result = rule.CanExecute(context);

            Assert.That(result.IsOk, Is.EqualTo(false));
            Assert.That(result.Error, Is.EqualTo(errorMessage));
        }

        private static IEnumerable<TestCaseData> CanExecute_Fail_Cases()
        {
            var ruleContext = Create.Context.ForRule.SelectOrder(OrderName, NodeName, o => o.Done(1)).Done(NodeName);

            yield return new TestCaseData(ruleContext, "Заявка z уже захвачена")
                {TestName = "CanExecute_OrderSelected_IsNotOk"};

            ruleContext = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done())
                                .ForRule
                                .Give(NodeName, OrderName, 1)
                                .Done(NodeName);

            yield return new TestCaseData(ruleContext, "Узел не может владеть еще одной заявкой z")
                {TestName = "CanExecute_NodeOwnOrderAndNodeNotManyOwnerAndOrderNotManyOwned_IsNotOk"};
        }

        [TestCaseSource(nameof(CanExecute_Success_Cases))]
        public void CanExecute_Success(RuleContext context)
        {
            var rule = Create.Rule.CreateOrder(OrderName);

            var result = rule.CanExecute(context);

            Assert.That(result.IsOk, Is.EqualTo(true));
            Assert.That(context.SelectedOrders[OrderName].Peek().State, Is.EqualTo(OrderContextState.Available));
        }

        private static IEnumerable<TestCaseData> CanExecute_Success_Cases()
        {
            var ruleContext = Create.Context
                                    .AddNode(Create.Operation.Done(NodeName, isManyOwner: true))
                                    .AddOrderTemplate(OrderName, Create.Template.Done())
                                    .ForRule
                                    .Give(NodeName, OrderName, 0)
                                    .Done(NodeName);

            yield return new TestCaseData(ruleContext)
                {TestName = "CanExecute_NodeOwnOrderButNodeIsManyOwner_IsOk"};

            ruleContext = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done(true))
                                .ForRule
                                .Give(NodeName, OrderName, 1)
                                .Done(NodeName);

            yield return new TestCaseData(ruleContext)
                {TestName = "CanExecute_NodeOwnOrderButOrderIsManyOwned_IsOk"};

            ruleContext = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done())
                                .ForRule
                                .Done(NodeName);

            yield return new TestCaseData(ruleContext)
                {TestName = "CanExecute_NodeNotOwnOrder_IsOk"};

            ruleContext = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done())
                                .ForRule
                                .SelectOrder(OrderName, NodeName, o => o.Done(1), OrderContextState.NotAvailable)
                                .Done(NodeName);

            yield return new TestCaseData(ruleContext)
                {TestName = "CanExecute_SelectedNotAvailableOrder_IsOk"};
        }

        [Test]
        public void Execute_OrderSelected_IsOk()
        {
            var context = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done())
                                .AddOrderInstance(OrderName)
                                .ForRule
                                .SelectOrder(OrderName, NodeName, o => o.Done(1))
                                .Done(NodeName);

            var rule = Create.Rule.CreateOrder(OrderName);

            var result = rule.Execute(context);

            Assert.That(result.IsOk, Is.EqualTo(true));
        }
    }
}