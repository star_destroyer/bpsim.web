﻿using System;
using BPsim.Web.Imitation.Structure;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing.Rules
{
    [TestFixture]
    public class AssignmentRuleTests
    {
        [Test]
        public void CanExecute_ExecutionError_IsNotOk()
        {
            var rule = Create.Rule.Assignment("a", "#\"a\".a");

            var result = rule.CanExecute(Create.Context.ForRule.Done("node"));

            Assert.That(result.IsOk, Is.False);
            Console.WriteLine(result.Error);
        }
        
        [Test]
        public void CanExecute_ExecutionResultIsFalse_IsNotOk()
        {
            var rule = Create.Rule.Assignment("a", "1 != 1");

            var result = rule.CanExecute(Create.Context.ForRule.Done("node"));

            Assert.That(result.IsOk, Is.False);
        }
        
        [Test]
        public void CanExecute_ResourceInNotAssign_IsNotOk()
        {
            var rule = Create.Rule.Assignment("a", "0");

            var result = rule.CanExecute(Create.Context.AddResource("a", new Resource(10, 1, 10)).ForRule.Done("node"));

            Assert.That(result.IsOk, Is.False);
        }
        
        [Test]
        public void CanExecute_OrderDoesNotSelect_IsNotOk()
        {
            var rule = Create.Rule.Assignment("a", "b", "0");

            var result = rule.CanExecute(Create.Context.ForRule.Done("node"));

            Assert.That(result.IsOk, Is.False);
        }
        
        [Test]
        public void CanExecute_ResourceAssign_IsOk()
        {
            var rule = Create.Rule.Assignment("a", "0");

            var result = rule.CanExecute(Create.Context.AddResource("a", new Resource(5)).ForRule.Done("node"));

            Assert.That(result.IsOk);
        }
        
        [Test]
        public void CanExecute_OrderSelected_IsOk()
        {
            var rule = Create.Rule.Assignment("a", "b", "15");

            var result = rule.CanExecute(Create.Context.ForRule.SelectOrder("a", "node", x => x.WithProp("b", 0).Done(0)).Done("node"));

            Assert.That(result.IsOk);
        }
    }
    
    [TestFixture]
    public class ExpressionRuleTests
    {
        [Test]
        public void CanExecute_ExecutionError_IsNotOk()
        {
            var rule = Create.Rule.Expression("#\"a\".a");

            var result = rule.CanExecute(Create.Context.ForRule.Done("node"));

            Assert.That(result.IsOk, Is.False);
            Console.WriteLine(result.Error);
        }

        [Test]
        public void CanExecute_ExecutionResultIsFalse_IsNotOk()
        {
            var rule = Create.Rule.Expression("1 != 1");

            var result = rule.CanExecute(Create.Context.ForRule.Done("node"));

            Assert.That(result.IsOk, Is.False);
        }

        [Test]
        public void CanExecute_ExecutionResultIsNotBoolean_IsOk()
        {
            var rule = Create.Rule.Expression("1");

            var result = rule.CanExecute(Create.Context.ForRule.Done("node"));

            Assert.That(result.IsOk, Is.True);
        }

        [Test]
        public void CanExecute_ExecutionResultIsTrue_IsOk()
        {
            var rule = Create.Rule.Expression("1 == 1");

            var result = rule.CanExecute(Create.Context.ForRule.Done("node"));

            Assert.That(result.IsOk, Is.True);
        }
    }
}