﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing.Rules
{
    [TestFixture]
    public class RemoveOrderRuleTests
    {
        private const string OrderName = "z";
        private const string NodeName = "n";

        [TestCaseSource(nameof(CanExecute_Fail_Cases))]
        public void CanExecute_Fail(RuleContext context, string errorMessage)
        {
            var rule = Create.Rule.RemoveOrder(OrderName);

            var result = rule.CanExecute(context);

            Assert.That(result.IsOk, Is.EqualTo(false));
            Assert.That(result.Error, Is.EqualTo(errorMessage));
        }

        private static IEnumerable<TestCaseData> CanExecute_Fail_Cases()
        {
            var ruleContext = Create.Context.ForRule.Done(NodeName);

            yield return new TestCaseData(ruleContext, "Заявка z не захвачена")
                {TestName = "CanExecute_OrderNotSelected_IsNotOk"};
        }

        [TestCaseSource(nameof(CanExecute_Success_Cases))]
        public void CanExecute_Success(RuleContext context)
        {
            var rule = Create.Rule.RemoveOrder(OrderName);

            var result = rule.CanExecute(context);

            Assert.That(result.IsOk, Is.EqualTo(true));
            Assert.That(context.SelectedOrders[OrderName].Peek().State, Is.EqualTo(OrderContextState.NotAvailable));
        }

        private static IEnumerable<TestCaseData> CanExecute_Success_Cases()
        {
            var ruleContext = Create.Context.ForRule.SelectOrder(OrderName, NodeName, o => o.Done(1)).Done(NodeName);

            yield return new TestCaseData(ruleContext)
                {TestName = "CanExecute_OrderSelected_IsOk"};
        }

        [Test]
        public void Execute_OrderSelected_IsOk()
        {
            var context = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done())
                                .AddOrderInstance(OrderName)
                                .ForRule
                                .SelectOrder(OrderName, NodeName, o => o.Done(0))
                                .Done(NodeName);

            var rule = Create.Rule.RemoveOrder(OrderName);

            var result = rule.Execute(context);

            Assert.That(result.IsOk, Is.EqualTo(true));
        }
    }
}