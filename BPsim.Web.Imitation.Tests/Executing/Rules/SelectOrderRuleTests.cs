﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Executing.Rules
{
    [TestFixture]
    public class SelectOrderRuleTests
    {
        private const string OrderName = "z";
        private const string NodeName = "n";

        [Test]
        public void CanExecute_AllRight_IsOk()
        {
            var context = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done())
                                .AddOrderInstance(OrderName)
                                .ForRule
                                .Give(NodeName, OrderName, 0)
                                .Done(NodeName);

            var rule = Create.Rule.SelecteOrder(OrderName, NodeName);

            var result = rule.CanExecute(context);

            Assert.That(result.IsOk);
            Assert.That(context.SelectedOrders[OrderName].Peek().State, Is.EqualTo(OrderContextState.Available));
        }

        [TestCaseSource(nameof(CanExecute_Fail_Cases))]
        public void CanExecute_Fail(RuleContext context, string errorMessage)
        {
            var rule = Create.Rule.SelecteOrder(OrderName, NodeName);

            var result = rule.CanExecute(context);

            Assert.That(result.IsOk, Is.EqualTo(false));
            Assert.That(result.Error, Is.EqualTo(errorMessage));
        }

        private static IEnumerable<TestCaseData> CanExecute_Fail_Cases()
        {
            var ruleContext = Create.Context.ForRule.SelectOrder(OrderName, NodeName, o => o.Done(1)).Done(NodeName);

            yield return new TestCaseData(ruleContext, "Заявка z уже захвачена")
                {TestName = "CanExecute_OrderSelected_IsNotOk"};

            ruleContext = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .ForRule
                                .Done(NodeName);

            yield return new TestCaseData(ruleContext, "Узел n не владеет свободными заявками z")
                {TestName = "CanExecute_TargetNodeDoesNotHaveFreeOrder_IsNotOk"};
        }

        [Test]
        public void CanExecute_SelectedNotAvailableOrder_IsOk()
        {
            var context = Create.Context
                                .AddNode(Create.Operation.Done(NodeName))
                                .AddOrderTemplate(OrderName, Create.Template.Done())
                                .AddOrderInstance(OrderName)
                                .ForRule
                                .Give(NodeName, OrderName, 0)
                                .SelectOrder(OrderName, NodeName, o => o.Done(1), OrderContextState.NotAvailable)
                                .Done(NodeName);
            
            var rule = Create.Rule.SelecteOrder(OrderName, NodeName);

            var result = rule.CanExecute(context);

            Assert.That(result.IsOk);
            Assert.That(context.SelectedOrders[OrderName].Peek().State, Is.EqualTo(OrderContextState.Available));
        }

        [Test]
        public void Execute_OrderSelected_IsOk()
        {
            var context = Create.Context.ForRule.SelectOrder(OrderName, NodeName, o => o.Done(1)).Done(NodeName);
            var rule = Create.Rule.SelecteOrder(OrderName, NodeName);

            var result = rule.Execute(context);

            Assert.That(result.IsOk, Is.EqualTo(true));
        }
    }
}