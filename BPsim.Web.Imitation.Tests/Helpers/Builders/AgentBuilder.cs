﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class AgentBuilder
    {
        private readonly List<IRule> globalRules = new List<IRule>();
        private readonly IDictionary<string, Knowledge> knowledges = new Dictionary<string, Knowledge>();

        public AgentBuilder AddGlobalRule(IRule rule)
        {
            globalRules.Add(rule);
            return this;
        }

        public AgentBuilder AddKonwledge(string name, Action<KnowledgeBuilder> builder)
        {
            var knowledgeBuilder = new KnowledgeBuilder();
            builder(knowledgeBuilder);
            knowledges[name] = knowledgeBuilder.Done();
            return this;
        }
        
        public Agent Done(string name, bool isManyOwner = false)
        {
            return new Agent(name)
            {
                GlobalRules = globalRules.ToArray(),
                Knowledges = knowledges,
                IsManyOwner = isManyOwner
            };
        }
    }
}