﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Dispatchers;
using BPsim.Web.Imitation.Dispatchers.Nodes;
using BPsim.Web.Imitation.Dispatchers.Relationships;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Reporting;
using BPsim.Web.Imitation.Structure;
using FakeItEasy;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class ContextBuilder
    {
        private readonly Dictionary<string, Resource> resources = new Dictionary<string, Resource>();
        private readonly Dictionary<string, OrderTemplate> templates = new Dictionary<string, OrderTemplate>();
        private readonly List<string> savingOrders = new List<string>();
        private readonly List<Node> nodes = new List<Node>();

        public RuleContextBuilder ForRule => new RuleContextBuilder(Done());

        public ContextBuilder AddResource(string name, Resource resource)
        {
            resources[name] = resource;
            return this;
        }

        public ContextBuilder AddOrderTemplate(string name, OrderTemplate template = null)
        {
            templates[name] = template ?? OrderTemplate.Default;
            return this;
        }

        public ContextBuilder AddOrderInstance(string name)
        {
            savingOrders.Add(name);
            return this;
        }

        public ContextBuilder AddNode(Node node)
        {
            nodes.Add(node);
            return this;
        }

        public IContext Done(int tick = 0)
        {
            var orderDispatcher = new OrderDispatcher(templates.Keys);
            foreach (var order in savingOrders)
            {
                orderDispatcher.Save(order, orderDispatcher.Create(templates[order]));
            }

            return new Context
            {
                Reporter = A.Fake<IReporter>(),
                ResourceDispatcher = new ResourceDispatcher(resources),
                NodeDispatcher = new NodeDispatcher(nodes.ToArray()),
                OrderTemplateDispatcher = new OrderTemplateDispatcher(templates),
                OrderOwningDispatcher = new OrderOwningDispatcher(nodes.Select(x => x.Name).ToArray()),
                OrderDispatcher = orderDispatcher,
                Tick = tick
            };
        }
    }
}