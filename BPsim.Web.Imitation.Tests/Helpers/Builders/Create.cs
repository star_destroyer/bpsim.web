﻿using Autofac;
using BPsim.Web.Imitation.Autofac;
using BPsim.Web.Imitation.Builders;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public static class Create
    {
        private static IContainer container;

        public static Resource Resource(decimal value)
        {
            return new Resource(value);
        }

        public static ExpressionInfo Expression(string line)
        {
            return Container.Resolve<IExpressionFactory>().Create(line);
        }

        public static OrderBuilder Order => new OrderBuilder();

        public static OrderTemplateBuilder Template => new OrderTemplateBuilder();

        public static ContextBuilder Context => new ContextBuilder();

        public static RuleBuilder Rule => new RuleBuilder();

        public static OperationBuilder Operation => new OperationBuilder();
        
        public static AgentBuilder Agent => new AgentBuilder();

        public static IContainer Container => container ?? (container = InitContainer());

        private static IContainer InitContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<BPsimWebImitationModule>();
            return builder.Build();
        }
    }
}