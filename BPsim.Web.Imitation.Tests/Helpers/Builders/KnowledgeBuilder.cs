﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class KnowledgeBuilder
    {
        private readonly List<IRule> rules = new List<IRule>();

        public KnowledgeBuilder AddRule(IRule rule)
        {
            rules.Add(rule);
            return this;
        }

        public Knowledge Done()
        {
            return new Knowledge
            {
                Rules = rules.ToArray()
            };
        }
    }
}