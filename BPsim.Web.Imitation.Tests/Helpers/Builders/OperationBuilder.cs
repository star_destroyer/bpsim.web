﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class OperationBuilder
    {
        private List<IRule> inRule = new List<IRule>();
        private List<IRule> outRule = new List<IRule>();

        public OperationBuilder AddInRule(IRule rule)
        {
            inRule.Add(rule);
            return this;
        }

        public OperationBuilder AddOutRule(IRule rule)
        {
            outRule.Add(rule);
            return this;
        }
        
        public Operation Done(string name, ExpressionRule duration = null, bool isManyOwner = false)
        {
            return new Operation(name)
            {
                InRules = inRule.ToArray(),
                OutRules = outRule.ToArray(),
                Duration =  duration ?? Create.Rule.Expression("0"),
                IsManyOwner = isManyOwner
            };
        }
    }
}