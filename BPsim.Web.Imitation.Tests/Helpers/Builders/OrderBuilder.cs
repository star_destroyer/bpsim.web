﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class OrderBuilder
    {
        private readonly IDictionary<string, decimal> props = new Dictionary<string, decimal>();

        public OrderBuilder WithProp(string name, decimal value)
        {
            props[name] = value;
            return this;
        }

        public Order Done(int number)
        {
            return new Order(number, props);
        }
    }
}