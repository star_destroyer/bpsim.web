﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class OrderTemplateBuilder
    {
        private readonly IDictionary<string, decimal> props = new Dictionary<string, decimal>();

        public OrderTemplateBuilder WithProp(string name, dynamic value)
        {
            props[name] = value;
            return this;
        }

        public OrderTemplate Done(bool isManyOwnered = false)
        {
            return new OrderTemplate {DefaultProps = props, IsManyOwnered = isManyOwnered};
        }
    }
}