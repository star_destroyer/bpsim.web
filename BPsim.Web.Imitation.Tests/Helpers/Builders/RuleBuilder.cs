﻿using System;
using Autofac;
using BPsim.Web.Imitation.Builders;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Executing.Rules;
using FakeItEasy;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class RuleBuilder
    {
        public IRule Fake(Func<Result> canExecute, Func<Result> execute)
        {
            return A.Fake<IRule>(o => o.ConfigureFake(rule =>
            {
                A.CallTo(() => rule.CanExecute(A<RuleContext>._)).Returns(canExecute.Invoke());
                A.CallTo(() => rule.Execute(A<RuleContext>._)).Returns(execute.Invoke());
            }));
        }

        public CreateOrderRule CreateOrder(string orderName)
        {
            return new CreateOrderRule(orderName);
        }

        public SelectOrderRule SelecteOrder(string orderName, string nodeName)
        {
            return new SelectOrderRule(orderName, nodeName);
        }

        public SendOrderRule SendOrder(string orderName, string targetName)
        {
            return new SendOrderRule(orderName, targetName);
        }

        public RemoveOrderRule RemoveOrder(string orderName)
        {
            return new RemoveOrderRule(orderName);
        }
        
        public ExpressionRule Expression(string expression)
        {            
            return new ExpressionRule(Create.Container.Resolve<IExpressionFactory>().Create(expression), Create.Container.Resolve<IExpressionExecutor>());
        }

        public AssignmentRule Assignment(string name, string expression)
        {
            return new AssignmentRule(name, Create.Container.Resolve<IExpressionFactory>().Create(expression), Create.Container.Resolve<IExpressionExecutor>());
        }
        
        public AssignmentRule Assignment(string name, string property, string expression)
        {
            return new AssignmentRule(name, property, Create.Container.Resolve<IExpressionFactory>().Create(expression), Create.Container.Resolve<IExpressionExecutor>());
        }
    }
}