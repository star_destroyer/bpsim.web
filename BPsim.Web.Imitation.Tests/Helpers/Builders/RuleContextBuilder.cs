﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Tests.Helpers.Builders
{
    public class RuleContextBuilder
    {
        private readonly IContext context;
        private readonly IDictionary<string, Stack<SelectingOrderInfo>> orders = new Dictionary<string, Stack<SelectingOrderInfo>>();

        public RuleContextBuilder(IContext context)
        {
            this.context = context;
        }

        public RuleContextBuilder SelectOrder(string name, string owner, Order order, OrderContextState state = OrderContextState.Available)
        {
            var selectedOrders = orders.TryGetValue(name, out var stack) ? stack : new Stack<SelectingOrderInfo>();
            selectedOrders.Push(new SelectingOrderInfo(order, state, owner));
            orders[name] = selectedOrders;
            return this;
        }

        public RuleContextBuilder SelectOrder(string name, string owner, Func<OrderBuilder, Order> builder, OrderContextState state = OrderContextState.Available)
        {
            var order = builder.Invoke(new OrderBuilder());
            return SelectOrder(name, owner, order, state);
        }

        public RuleContextBuilder Give(string nodeName, string orderName, int number)
        {
            context.OrderOwningDispatcher.Give(nodeName, orderName, number);
            return this;
        }

        public RuleContext Done(string nodeName)
        {
            return new RuleContext
            {
                CancellationToken = context.CancellationToken,
                NodeDispatcher = context.NodeDispatcher,
                NodeName = nodeName,
                OrderDispatcher = context.OrderDispatcher,
                OrderOwningDispatcher = context.OrderOwningDispatcher,
                OrderTemplateDispatcher = context.OrderTemplateDispatcher,
                ResourceDispatcher = context.ResourceDispatcher,
                Reporter = context.Reporter,
                SelectedOrders = orders
            };
        }
    }
}