﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Structure;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Integration
{
    public class HatManufacture : ScenarioTestsBase
    {
        [Test]
        public async Task SimpleModel()
        {
            var fullTimer = Stopwatch.StartNew();
            var context = ContextFactory.Create(new Dictionary<string, Resource>
                                                {
                                                    {"Количество сшитых шляп", new Resource(0, 0, 1000)},
                                                    {"Количество заказов", new Resource(0, 0, 1000)},
                                                    {"Очередь на раскройку", new Resource(0, 0, 1000)},
                                                    {"Очередь на пошив", new Resource(0, 0, 1000)},
                                                    {"Степень удовлетворенности клиента", new Resource(0, 0, 1)},
                                                    {"Количество мастеров", new Resource(4, 1, 6)},
                                                    {"Прибыль", new Resource(1000, -10000, 10000)},
                                                    {"Интервал прихода заказов", new Resource(1, 1, 5)}
                                                },
                                                new Dictionary<string, OrderTemplate>(),
                                                new Node[]
                                                {
                                                    new Operation("Поступление заказа на пошив")
                                                    {
                                                        Duration = CreateExpressionRule("1 + @\"Интервал прихода заказов\""),
                                                        OutRules = new IRule[]
                                                        {
                                                            CreateAssignmentRule("Количество заказов", "@\"Количество заказов\" + 1"), 
                                                            CreateAssignmentRule("Очередь на раскройку", "@\"Очередь на раскройку\" + 1"),
                                                            CreateAssignmentRule("Прибыль", "@\"Прибыль\" + 250")
                                                        }
                                                    },
                                                    new Operation("Раскройка")
                                                    {
                                                        Duration = CreateExpressionRule("2"),
                                                        InRules = new IRule[]
                                                        {
                                                            CreateAssignmentRule("Очередь на раскройку", "@\"Очередь на раскройку\" - 1")
                                                        },
                                                        OutRules = new IRule[]
                                                        {
                                                            CreateAssignmentRule("Очередь на пошив", "@\"Очередь на пошив\" + 1")
                                                        }
                                                    },
                                                    new Operation("Пошив")
                                                    {
                                                        Duration = CreateExpressionRule("5"),
                                                        InRules = new IRule[]
                                                        {
                                                            CreateAssignmentRule("Очередь на пошив", "@\"Очередь на пошив\" - 1")
                                                        },
                                                        OutRules = new IRule[]
                                                        {
                                                            CreateAssignmentRule("Количество сшитых шляп", "@\"Количество сшитых шляп\" + 1"),
                                                            CreateAssignmentRule("Степень удовлетворенности клиента", "@\"Количество сшитых шляп\"/@\"Количество заказов\""),
                                                            CreateAssignmentRule("Прибыль", "@\"Прибыль\" + 250")
                                                        }
                                                    },
                                                    new Operation("Выплата з/п")
                                                    {
                                                        Duration = CreateExpressionRule("8"),
                                                        OutRules = new IRule[]
                                                        {
                                                            CreateAssignmentRule("Прибыль", "@\"Прибыль\" - (@\"Количество мастеров\"*200 + 150 + 150)")
                                                        }
                                                    }
                                                });

            var imitationTimer = Stopwatch.StartNew();
            await ImitationEngine.Run(new ImitationTask
                                      {
                                          Id = Guid.NewGuid(),
                                          ModelId = Guid.NewGuid(),
                                          Context = context,
                                          Configuration = ConfigurationBuilder
                                                          .WithPipelineStep(ImitationPipelineStep.ExecuteFreeNode)
                                                          .WithPipelineStep(ImitationPipelineStep.ExecuteBusyNode)
                                                          .EndWhenExpression("Tick > 100")
                                      },
                                      null,
                                      CancellationToken.None);

            Console.WriteLine($"Общее время: {fullTimer.Elapsed}");
            Console.WriteLine($"Время имитации: {imitationTimer.Elapsed}");
        }
    }
}