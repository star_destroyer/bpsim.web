﻿using Autofac;
using BPsim.Web.Imitation.Builders;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Executing.Rules;
using BPsim.Web.Imitation.Tests.Helpers.Builders;
using NUnit.Framework;

namespace BPsim.Web.Imitation.Tests.Integration
{
    [TestFixture]
    public abstract class ScenarioTestsBase
    {
        protected IContextFactory ContextFactory { get; private set; }
        protected IExpressionFactory ExpressionFactory { get; private set; }
        protected IImitator Imitator { get; private set; }
        protected IConfigurationBuilder ConfigurationBuilder { get; private set; }
        protected IImitationEngine ImitationEngine { get; private set; }

        [OneTimeSetUp]
        public void Initialize()
        {
            ContextFactory = Create.Container.Resolve<IContextFactory>();
            ExpressionFactory = Create.Container.Resolve<IExpressionFactory>();
            Imitator = Create.Container.Resolve<IImitator>();
            ConfigurationBuilder = Create.Container.Resolve<IConfigurationBuilder>();
            ImitationEngine = Create.Container.Resolve<IImitationEngine>();
        }

        protected ExpressionRule CreateExpressionRule(string expression)
        {
            return new ExpressionRule(ExpressionFactory.Create(expression), Create.Container.Resolve<IExpressionExecutor>());
        }

        protected AssignmentRule CreateAssignmentRule(string name, string expression)
        {
            return new AssignmentRule(name, ExpressionFactory.Create(expression), Create.Container.Resolve<IExpressionExecutor>());
        }
        
        protected AssignmentRule CreateAssignmentRule(string name, string propertyName, string expression)
        {
            return new AssignmentRule(name, propertyName, ExpressionFactory.Create(expression), Create.Container.Resolve<IExpressionExecutor>());
        }
    }
}