﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public class ConfigurationBuilder : IConfigurationBuilder
    {
        private readonly IExpressionFactory expressionFactory;
        private readonly List<ImitationPipelineStep> steps = new List<ImitationPipelineStep>();

        public ConfigurationBuilder(IExpressionFactory expressionFactory)
        {
            this.expressionFactory = expressionFactory;
        }

        public IConfigurationBuilder WithPipelineStep(ImitationPipelineStep step)
        {
            steps.Add(step);
            return this;
        }

        public IImitationConfiguration Build(ExpressionInfo endingRule)
        {
            return new ImitationConfiguration
            {
                EndExpression = endingRule,
                PipelineSteps = steps.ToArray()
            };
        }

        public IImitationConfiguration EndWhenTicks(int ticks)
        {
            throw new NotImplementedException();
        }

        public IImitationConfiguration EndWhenExpression(string expression)
        {
            return Build(expressionFactory.Create(expression));
        }
    }
}