﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using BPsim.Web.Imitation.Dispatchers;
using BPsim.Web.Imitation.Dispatchers.Nodes;
using BPsim.Web.Imitation.Dispatchers.Relationships;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Reporting;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public class ContextFactory : IContextFactory
    {
        public Context Create(IDictionary<string, Resource> resources,
                              IDictionary<string, OrderTemplate> orderTemplates,
                              Node[] nodes,
                              CancellationToken cancellation = default(CancellationToken))
        {
            return new Context
            {
                ResourceDispatcher = new ResourceDispatcher(resources),
                OrderTemplateDispatcher = new OrderTemplateDispatcher(orderTemplates),
                OrderOwningDispatcher = new OrderOwningDispatcher(nodes.Select(x => x.Name).ToArray()),
                OrderDispatcher = new OrderDispatcher(orderTemplates.Keys),
                NodeDispatcher = new NodeDispatcher(nodes),
                Reporter = new Reporter(),
                CancellationToken = cancellation
            };
        }
    }
}