﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public class ExpressionFactory : IExpressionFactory
    {
        private readonly IExpressionParser parser;

        public ExpressionFactory(IExpressionParser parser)
        {
            this.parser = parser;
        }

        public ExpressionInfo Create(string line)
        {
            var resourceValueProviders = new Dictionary<string, ResourceParameter>();
            var orderValueProviders = new Dictionary<string, IDictionary<string, OrderProperyParameter>>();

            var resourceReplaced = Patterns.ResourceRegex.Replace(line,
                                                                  match =>
                                                                  {
                                                                      var name = match.Groups["name"].Value;
                                                                      var parameterName = $"r_{Patterns.DangerSymbols.Replace(name, "_")}";
                                                                      resourceValueProviders[name] = new ResourceParameter(parameterName, r => r.Value);
                                                                      return parameterName;
                                                                  });

            var orderReplaced = Patterns.OrderRegex.Replace(resourceReplaced,
                                                            match =>
                                                            {
                                                                var name = match.Groups["name"].Value;
                                                                var value = match.Groups["value"].Value;
                                                                var parameterName = $"o_{Patterns.DangerSymbols.Replace(name, "_")}_{Patterns.DangerSymbols.Replace(value, "_")}";
                                                                var propertyValueProviders = orderValueProviders.TryGetValue(name, out var order) ? order : new Dictionary<string, OrderProperyParameter>();
                                                                propertyValueProviders[value] = new OrderProperyParameter(parameterName, o => o.Props[value].Value);
                                                                orderValueProviders[name] = propertyValueProviders;
                                                                return parameterName;
                                                            });
            try
            {
                var expression = parser.Parse(orderReplaced);

                return new ExpressionInfo(orderReplaced, expression, resourceValueProviders, orderValueProviders);
            } catch (Exception e)
            {
                throw new Exception(orderReplaced, e);
            }
        }
    }
}