﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Text;
using BPsim.Web.Imitation.Executing;

namespace BPsim.Web.Imitation.Builders
{
    public class ExpressionParser : IExpressionParser
    {
        private static readonly CultureInfo culture = CultureInfo.InvariantCulture;

        public Func<IDictionary<string, decimal>, object> Parse(string line)
        {
            if (string.IsNullOrWhiteSpace(line))
            {
                return p => 0;
            }
            
            var parameter = Expression.Parameter(typeof(IDictionary<string, decimal>), "p");
            var expression = Parse(line, 0, line.Length - 1, parameter);

            return Expression.Lambda<Func<IDictionary<string, decimal>, object>>(Expression.Convert(expression, typeof(object)), parameter).Compile();
        }

        private static Expression Parse(string line, int start, int end, ParameterExpression parameter)
        {
            var expressionStack = new Stack<Expression>();
            var operatorStack = new Stack<Operation>();
            var parameters = new List<string>();
            

            var index = start;

            while(index <= end)
            {
                var next = line[index];

                if (char.IsDigit(next))
                {
                    expressionStack.Push(ReadOperand(line, ref index));
                    continue;
                }

                if (char.IsLetter(next))
                {
                    var name = ReadWord(line, ref index);

                    if (Function.Functions.TryGetValue(name, out var function))
                    {
                        expressionStack.Push(function.Transform(line, ref index, parameter));
                        continue;
                    }

                    if (!parameters.Contains(name))
                    {
                        parameters.Add(name);
                    }

                    expressionStack.Push(Expression.Property(parameter, "Item", Expression.Constant(name)));
                    continue;
                }

                if (Operation.IsOperationSign(next))
                {
                    var currentOperation = ReadOperation(line, ref index);
                    ApplyOperationToParentheses(operatorStack, expressionStack, o => currentOperation.Precedence <= o.Precedence);
                    operatorStack.Push(currentOperation);
                    continue;
                }

                switch (next)
                {
                    case '(':
                        operatorStack.Push(Operation.From("("));
                        index++;
                        continue;
                    case ')':
                        ApplyOperationToParentheses(operatorStack, expressionStack);
                        operatorStack.Pop();
                        index++;
                        continue;
                    case ' ':
                        index++;
                        break;
                    default:
                        throw new ArgumentException($"Поулчили невалидный символ {next}", "expression");
                }
            }

            ApplyOperationToParentheses(operatorStack, expressionStack);

            return expressionStack.Pop();
        }

        private static void ApplyOperationToParentheses(Stack<Operation> operatorStack, Stack<Expression> expressionStack, Func<Operation, bool> additionalCondision = null)
        {
            while (operatorStack.Count > 0 && !operatorStack.Peek().Equals(Operation.OpenParentheses))
            {
                if (!additionalCondision?.Invoke(operatorStack.Peek()) ?? false)
                {
                    return;
                }

                var right = expressionStack.Pop();

                var left = expressionStack.Pop();
                expressionStack.Push(operatorStack.Pop().Apply(left, right));
            }
        }

        private static Expression ReadOperand(string line, ref int index)
        {
            var decimalSeparator = culture.NumberFormat.NumberDecimalSeparator[0];
            var groupSeparator = culture.NumberFormat.NumberGroupSeparator[0];
            var operand = new StringBuilder();
            
            for (; index < line.Length; index++)
            {
                var next = line[index];
                if (char.IsDigit(next) || next == decimalSeparator || next == groupSeparator)
                {
                    operand.Append(next);
                }
                else
                {
                    break;
                }
            }

            return Expression.Constant(decimal.Parse(operand.ToString(), culture), typeof(decimal));
        }

        private static Operation ReadOperation(string line, ref int index)
        {
            var operation = new StringBuilder();
            
            for (; index < line.Length; index++)
            {
                var next = line[index];
                if (Operation.IsOperationSign(next))
                {
                    operation.Append(next);
                }
                else
                {
                    break;
                }
            }

            return Operation.From(operation.ToString());
        }

        private static string ReadWord(string line, ref int index)
        {
            var parameter = new StringBuilder();
            
            for (; index < line.Length; index++)
            {
                var next = line[index];
                if (!char.IsWhiteSpace(next) && !Operation.IsOperationSign(next) && next != '(' && next != ')')
                {
                    parameter.Append(next);
                }
                else
                {
                    break;
                }
            }

            return parameter.ToString();
        }
        
        internal sealed class Function
        {
            public static readonly Function Random = new Function("Random");
            
            
            public static readonly Dictionary<string, Function> Functions = new Dictionary<string, Function>
            {
                {"Random", Random}
            };
            
            private readonly string name;

            public Function(string name)
            {
                this.name = name;
            }

            public Expression Transform(string line, ref int index, ParameterExpression parameter)
            {
                var commaIndex = line.IndexOf(",", index);
                var closeParenthesIndex = line.IndexOf(")", commaIndex);

                var firstParameter = Parse(line, index + 1, commaIndex - 1, parameter);
                var secondParameter = Parse(line, commaIndex + 1, closeParenthesIndex - 1, parameter);

                index = closeParenthesIndex + 1;

                return Expression.Call(typeof(MathExtensions), "Random", null, firstParameter, secondParameter);
            }

        }

        internal sealed class Operation : IEquatable<Operation>
        {
            private static readonly HashSet<char> operationSign = new HashSet<char> {'+', '-', '*', '/', '=', '!', '>', '<'};
            private readonly Func<Expression, Expression, Expression> operation;
            private readonly Func<Expression, Expression> unaryOperation;
            public static readonly Operation Addition = new Operation(2, Expression.Add, "Addition");
            public static readonly Operation Subtraction = new Operation(2, Expression.Subtract, "Subtraction");
            public static readonly Operation Multiplication = new Operation(3, Expression.Multiply, "Multiplication");
            public static readonly Operation Division = new Operation(3, Expression.Divide, "Division");
            public static readonly Operation Equal = new Operation(1, Expression.Equal, "Equal");
            public static readonly Operation NotEqual = new Operation(1, Expression.NotEqual, "NotEqual");
            public static readonly Operation GreaterThan = new Operation(1, Expression.GreaterThan, "GreaterThan");
            public static readonly Operation GreaterThanOrEqual = new Operation(1, Expression.GreaterThanOrEqual, "GreaterThanOrEqual");
            public static readonly Operation LessThan = new Operation(1, Expression.LessThan, "LessThan");
            public static readonly Operation LessThanOrEqual = new Operation(1, Expression.LessThanOrEqual, "LessThanOrEqual");
            public static readonly Operation UnaryMinus = new Operation(1, Expression.Negate, "Negation");
            public static readonly Operation OpenParentheses = new Operation(-1, Expression.Constant, "OpenParentheses");
            public static readonly Operation CloseParentheses = new Operation(-1, Expression.Constant, "CloseParentheses");

            private static readonly Dictionary<string, Operation> operations = new Dictionary<string, Operation>
            {
                {"+", Addition},
                {"-", Subtraction},
                {"*", Multiplication},
                {"/", Division},
                {"==", Equal},
                {"!=", NotEqual},
                {"<", LessThan},
                {"<=", LessThanOrEqual},
                {">", GreaterThan},
                {">=", GreaterThanOrEqual},
            };

            private Operation(int precedence, string name)
            {
                Name = name;
                Precedence = precedence;
            }

            private Operation(int precedence, Func<Expression, Expression> unaryOperation, string name) : this(precedence, name)
            {
                this.unaryOperation = unaryOperation;
                NumberOfOperands = 1;
            }

            private Operation(int precedence, Func<Expression, Expression, Expression> operation, string name) : this(precedence, name)
            {
                this.operation = operation;
                NumberOfOperands = 2;
            }

            public string Name { get; }
            public int NumberOfOperands { get; }
            public int Precedence { get; }

            private Expression Apply(Expression expression)
            {
                return unaryOperation(expression);
            }

            private Expression Apply(Expression left, Expression right)
            {
                return operation(left, right);
            }

            public static bool IsOperationSign(char symbol)
            {
                return operationSign.Contains(symbol);
            }

            public Expression Apply(params Expression[] expressions)
            {
                if (expressions.Length == 1)
                {
                    return Apply(expressions[0]);
                }

                if (expressions.Length == 2)
                {
                    return Apply(expressions[0], expressions[1]);
                }

                throw new ArgumentException($"Не можем работать с массивом из {expressions.Length} выражений");
            }

            public bool Equals(Operation other)
            {
                if (ReferenceEquals(null, other))
                    return false;
                if (ReferenceEquals(this, other))
                    return true;
                return string.Equals(Name, other.Name);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj))
                    return false;
                if (ReferenceEquals(this, obj))
                    return true;
                return obj is Operation operation1 && Equals(operation1);
            }

            public override int GetHashCode()
            {
                return Name != null ? Name.GetHashCode() : 0;
            }

            public static Operation From(string symbol)
            {
                if (operations.TryGetValue(symbol, out var result))
                {
                    return result;
                }

                if (symbol == "(")
                {
                    return OpenParentheses;
                }

                if (symbol == ")")
                {
                    return CloseParentheses;
                }

                throw new ArgumentException($"Нет операции с символом {symbol}");
            }

            public static bool operator ==(Operation left, Operation right)
            {
                return Equals(left, right);
            }

            public static bool operator !=(Operation left, Operation right)
            {
                return !Equals(left, right);
            }
        }
    }
}