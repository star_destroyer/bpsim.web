﻿using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public interface IConfigurationBuilder
    {
        IConfigurationBuilder WithPipelineStep(ImitationPipelineStep step);

        IImitationConfiguration Build(ExpressionInfo endingRule);

        IImitationConfiguration EndWhenTicks(int ticks);

        IImitationConfiguration EndWhenExpression(string expression);
    }
}