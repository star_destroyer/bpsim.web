﻿using System.Collections.Generic;
using System.Threading;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public interface IContextFactory
    {
        Context Create(IDictionary<string, Resource> resources, IDictionary<string, OrderTemplate> orderTemplates, Node[] nodes, CancellationToken cancellation = default(CancellationToken));
    }
}