﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public interface IExpressionFactory
    {
        ExpressionInfo Create(string line);
    }
}