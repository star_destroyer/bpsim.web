﻿using System;
using System.Collections.Generic;

namespace BPsim.Web.Imitation.Builders
{
    public interface IExpressionParser
    {
        Func<IDictionary<string, decimal>, object> Parse(string line);
    }
}