﻿using System;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public class OrderProperyParameter
    {
        public OrderProperyParameter(string name, Func<Order, decimal> valueProvider)
        {
            Name = name;
            ValueProvider = valueProvider;
        }

        public string Name { get; }

        public Func<Order, decimal> ValueProvider { get; }
    }
}