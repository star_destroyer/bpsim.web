﻿using System.Text.RegularExpressions;

namespace BPsim.Web.Imitation.Builders
{
    public static class Patterns
    {
        public static readonly Regex DangerSymbols = new Regex(@"[\s+\-*/!<>=]+", RegexOptions.Compiled);
        public static readonly Regex ResourceRegex = new Regex(@"@""(?<name>[^+=->\*/<!\)\(]+)""", RegexOptions.Compiled);
        public static readonly Regex OrderRegex = new Regex(@"#""(?<name>[^+=->\*/<!\)\(.]+)"".(""(?<value>[^+=->\*\\<!\)\("".]+)""|(?<value>[^+=->\*\\<!\)\(\s]+))", RegexOptions.Compiled);
    }
}