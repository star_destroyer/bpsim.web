﻿using System;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Builders
{
    public class ResourceParameter
    {
        public ResourceParameter(string name, Func<Resource, decimal> valueProvider)
        {
            Name = name;
            ValueProvider = valueProvider;
        }

        public string Name { get; }

        public Func<Resource, decimal> ValueProvider { get; }
    }
}