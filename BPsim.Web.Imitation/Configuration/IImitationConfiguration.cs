﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Configuration
{
    public interface IImitationConfiguration
    {
        ImitationPipelineStep[] PipelineSteps { get; }

        ExpressionInfo EndExpression { get; }
    }
}