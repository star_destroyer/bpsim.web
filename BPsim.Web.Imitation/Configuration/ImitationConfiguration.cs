﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Configuration
{
    public class ImitationConfiguration : IImitationConfiguration
    {
        public ImitationPipelineStep[] PipelineSteps { get; set; }
        public ExpressionInfo EndExpression { get; set; }
    }
}