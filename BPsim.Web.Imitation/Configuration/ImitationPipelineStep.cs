﻿namespace BPsim.Web.Imitation.Configuration
{
    public enum ImitationPipelineStep
    {
        ExecuteFreeNode,
        ExecuteBusyNode,
        ExecuteAgents
    }
}