﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers
{
    public interface IOrderDispatcher
    {
        Order Create(OrderTemplate template);

        Order Get(string orderName, int number);

        Order Save(string orderName, Order order);

        void Remove(string orderName, Order order);
    }
}