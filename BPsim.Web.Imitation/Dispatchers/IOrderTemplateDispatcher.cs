﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers
{
    public interface IOrderTemplateDispatcher
    {
        OrderTemplate Get(string orderName);
    }
}