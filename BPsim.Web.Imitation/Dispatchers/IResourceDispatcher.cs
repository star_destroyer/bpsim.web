﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers
{
    public interface IResourceDispatcher
    {
        IDictionary<string, Resource> Get();

        Resource Get(string name);
    }
}