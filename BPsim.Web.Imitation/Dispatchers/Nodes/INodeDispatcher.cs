﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers.Nodes
{
    public interface INodeDispatcher
    {
        NodeInfo Get(string nodeName);

        Operation[] SelectOperations(NodeState state);

        Agent[] SelectAgents();

        void SetAsBusy(string nodeName, int unlockTick);

        void SetAsFree(string nodeName);
    }
}