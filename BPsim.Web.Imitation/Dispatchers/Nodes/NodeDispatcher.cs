﻿using System;
using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers.Nodes
{
    public class NodeDispatcher : INodeDispatcher
    {
        private readonly IDictionary<string, OperationInfo> freeOperations;
        private readonly IDictionary<string, OperationInfo> busyOperations;
        private readonly IDictionary<string, Agent> agents;

        public NodeDispatcher(Node[] nodes)
        {
            freeOperations = nodes.Where(x => x is Operation).ToDictionary(x => x.Name, x => new OperationInfo {Operation = (Operation) x, UnlockTick = -1});
            busyOperations = new Dictionary<string, OperationInfo>(freeOperations.Count);
            agents = nodes.Where(x => x is Agent).ToDictionary(x => x.Name, x => (Agent) x);
        }

        public NodeInfo Get(string nodeName)
        {
            if (freeOperations.TryGetValue(nodeName, out var freeOperation))
            {
                return new NodeInfo {Node = freeOperation.Operation, State = NodeState.Free};
            }

            if (busyOperations.TryGetValue(nodeName, out var busyOperation))
            {
                return new NodeInfo {Node = busyOperation.Operation, State = NodeState.Busy, UnlockTick = busyOperation.UnlockTick};
            }

            if (agents.TryGetValue(nodeName, out var agent))
            {
                return new NodeInfo {Node = agent, State = NodeState.Stateless};
            }

            throw new ArgumentOutOfRangeException(nameof(nodeName));
        }

        public Operation[] SelectOperations(NodeState state)
        {
            switch (state)
            {
                case NodeState.Free:
                    return freeOperations.Values.Select(x => x.Operation).ToArray();
                case NodeState.Busy:
                    return busyOperations.Values.Select(x => x.Operation).ToArray();
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        public Agent[] SelectAgents()
        {
            return agents.Values.ToArray();
        }

        public void SetAsBusy(string nodeName, int unlockTick)
        {
            var operation = freeOperations[nodeName];
            operation.UnlockTick = unlockTick;
            busyOperations[nodeName] = operation;
            freeOperations.Remove(nodeName);
        }

        public void SetAsFree(string nodeName)
        {
            var operation = busyOperations[nodeName];
            freeOperations[nodeName] = operation;
            busyOperations.Remove(nodeName);
        }

        private class OperationInfo
        {
            public Operation Operation { get; set; }

            public int UnlockTick { get; set; }
        }
    }
}