﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers.Nodes
{
    public class NodeInfo
    {
        public Node Node { get; set; }

        public NodeState State { get; set; }

        public int UnlockTick { get; set; }
    }
}