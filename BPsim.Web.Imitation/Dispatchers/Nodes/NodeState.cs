﻿namespace BPsim.Web.Imitation.Dispatchers.Nodes
{
    public enum NodeState
    {
        Stateless,
        Free,
        Busy
    }
}