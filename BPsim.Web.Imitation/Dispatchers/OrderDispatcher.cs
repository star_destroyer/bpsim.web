﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers
{
    public class OrderDispatcher : IOrderDispatcher
    {
        private readonly IDictionary<string, IDictionary<int, Order>> orders = new Dictionary<string, IDictionary<int, Order>>();
        private readonly IDictionary<string, int> ordersCounters;

        public OrderDispatcher(IEnumerable<string> orderNames)
        {
            ordersCounters = orderNames.ToDictionary(x => x, x => 0);
        }

        public Order Create(OrderTemplate template)
        {
            return new Order(-1, template.DefaultProps.ToDictionary(x => x.Key, x => x.Value));
        }

        public Order Get(string orderName, int number)
        {
            return orders[orderName][number];
        }

        public Order Save(string orderName, Order order)
        {
            if (!orders.ContainsKey(orderName))
            {
                orders[orderName] = new Dictionary<int, Order>();
            }

            order.Number = ordersCounters[orderName];
            orders[orderName][ordersCounters[orderName]] = order;
            ordersCounters[orderName]++;

            return order;
        }

        public void Remove(string orderName, Order order)
        {
            orders[orderName].Remove(order.Number);
        }
    }
}