﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers
{
    public class OrderTemplateDispatcher : IOrderTemplateDispatcher
    {
        private readonly IDictionary<string, OrderTemplate> templates;

        public OrderTemplateDispatcher(IDictionary<string, OrderTemplate> templates)
        {
            this.templates = templates;
        }

        public OrderTemplate Get(string orderName)
        {
            return templates[orderName];
        }
    }
}