﻿using System.Collections.Generic;

namespace BPsim.Web.Imitation.Dispatchers.Relationships
{
    public interface IOrderOwningDispatcher
    {
        bool IsOwn(string nodeName, string orderName);

        void Give(string nodeName, string orderName, int number);

        int? TakeFree(string nodeName, string orderName);

        void Lock(string lockerName, string nodeName, string orderName, int number);

        IDictionary<string, LockingInfo> GetLocked(string lockerName);

        void Unlock(string lockerName);

        void PickUp(string nodeName, string orderName, int number);
    }
}