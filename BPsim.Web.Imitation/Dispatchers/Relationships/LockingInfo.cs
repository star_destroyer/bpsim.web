﻿namespace BPsim.Web.Imitation.Dispatchers.Relationships
{
    public class LockingInfo
    {
        public StateInfo StateInfo { get; set; }

        public string LockerName { get; set; }

        public int Number { get; set; }
    }
}