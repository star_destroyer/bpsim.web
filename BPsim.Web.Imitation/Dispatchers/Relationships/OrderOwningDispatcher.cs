﻿using System.Collections.Generic;
using System.Linq;

namespace BPsim.Web.Imitation.Dispatchers.Relationships
{
    public class OrderOwningDispatcher : IOrderOwningDispatcher
    {
        private readonly IDictionary<string, Dictionary<string, StateInfo>> nodeToOrder;
        private readonly IDictionary<string, IDictionary<string, LockingInfo>> lockerToOrder;

        public OrderOwningDispatcher(string[] nodeNames)
        {
            nodeToOrder = nodeNames.ToDictionary(x => x, x => new Dictionary<string, StateInfo>());
            lockerToOrder = nodeNames.ToDictionary(x => x, x => (IDictionary<string, LockingInfo>) null);
        }

        public bool IsOwn(string nodeName, string orderName)
        {
            return nodeToOrder[nodeName].ContainsKey(orderName) && nodeToOrder[nodeName][orderName].Count > 0;
        }

        public void Give(string nodeName, string orderName, int number)
        {
            if (!nodeToOrder[nodeName].ContainsKey(orderName))
            {
                nodeToOrder[nodeName][orderName] = new StateInfo();
            }

            nodeToOrder[nodeName][orderName].Unlocked.Add(number);
        }

        public int? TakeFree(string nodeName, string orderName)
        {
            if (!nodeToOrder[nodeName].ContainsKey(orderName))
            {
                nodeToOrder[nodeName][orderName] = new StateInfo();
            }

            var unlocked = nodeToOrder[nodeName][orderName].Unlocked;
            return unlocked.Count > 0 ? (int?)unlocked.First() : null;
        }

        public void Lock(string lockerName, string nodeName, string orderName, int number)
        {
            var stateInfo = nodeToOrder[nodeName][orderName];
            stateInfo.Lock(number);
            if (lockerToOrder[lockerName] == null)
            {
                lockerToOrder[lockerName] = new Dictionary<string, LockingInfo>();
            }

            lockerToOrder[lockerName][orderName] = new LockingInfo
            {
                LockerName = nodeName,
                StateInfo = stateInfo,
                Number = number
            };
        }

        public IDictionary<string, LockingInfo> GetLocked(string lockerName)
        {
            return lockerToOrder[lockerName];
        }

        public void Unlock(string lockerName)
        {
            var lockingInfo = lockerToOrder[lockerName];

            if (lockingInfo == null)
            {
                return;
            }

            foreach (var infoByOrder in lockingInfo)
            {
                infoByOrder.Value.StateInfo.Unlock(infoByOrder.Value.Number);
            }

            lockerToOrder[lockerName] = null;
        }

        public void PickUp(string nodeName, string orderName, int number)
        {
            if (!nodeToOrder[nodeName].ContainsKey(orderName))
            {
                nodeToOrder[nodeName][orderName] = new StateInfo();
            }
            nodeToOrder[nodeName][orderName].Remove(number);
        }
    }
}