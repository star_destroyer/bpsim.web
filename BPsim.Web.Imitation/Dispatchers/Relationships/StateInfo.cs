﻿using System.Collections.Generic;

namespace BPsim.Web.Imitation.Dispatchers.Relationships
{
    public class StateInfo
    {
        public HashSet<int> Unlocked { get; } = new HashSet<int>();

        public HashSet<int> Locked { get; } = new HashSet<int>();

        public int Count => Unlocked.Count + Locked.Count;

        public void Lock(int value)
        {
            if (Unlocked.Contains(value))
            {
                Unlocked.Remove(value);
                Locked.Add(value);
            }
        }

        public void Unlock(int value)
        {
            if (Locked.Contains(value))
            {
                Locked.Remove(value);
                Unlocked.Add(value);
            }
        }

        public bool Remove(int value)
        {
            return Unlocked.Remove(value) || Locked.Remove(value);
        }
    }
}