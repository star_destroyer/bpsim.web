﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Dispatchers
{
    public class ResourceDispatcher : IResourceDispatcher
    {
        private readonly IDictionary<string, Resource> resources;

        public ResourceDispatcher(IDictionary<string, Resource> resources)
        {
            this.resources = resources;
        }

        public IDictionary<string, Resource> Get()
        {
            return resources;
        }

        public Resource Get(string name)
        {
            return resources[name];
        }
    }
}