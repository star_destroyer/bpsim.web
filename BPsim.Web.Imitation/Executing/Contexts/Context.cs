﻿using System.Threading;
using BPsim.Web.Imitation.Dispatchers;
using BPsim.Web.Imitation.Dispatchers.Nodes;
using BPsim.Web.Imitation.Dispatchers.Relationships;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation.Executing.Contexts
{
    public class Context : IContext
    {
        public int Tick { get; set; }
        public IResourceDispatcher ResourceDispatcher { get; set; }
        public IOrderTemplateDispatcher OrderTemplateDispatcher { get; set; }
        public IOrderDispatcher OrderDispatcher { get; set; }
        public INodeDispatcher NodeDispatcher { get; set; }
        public IOrderOwningDispatcher OrderOwningDispatcher { get; set; }
        public IReporter Reporter { get; set; }
        public CancellationToken CancellationToken { get; set; }
    }
}