﻿using System.Collections.Generic;

namespace BPsim.Web.Imitation.Executing.Contexts.Extensions
{
    public static class ContextExtensions
    {
        public static RuleContext ForRule(this IContext context, string nodeName, IDictionary<string, Stack<SelectingOrderInfo>> selectedOrders = null)
        {
            return new RuleContext
            {
                Tick = context.Tick,
                CancellationToken = context.CancellationToken,
                NodeDispatcher = context.NodeDispatcher,
                OrderDispatcher = context.OrderDispatcher,
                OrderOwningDispatcher = context.OrderOwningDispatcher,
                OrderTemplateDispatcher = context.OrderTemplateDispatcher,
                ResourceDispatcher = context.ResourceDispatcher,
                Reporter = context.Reporter,
                NodeName = nodeName,
                SelectedOrders = selectedOrders ?? new Dictionary<string, Stack<SelectingOrderInfo>>()
            };
        }
    }
}