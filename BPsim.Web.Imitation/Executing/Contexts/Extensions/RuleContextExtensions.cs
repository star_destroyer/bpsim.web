﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Contexts.Extensions
{
    public static class RuleContextExtensions
    {
        public static bool TryCheckOrderSelect(this RuleContext context, string orderName, out string message)
        {
            if (context.SelectedOrders.TryGetValue(orderName, out var orders) && orders.Peek().State == OrderContextState.Available)
            {
                message = null;
                return true;
            }

            message = $"Заявка {orderName} не захвачена";
            return false;
        }

        public static bool TryCheckOrderNotSelect(this RuleContext context, string orderName, out string message)
        {
            if (context.SelectedOrders.TryGetValue(orderName, out var orders) && orders.Peek().State == OrderContextState.Available)
            {
                message = $"Заявка {orderName} уже захвачена";
                return false;
            }

            message = null;
            return true;
        }

        public static bool TryCheckCanOwn(this Context context, string orderName, string nodeName, out string message)
        {
            var orderTemplate = context.OrderTemplateDispatcher.Get(orderName);
            var node = context.NodeDispatcher.Get(nodeName).Node;

            if (!node.IsManyOwner && !orderTemplate.IsManyOwnered && context.OrderOwningDispatcher.IsOwn(nodeName, orderName))
            {
                message = $"Узел не может владеть еще одной заявкой {orderName}";
                return false;
            }

            message = null;
            return true;
        }

        public static void SelectOrder(this RuleContext context, string orderName, int number, string owner)
        {
            SelectOrder(context, orderName, context.OrderDispatcher.Get(orderName, number), owner);
        }

        public static void SelectOrder(this RuleContext context, string orderName, Order order, string owner)
        {
            var selectingOrders = context.SelectedOrders.TryGetValue(orderName, out var selectedOrders) ? selectedOrders : new Stack<SelectingOrderInfo>();
            selectingOrders.Push(new SelectingOrderInfo(order, OrderContextState.Available, owner));
            context.SelectedOrders[orderName] = selectingOrders;
        }

        public static Dictionary<string, Order> GetFreeSelectedOrders(this RuleContext context)
        {
            return context.SelectedOrders.Where(x => x.Value.Peek().State == OrderContextState.Available).ToDictionary(x => x.Key, x => x.Value.Peek().Order);
        }
    }
}