﻿using System.Threading;
using BPsim.Web.Imitation.Dispatchers;
using BPsim.Web.Imitation.Dispatchers.Nodes;
using BPsim.Web.Imitation.Dispatchers.Relationships;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation.Executing.Contexts
{
    //TODO (byTimo) можно попробовать закинуть в LogicContext потока
    public interface IContext
    {
        int Tick { get; set; }
        IResourceDispatcher ResourceDispatcher { get; }
        IOrderTemplateDispatcher OrderTemplateDispatcher { get; }
        IOrderDispatcher OrderDispatcher { get; set; }
        INodeDispatcher NodeDispatcher { get; }
        IOrderOwningDispatcher OrderOwningDispatcher { get; }
        IReporter Reporter { get; }
        CancellationToken CancellationToken { get; }
    }
}