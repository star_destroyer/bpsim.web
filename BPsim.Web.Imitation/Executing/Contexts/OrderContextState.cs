﻿namespace BPsim.Web.Imitation.Executing.Contexts
{
    public enum OrderContextState
    {
        Available,
        NotAvailable
    }
}