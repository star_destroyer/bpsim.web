﻿using System.Collections.Generic;

namespace BPsim.Web.Imitation.Executing.Contexts
{
    public class RuleContext : Context
    {
        public string NodeName { get; set; }
        public IDictionary<string, Stack<SelectingOrderInfo>> SelectedOrders { get; set; }
    }
}