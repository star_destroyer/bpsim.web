﻿using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Contexts
{
    public class SelectingOrderInfo
    {
        public SelectingOrderInfo(Order order, OrderContextState state, string ownNode)
        {
            Order = order;
            State = state;
            OwnNode = ownNode;
        }

        public Order Order { get; }

        public OrderContextState State { get; set; }

        public string OwnNode { get; }
    }
}