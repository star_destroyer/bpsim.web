﻿using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Contexts.Extensions;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public class AgentExecutor : IAgentExecutor
    {
        private readonly IRuleExecutor ruleExecutor;
        private readonly IKnowledgeExecutor knowledgeExecutor;

        public AgentExecutor(IRuleExecutor ruleExecutor, IKnowledgeExecutor knowledgeExecutor)
        {
            this.ruleExecutor = ruleExecutor;
            this.knowledgeExecutor = knowledgeExecutor;
        }

        public Result Execute(IContext context, Agent agent)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            var ruleContext = context.ForRule(agent.Name);

            var canExecuteResult = ruleExecutor.CanExecute(ruleContext, agent.GlobalRules);

            if (!canExecuteResult.IsOk)
            {
                return Result.Fail($"Глобавльные правила агента {agent.Name} не могут быть выполнены\r\n -> {canExecuteResult.Error}");
            }

            //TODO log
            var executeResult = ruleExecutor.Execute(ruleContext, agent.GlobalRules);

            foreach (var knowledge in agent.Knowledges)
            {
                var knowledgeExecuteResult = knowledgeExecutor.Execute(ruleContext, knowledge.Value);

                if (knowledgeExecuteResult.IsOk)
                {
                    context.Reporter.IncKnowledge(agent.Name, knowledge.Key);
                }
            }

            return Result.Ok();
        }
    }
}