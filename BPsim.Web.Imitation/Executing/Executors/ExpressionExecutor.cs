﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public class ExpressionExecutor : IExpressionExecutor
    {
        public ExpressionResult Execute(ExpressionInfo expression, IDictionary<string, Resource> resources, IDictionary<string, Order> orders, int tick = 0)
        {
            var parameters = new Dictionary<string, decimal>();
            try
            {
                foreach (var requireOrder in expression.OrderValueProviders)
                {
                    foreach (var property in requireOrder.Value)
                    {
                        parameters[property.Value.Name] = property.Value.ValueProvider.Invoke(orders[requireOrder.Key]);
                    }
                }

                foreach (var resource in expression.ResourceValueProviders)
                {
                    parameters[resource.Value.Name] = resource.Value.ValueProvider(resources[resource.Key]);
                }
            } catch (Exception e)
            {
                throw new ArgumentException("Не можем выполнить выражение.", e);
            }

            parameters["Tick"] = tick;

            var result = expression.Expression.Invoke(parameters);

            return new ExpressionResult {Result = result};
        }
    }
}