﻿using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public interface IAgentExecutor
    {
        Result Execute(IContext context, Agent agent);
    }
}