﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public interface IExpressionExecutor
    {
        ExpressionResult Execute(ExpressionInfo expression, IDictionary<string, Resource> resources, IDictionary<string, Order> orders, int tick = 0);
    }
}