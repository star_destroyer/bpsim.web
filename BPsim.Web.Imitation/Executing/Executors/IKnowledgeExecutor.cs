﻿using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public interface IKnowledgeExecutor
    {
        Result Execute(RuleContext context, Knowledge knowledge);
    }
}