﻿using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public interface IOperationExecutor
    {
        Result ExecuteAsFree(IContext context, Operation operation);

        Result ExecuteAsBusy(IContext context, Operation operation);
    }
}