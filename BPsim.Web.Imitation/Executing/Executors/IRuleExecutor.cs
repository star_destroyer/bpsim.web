﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public interface IRuleExecutor
    {
        Result CanExecute(RuleContext context, IEnumerable<IRule> rules);

        Result Execute(RuleContext context, IEnumerable<IRule> rules);
    }
}