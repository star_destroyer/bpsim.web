﻿using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public class KnowledgeExecutor : IKnowledgeExecutor
    {
        private readonly IRuleExecutor ruleExecutor;

        public KnowledgeExecutor(IRuleExecutor ruleExecutor)
        {
            this.ruleExecutor = ruleExecutor;
        }

        public Result Execute(RuleContext context, Knowledge knowledge)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            var canExecuteResult = ruleExecutor.CanExecute(context, knowledge.Rules);

            if (!canExecuteResult.IsOk)
            {
                return Result.Fail($"Знание на может быть выполнено\r\n -> {canExecuteResult.Error}");
            }

            //TODO log
            var executeResult = ruleExecutor.Execute(context, knowledge.Rules);

            return Result.Ok();
        }
    }
}