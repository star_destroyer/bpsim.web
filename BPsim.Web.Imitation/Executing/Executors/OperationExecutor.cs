﻿using System.Collections.Generic;
using System.Linq;
using BPsim.Web.Imitation.Dispatchers.Nodes;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Contexts.Extensions;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public class OperationExecutor : IOperationExecutor
    {
        private readonly IRuleExecutor ruleExecutor;
        private readonly IExpressionExecutor expressionExecutor;

        public OperationExecutor(IRuleExecutor ruleExecutor, IExpressionExecutor expressionExecutor)
        {
            this.ruleExecutor = ruleExecutor;
            this.expressionExecutor = expressionExecutor;
        }

        public Result ExecuteAsFree(IContext context, Operation operation)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            var ruleContext = context.ForRule(operation.Name);

            var canExecuteResult = ruleExecutor.CanExecute(ruleContext, operation.InRules);

            if (!canExecuteResult.IsOk)
            {
                return Result.Fail($"Узел не может быть выполнен\r\n -> {canExecuteResult.Error}");
            }

            var durationCanExecute = operation.Duration.CanExecute(ruleContext);

            if (!durationCanExecute.IsOk)
            {
                return Result.Fail($"Время блокировки узла не может быть расчитано\r\n -> {durationCanExecute.Error}");
            }

            //TODO log
            var executeResult = ruleExecutor.Execute(ruleContext, operation.InRules);

            foreach (var orderInfo in ruleContext.SelectedOrders)
            {
                if (orderInfo.Value.Count == 0 && orderInfo.Value.Peek().State == OrderContextState.NotAvailable)
                {
                    continue;
                }

                var order = orderInfo.Value.Peek();

                context.OrderOwningDispatcher.Lock(operation.Name, order.OwnNode, orderInfo.Key, order.Order.Number);
            }

            var result = operation.Duration.Execute(ruleContext) as ExpressionResult;

            context.NodeDispatcher.SetAsBusy(operation.Name, context.Tick + (result.TryGetDecimal(out var value) ? (int) value : 0));

            context.Reporter.SetNodeState(operation.Name, NodeState.Busy);

            return Result.Ok();
        }

        public Result ExecuteAsBusy(IContext context, Operation operation)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            if (context.Tick < context.NodeDispatcher.Get(operation.Name).UnlockTick)
            {
                return Result.Ok();
            }

            var selectedOrder = context.OrderOwningDispatcher.GetLocked(operation.Name)?
                .ToDictionary(x => x.Key,
                              x => new Stack<SelectingOrderInfo>(new[]
                              {
                                  new SelectingOrderInfo(context.OrderDispatcher.Get(x.Key, x.Value.Number),
                                                         OrderContextState.Available,
                                                         x.Value.LockerName)
                              }));

            var ruleContext = context.ForRule(operation.Name, selectedOrder);

            var canExecuteResult = ruleExecutor.CanExecute(ruleContext, operation.OutRules);

            if (!canExecuteResult.IsOk)
            {
                return Result.Fail($"Узел не может быть выполнен\r\n -> {canExecuteResult.Error}");
            }

            //TODO log
            var executeResult = ruleExecutor.Execute(ruleContext, operation.OutRules);

            context.OrderOwningDispatcher.Unlock(operation.Name);
            context.NodeDispatcher.SetAsFree(operation.Name);

            context.Reporter.SetNodeState(operation.Name, NodeState.Free);

            return Result.Ok();
        }
    }
}