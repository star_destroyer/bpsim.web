﻿using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Executing.Executors
{
    public class RuleExecutor : IRuleExecutor
    {
        public Result CanExecute(RuleContext context, IEnumerable<IRule> rules)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            foreach (var rule in rules)
            {
                var result = rule.CanExecute(context);

                if (!result.IsOk)
                {
                    return Result.Fail($"Правила не могут быть выполнены\r\n -> {result.Error}");
                }
            }

            return Result.Ok();
        }

        public Result Execute(RuleContext context, IEnumerable<IRule> rules)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            foreach (var rule in rules)
            {
                //TODO log
                var result = rule.Execute(context);
            }

            return Result.Ok();
        }
    }
}