﻿using System;

namespace BPsim.Web.Imitation.Executing
{
    public static class MathExtensions
    {
        private static readonly Random random = new Random();

        public static decimal Random(decimal from, decimal to)
        {
            return random.Next((int)from, (int)to);
        }

        public static double Normal(double mu = 0, double sigma = 1)
        {
            var u1 = random.NextDouble();
            var u2 = random.NextDouble();

            var randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                                Math.Sin(2.0 * Math.PI * u2);

            var randNormal = mu + sigma * randStdNormal;

            return randNormal;
        }
    }
}