﻿namespace BPsim.Web.Imitation.Executing.Results
{
    public class ExpressionResult : Result
    {
        public object Result { get; set; }

        public bool TryGetBool(out bool value)
        {
            if (Result is bool b)
            {
                value = b;
                return true;
            }

            value = false;
            return false;
        }

        public bool TryGetDecimal(out decimal value)
        {
            if (Result is decimal b)
            {
                value = b;
                return true;
            }

            value = 0;
            return false;
        }

        public decimal Decimal()
        {
            return (decimal) Result;
        }
    }
}