﻿namespace BPsim.Web.Imitation.Executing.Results
{
    public class Result
    {
        //TODO (byTimo) в идеале бы цеплять не только ошибки но и нормальные ситуации. Например, захватили такую то заявку
        private string error;

        public bool IsOk { get; private set; } = true;

        public string Error
        {
            get => error;
            set
            {
                error = value;
                IsOk = false;
            }
        }

        public static Result Ok()
        {
            return new Result();
        }

        public static Result Fail(string message)
        {
            return new Result {Error = message};
        }
    }
}