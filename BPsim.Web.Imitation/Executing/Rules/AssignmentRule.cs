﻿using System;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Contexts.Extensions;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public class AssignmentRule : Rule
    {
        private readonly ExpressionRule expressionRule;
        private readonly string resourceName;
        private readonly string orderName;
        private readonly string propertyName;

        public AssignmentRule(string resourceName, ExpressionInfo expressionInfo, IExpressionExecutor expressionExecutor)
            : this(expressionInfo, expressionExecutor)
        {
            this.resourceName = resourceName;
        }

        public AssignmentRule(string orderName, string propertyName, ExpressionInfo expressionInfo, IExpressionExecutor expressionExecutor)
            : this(expressionInfo, expressionExecutor)
        {
            this.orderName = orderName;
            this.propertyName = propertyName;
        }

        private AssignmentRule(ExpressionInfo expressionInfo, IExpressionExecutor expressionExecutor)
        {
            expressionRule = new ExpressionRule(expressionInfo, expressionExecutor);
        }

        public override Result CanExecute(RuleContext context)
        {
            var result = expressionRule.CanExecute(context);
            if (!(result is ExpressionResult) || !result.IsOk)
            {
                return result;
            }

            var expressionResult = result as ExpressionResult;

            if (expressionResult.TryGetBool(out var boolean))
            {
                return Result.Fail("Нельзя присвоить булево значение ресурсу или свойтсу заявки");
            }

            if (resourceName != null)
            {
                var resource = context.ResourceDispatcher.Get(resourceName);
                var isAssignable = resource.IsAssignable(expressionResult.Decimal());
                return isAssignable ? Result.Ok() : Result.Fail($"Нельзя назначить значение {expressionResult.Decimal()} ресурсу {resourceName}");
            }
            else if (orderName != null && propertyName != null)
            {
                var selectedOrders = context.GetFreeSelectedOrders();
                return selectedOrders.ContainsKey(orderName) ? Result.Ok() : Result.Fail($"Заявка {orderName} не захвачена");
            }

            throw new ArgumentException("Имя ресурса и имя заявки или имя свойства == null");
        }

        public override Result Execute(RuleContext context)
        {
            var result = expressionRule.Execute(context);
            if (!(result is ExpressionResult) || !result.IsOk)
            {
                return result;
            }

            var expressionResult = result as ExpressionResult;

            if (expressionResult.TryGetBool(out var boolean))
            {
                throw new ArgumentException("Нельзя присвоить булево значение ресурсу или свойтсу заявки");
            }

            if (resourceName != null)
            {
                var resource = context.ResourceDispatcher.Get(resourceName);
                resource.Value = expressionResult.Decimal();
                context.Reporter.ChangeResource(resourceName, expressionResult.Decimal());
                return Result.Ok();
            }
            else if (orderName != null && propertyName != null)
            {
                context.SelectedOrders[orderName].Peek().Order.Props[propertyName].Value = expressionResult.Decimal();
                return Result.Ok();
            }

            throw new ArgumentException($"Имя ресурса и имя заявки или имя свойства == null");
        }
    }
}