﻿using System;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Contexts.Extensions;
using BPsim.Web.Imitation.Executing.Results;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public class CreateOrderRule : RuleWithOrder
    {
        public CreateOrderRule(string orderName)
            : base(orderName)
        {
        }

        public override Result CanExecute(RuleContext context)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            if (!context.TryCheckOrderNotSelect(OrderName, out var errorMessage))
            {
                return Result.Fail(errorMessage);
            }

            if (!context.TryCheckCanOwn(OrderName, context.NodeName, out errorMessage))
            {
                return Result.Fail(errorMessage);
            }

            var order = context.OrderDispatcher.Create(context.OrderTemplateDispatcher.Get(OrderName));
            context.SelectOrder(OrderName, order, context.NodeName);

            return Result.Ok();
        }

        public override Result Execute(RuleContext context)
        {
            if (!context.SelectedOrders.TryGetValue(OrderName, out var selectedOrders))
            {
                throw new ArgumentException($"Попытка выполнить правило, когда заявка {OrderName} не захвачена.");
            }

            var orderInfo = selectedOrders.Peek();
            var order = context.OrderDispatcher.Save(OrderName, orderInfo.Order);
            context.OrderOwningDispatcher.Give(context.NodeName, OrderName, order.Number);
            orderInfo.Order.Number = order.Number;

            context.Reporter.CreateOrder(OrderName);

            return Result.Ok();
        }

        public override string ToString()
        {
            return $"Create order rule {OrderName}";
        }
    }
}