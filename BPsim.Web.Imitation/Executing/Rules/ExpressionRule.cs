﻿using System;
using System.Linq;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public class ExpressionRule : Rule
    {
        public readonly ExpressionInfo expression;
        private readonly IExpressionExecutor expressionExecutor;

        public ExpressionRule(ExpressionInfo expression, IExpressionExecutor expressionExecutor)
        {
            this.expression = expression;
            this.expressionExecutor = expressionExecutor;
        }

        public override Result CanExecute(RuleContext context)
        {
            try
            {
                var result = Execute(context) as ExpressionResult;
                if (result.TryGetBool(out var boolean))
                {
                    return boolean ? Result.Ok() : Result.Fail("Выражение ложно");
                }

                return result;
            } catch (ArgumentException e) when (e.Message.StartsWith("Не можем выполнить выражение."))
            {
                return Result.Fail(e.Message);
            }
        }

        public override Result Execute(RuleContext context)
        {
            var resources = context.ResourceDispatcher.Get();
            var selectedOrders = context.SelectedOrders.Where(x => x.Value.Count > 0).ToDictionary(x => x.Key, x => x.Value.Peek().Order);

            var result = expressionExecutor.Execute(expression, resources, selectedOrders, context.Tick);

            return result;
        }

        public override string ToString()
        {
            return $"Expression Rule {expression.Script}";
        }
    }
}