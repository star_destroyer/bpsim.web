﻿using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public interface IRule
    {
        Result CanExecute(RuleContext context);
        Result Execute(RuleContext context);
    }
}