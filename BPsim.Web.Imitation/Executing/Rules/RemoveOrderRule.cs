﻿using System;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Contexts.Extensions;
using BPsim.Web.Imitation.Executing.Results;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public class RemoveOrderRule : RuleWithOrder
    {
        public RemoveOrderRule(string orderName)
            : base(orderName)
        {
        }

        public override Result CanExecute(RuleContext context)
        {
            if (!context.TryCheckOrderSelect(OrderName, out var errorMessage))
            {
                return Result.Fail(errorMessage);
            }

            context.SelectedOrders[OrderName].Peek().State = OrderContextState.NotAvailable;

            return Result.Ok();
        }

        public override Result Execute(RuleContext context)
        {
            if (!context.SelectedOrders.ContainsKey(OrderName))
            {
                throw new ArgumentException($"Попытка выполнить правило, когда заявка {OrderName} не захвачена.");
            }

            var selectingOrderInfo = context.SelectedOrders[OrderName].Pop();

            context.OrderDispatcher.Remove(OrderName, selectingOrderInfo.Order);
            context.OrderOwningDispatcher.PickUp(selectingOrderInfo.OwnNode, OrderName, selectingOrderInfo.Order.Number);

            context.Reporter.DeleteOrder(OrderName);

            return Result.Ok();
        }

        public override string ToString()
        {
            return $"Remove order rule {OrderName}";
        }
    }
}