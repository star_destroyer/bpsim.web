﻿using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Results;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public abstract class Rule : IRule
    {
        public abstract Result CanExecute(RuleContext context);

        public abstract Result Execute(RuleContext context);
    }
}