﻿namespace BPsim.Web.Imitation.Executing.Rules
{
    public abstract class RuleWithOrder : Rule
    {
        protected readonly string OrderName;

        protected RuleWithOrder(string orderName)
        {
            OrderName = orderName;
        }
    }
}