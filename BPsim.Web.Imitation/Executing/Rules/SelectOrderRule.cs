﻿using System;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Contexts.Extensions;
using BPsim.Web.Imitation.Executing.Results;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public class SelectOrderRule : RuleWithOrder
    {
        private readonly string targetNodeName;

        public SelectOrderRule(string orderName, string targetNodeName)
            : base(orderName)
        {
            this.targetNodeName = targetNodeName;
        }

        public override Result CanExecute(RuleContext context)
        {
            context.CancellationToken.ThrowIfCancellationRequested();

            if (!context.TryCheckOrderNotSelect(OrderName, out var selectedMessage))
            {
                return Result.Fail(selectedMessage);
            }

            var node = targetNodeName ?? context.NodeName;
            var selectableOrder = context.OrderOwningDispatcher.TakeFree(node, OrderName);

            if (selectableOrder == null)
            {
                return Result.Fail($"Узел {node} не владеет свободными заявками {OrderName}");
            }

            context.SelectOrder(OrderName, selectableOrder.Value, node);

            return Result.Ok();
        }

        public override Result Execute(RuleContext context)
        {
            if (!context.SelectedOrders.TryGetValue(OrderName, out var selectedOrders))
            {
                throw new ArgumentException($"Попытка выполнить правило, когда заявка {OrderName} не захвачена.");
            }

            return Result.Ok();
        }

        public override string ToString()
        {
            return $"Select order rule {OrderName} from {targetNodeName ?? "self"}";
        }
    }
}