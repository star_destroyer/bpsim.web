﻿using System;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Contexts.Extensions;
using BPsim.Web.Imitation.Executing.Results;

namespace BPsim.Web.Imitation.Executing.Rules
{
    public class SendOrderRule : RuleWithOrder
    {
        private readonly string targetNodeName;

        public SendOrderRule(string orderName, string targetNodeName)
            : base(orderName)
        {
            this.targetNodeName = targetNodeName;
        }

        public override Result CanExecute(RuleContext context)
        {
            if (!context.TryCheckOrderSelect(OrderName, out var errorMessage))
            {
                return Result.Fail(errorMessage);
            }

            if (!context.TryCheckCanOwn(OrderName, targetNodeName, out errorMessage))
            {
                return Result.Fail(errorMessage);
            }

            context.SelectedOrders[OrderName].Peek().State = OrderContextState.NotAvailable;

            return Result.Ok();
        }

        public override Result Execute(RuleContext context)
        {
            if (!context.SelectedOrders.TryGetValue(OrderName, out var selectedOrders))
            {
                throw new ArgumentException($"Попытка выполнить правило, когда заявка {OrderName} не захвачена.");
            }

            var selectedOrderInfo = selectedOrders.Pop();
            context.OrderOwningDispatcher.PickUp(selectedOrderInfo.OwnNode, OrderName, selectedOrderInfo.Order.Number);
            context.OrderOwningDispatcher.Give(targetNodeName, OrderName, selectedOrderInfo.Order.Number);

            return Result.Ok();
        }

        public override string ToString()
        {
            return $"Send order rule {OrderName} to {targetNodeName}";
        }
    }
}