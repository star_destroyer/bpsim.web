﻿using System.Text.RegularExpressions;
using BPsim.Web.Imitation.Builders;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Factories
{
    public class ExpressionRuleFactory : IExpressionRuleFactory
    {
        private readonly IExpressionFactory expressionFactory;
        private readonly IExpressionExecutor expressionExecutor;
        private static readonly Regex beginFromResourceAssign = new Regex($@"^\s*{Patterns.ResourceRegex}\s*=[^=]", RegexOptions.Compiled);
        private static readonly Regex beginFromOrderAssign = new Regex($@"^\s*{Patterns.OrderRegex}\s*=[^=]", RegexOptions.Compiled);

        public ExpressionRuleFactory(IExpressionFactory expressionFactory, IExpressionExecutor expressionExecutor)
        {
            this.expressionFactory = expressionFactory;
            this.expressionExecutor = expressionExecutor;
        }
        
        public IRule Create(string line)
        {
            var resourceMatch = beginFromResourceAssign.Match(line);

            if (resourceMatch.Success)
            {
                var resourceName = resourceMatch.Groups["name"].Value;
                var expression = expressionFactory.Create(line.Substring(resourceMatch.Length - 1));
                return new AssignmentRule(resourceName, expression, expressionExecutor);
            }

            var orderMatch = beginFromOrderAssign.Match(line);

            if (orderMatch.Success)
            {
                var orderName = orderMatch.Groups["name"].Value;
                var orderPropertyName = orderMatch.Groups["value"].Value;
                var expression = expressionFactory.Create(line.Substring(orderMatch.Length - 1));
                return new AssignmentRule(orderName, orderPropertyName, expression, expressionExecutor);
            }

            return new ExpressionRule(expressionFactory.Create(line), expressionExecutor);
        }
    }
}