﻿using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Factories
{
    public interface IExpressionRuleFactory
    {
        IRule Create(string line);
    }
}