﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation
{
    public interface IImitationEngine
    {
        Task Run(ImitationTask imitationTask, Func<int, Report, CancellationToken, Task> reportCallback, CancellationToken cancellation);
    }
}