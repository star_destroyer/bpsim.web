﻿namespace BPsim.Web.Imitation
{
    public interface IImitator
    {
        ImitationStepResult Imitate(ImitationTask task);
    }
}