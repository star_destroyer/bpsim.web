﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation
{
    public class ImitationEngine : IImitationEngine
    {
        private readonly IImitator imitator;

        public ImitationEngine(IImitator imitator)
        {
            this.imitator = imitator;
        }

        public async Task Run(ImitationTask imitationTask, Func<int, Report, CancellationToken, Task> reportCallback, CancellationToken cancellation)
        {
            var tick = 1;
            var result = new ImitationStepResult();
            while (!result.IsEnd && !cancellation.IsCancellationRequested)
            {
                imitationTask.Context.Tick = tick;
                result = imitator.Imitate(imitationTask);
                if (reportCallback != null)
                {
                    await reportCallback.Invoke(tick, result.Report, cancellation).ConfigureAwait(false);
                }

                tick++;
            }
        }
    }
}