﻿using System;
using BPsim.Web.Imitation.Reporting;

namespace BPsim.Web.Imitation
{
    public class ImitationStepResult
    {
        public Guid Id { get; set; }

        public int Tick { get; set; }

        public bool IsEnd { get; set; }

        public Report Report { get; set; }
    }
}