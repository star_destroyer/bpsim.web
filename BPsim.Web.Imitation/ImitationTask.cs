﻿using System;
using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.Executing.Contexts;

namespace BPsim.Web.Imitation
{
    public class ImitationTask
    {
        public Guid Id { get; set; }

        public Guid ModelId { get; set; }

        public IContext Context { get; set; }

        public IImitationConfiguration Configuration { get; set; }
    }
}