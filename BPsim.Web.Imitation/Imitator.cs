﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Configuration;
using BPsim.Web.Imitation.Dispatchers.Nodes;
using BPsim.Web.Imitation.Executing.Contexts;
using BPsim.Web.Imitation.Executing.Executors;
using BPsim.Web.Imitation.Executing.Results;
using BPsim.Web.Imitation.Structure;

namespace BPsim.Web.Imitation
{
    public class Imitator : IImitator
    {
        private readonly IOperationExecutor operationExecutor;
        private readonly IAgentExecutor agentExecutor;
        private readonly IExpressionExecutor expressionExecutor;

        public Imitator(IOperationExecutor operationExecutor,
                        IAgentExecutor agentExecutor,
                        IExpressionExecutor expressionExecutor)
        {
            this.operationExecutor = operationExecutor;
            this.agentExecutor = agentExecutor;
            this.expressionExecutor = expressionExecutor;
        }

        public ImitationStepResult Imitate(ImitationTask task)
        {
            var context = task.Context;
            using (context.Reporter)
            {
                try
                {
                    ExecuteStep(context, task.Configuration);
                    var endingRuleExecutionResult = expressionExecutor.Execute(
                        task.Configuration.EndExpression,
                        context.ResourceDispatcher.Get(),
                        new Dictionary<string, Order>(),
                        context.Tick
                    );

                    return new ImitationStepResult
                    {
                        Id = task.Id,
                        Tick = context.Tick,
                        IsEnd = !endingRuleExecutionResult.TryGetBool(out var boolean) || boolean,
                        Report = context.Reporter.Report
                    };
                }
                catch (Exception e)
                {
                    throw new ImitationException($"Возникла ошибка в время имитации. Tick: {context.Tick}", e);
                }
            }
        }

        private Result ExecuteStep(IContext context, IImitationConfiguration configuration)
        {
            foreach (var pipelineStep in configuration.PipelineSteps)
            {
                switch (pipelineStep)
                {
                    case ImitationPipelineStep.ExecuteFreeNode:
                        ExecuteFreeNode(context);
                        break;
                    case ImitationPipelineStep.ExecuteBusyNode:
                        ExecuteBusyNode(context);
                        break;
                    case ImitationPipelineStep.ExecuteAgents:
                        ExecuteAgents(context);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(pipelineStep));
                }
            }

            return Result.Ok();
        }

        private void ExecuteAgents(IContext context)
        {
            foreach (var agent in context.NodeDispatcher.SelectAgents())
            {
                var agentResult = agentExecutor.Execute(context, agent);
                //TODO (byTimo) зацеплять информацию по ошибкам выполения
            }
        }

        private void ExecuteBusyNode(IContext context)
        {
            foreach (var operation in context.NodeDispatcher.SelectOperations(NodeState.Busy))
            {
                var operationResult = operationExecutor.ExecuteAsBusy(context, operation);
                //TODO (byTimo) зацеплять информацию по ошибкам выполения
            }
        }

        private void ExecuteFreeNode(IContext context)
        {
            foreach (var operation in context.NodeDispatcher.SelectOperations(NodeState.Free))
            {
                var operationResult = operationExecutor.ExecuteAsFree(context, operation);
                //TODO (byTimo) зацеплять информацию по ошибкам выполения
            }
        }
    }
}