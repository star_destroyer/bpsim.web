﻿using System;
using BPsim.Web.Imitation.Dispatchers.Nodes;

namespace BPsim.Web.Imitation.Reporting
{
    public interface IReporter : IDisposable
    {
        Report Report { get; }

        void ChangeResource(string name, decimal value);

        void CreateOrder(string name);

        void DeleteOrder(string name);

        void SetNodeState(string name, NodeState state);

        void IncKnowledge(string agentName, string knowledgeName);
    }
}