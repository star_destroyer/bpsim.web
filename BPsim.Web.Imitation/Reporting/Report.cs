﻿using System.Collections.Generic;

namespace BPsim.Web.Imitation.Reporting
{
    public class Report
    {
        public IDictionary<string, decimal> Resources { get; } = new Dictionary<string, decimal>();

        public IDictionary<string, int> Orders { get; } = new Dictionary<string, int>();

        public IDictionary<string, bool> Operations { get; } = new Dictionary<string, bool>();

        public IDictionary<string, IDictionary<string, int>> Agents { get; } = new Dictionary<string, IDictionary<string, int>>();
    }
}