﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Dispatchers.Nodes;

namespace BPsim.Web.Imitation.Reporting
{
    public class Reporter : IReporter
    {
        private Report report;

        public Report Report => report ?? (report = new Report());

        public void Dispose()
        {
            report = null;
        }

        public void ChangeResource(string name, decimal value)
        {
            Report.Resources[name] = value;
        }

        public void CreateOrder(string name)
        {
            Report.Orders[name] = Report.Orders.ContainsKey(name) ? Report.Orders[name] + 1 : 1;
        }

        public void DeleteOrder(string name)
        {
            Report.Orders[name] = Report.Orders.ContainsKey(name) ? Report.Orders[name] - 1 : 1;
        }

        public void SetNodeState(string name, NodeState state)
        {
            switch (state)
            {
                case NodeState.Free:
                    Report.Operations[name] = false;
                    break;
                case NodeState.Busy:
                    Report.Operations[name] = true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }

        public void IncKnowledge(string agentName, string knowledgeName)
        {
            if (!Report.Agents.ContainsKey(agentName))
            {
                Report.Agents[agentName] = new Dictionary<string, int>();
            }

            Report.Agents[agentName][knowledgeName] = Report.Agents[agentName].ContainsKey(knowledgeName) ? Report.Agents[agentName][knowledgeName] + 1 : 1;
        }
    }
}