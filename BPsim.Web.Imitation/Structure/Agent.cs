﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Structure
{
    public class Agent : Node
    {
        public Agent(string name) 
            : base(name)
        {
        }
        
        private IRule[] globalRules;
        private IDictionary<string, Knowledge> knowledges;

        public IRule[] GlobalRules { get => globalRules ?? (globalRules = Array.Empty<IRule>()); set => globalRules = value; }

        public IDictionary<string, Knowledge> Knowledges { get => knowledges ?? (knowledges = new Dictionary<string, Knowledge>()); set => knowledges = value; }
    }
}