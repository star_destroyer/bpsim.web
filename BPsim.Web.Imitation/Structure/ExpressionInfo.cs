﻿using System;
using System.Collections.Generic;
using BPsim.Web.Imitation.Builders;

namespace BPsim.Web.Imitation.Structure
{
    public class ExpressionInfo
    {
        public ExpressionInfo(string script,
                              Func<IDictionary<string, decimal>, object> expression,
                              IDictionary<string, ResourceParameter> resourceValueProviders,
                              IDictionary<string, IDictionary<string, OrderProperyParameter>> orderValueProviders
        )
        {
            Script = script;
            Expression = expression;
            ResourceValueProviders = resourceValueProviders;
            OrderValueProviders = orderValueProviders;
        }

        public string Script { get; }

        public Func<IDictionary<string, decimal>, object> Expression { get; }

        public IDictionary<string, ResourceParameter> ResourceValueProviders { get; }

        public IDictionary<string, IDictionary<string, OrderProperyParameter>> OrderValueProviders { get; }
    }
}