﻿using System;
using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Structure
{
    public class Knowledge
    {
        private IRule[] rules;

        public IRule[] Rules { get => rules ?? (rules = Array.Empty<IRule>()); set => rules = value; }

        //TODO Priority
    }
}