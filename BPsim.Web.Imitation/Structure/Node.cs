﻿namespace BPsim.Web.Imitation.Structure
{
    public abstract class Node
    {
        protected Node(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public bool IsManyOwner { get; set; }

        //TODO Priority
    }
}