﻿using System;
using BPsim.Web.Imitation.Executing.Rules;

namespace BPsim.Web.Imitation.Structure
{
    public class Operation : Node
    {
        public Operation(string name) 
            : base(name)
        {
        }
        
        private IRule[] inRules;
        private IRule[] outRules;

        public IRule[] InRules { get => inRules ?? (inRules = Array.Empty<IRule>()); set => inRules = value; }

        public IRule[] OutRules { get => outRules ?? (outRules = Array.Empty<IRule>()); set => outRules = value; }

        public ExpressionRule Duration { get; set; }
    }
}