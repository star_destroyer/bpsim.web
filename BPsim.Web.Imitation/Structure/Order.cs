﻿using System.Collections.Generic;
using System.Linq;

namespace BPsim.Web.Imitation.Structure
{
    public class Order
    {
        public Order(int number, IDictionary<string, decimal> props)
        {
            Number = number;
            Props = props.ToDictionary(x => x.Key, x => new OrderProperty(x.Value));
        }

        public int Number { get; set; }

        public IDictionary<string, OrderProperty> Props { get; }
    }
}