﻿namespace BPsim.Web.Imitation.Structure
{
    public class OrderProperty
    {
        public OrderProperty(decimal value)
        {
            Value = value;
        }

        public decimal Value { get; set; }
    }
}