﻿using System.Collections.Generic;

namespace BPsim.Web.Imitation.Structure
{
    public class OrderTemplate
    {
        public IDictionary<string, decimal> DefaultProps { get; set; }

        public bool IsManyOwnered { get; set; }

        public static OrderTemplate Default => new OrderTemplate {DefaultProps = new Dictionary<string, decimal>(), IsManyOwnered = false};
    }
}