﻿namespace BPsim.Web.Imitation.Structure
{
    public class Resource
    {
        private readonly decimal min;
        private readonly decimal max;

        private decimal value;

        public Resource(decimal value, decimal min = decimal.MinValue, decimal max = decimal.MaxValue)
        {
            this.min = min;
            this.max = max;
            CheckDoubleValue(value);
            this.value = value;
        }

        public decimal Value
        {
            get => value;
            set
            {
                CheckDoubleValue(value);
                this.value = value;
            }
        }

        public bool IsAssignable(object number)
        {
            if (number is decimal n)
            {
                return n >= min && n <= max;
            }

            return false;
        }

        private void CheckDoubleValue(decimal number)
        {
            if (!IsAssignable(number))
                throw new ResourceInvalidValueException($"Value {number} должно быть между {min} и {max}");
        }
    }
}