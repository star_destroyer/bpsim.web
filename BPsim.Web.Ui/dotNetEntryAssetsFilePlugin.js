const fs = require("fs");
const path = require("path");

class DotNetEntryAssetsFilePlugin {
    constructor(buildDir, filename, prefix){
        this.buildDir = buildDir;
        this.filename = filename;
        this.prefix = prefix;
    }

    apply(compiler) {
        compiler.hooks.done.tap("DotNetEntryAssetsFilePlugin", stats => {
            const assetsByChunkName = stats.toJson().assetsByChunkName;
            const result = DotNetEntryAssetsFilePlugin.__createWebpackAssetsInfo(assetsByChunkName, this.prefix);
            const fullName = path.resolve(this.buildDir, this.filename);
            if (!fs.existsSync(this.buildDir)) {
                fs.mkdirSync(this.buildDir);
            }
            fs.writeFileSync(fullName, JSON.stringify(result));
        });
    }

    static __createWebpackAssetsInfo(assetsByChunkName, prefix) {
        const result = {};
        for (const key in assetsByChunkName) {
            if (assetsByChunkName.hasOwnProperty(key)) {
                const assets = assetsByChunkName[key];
                if (Array.isArray(assets)) {
                    for (const asset of assets) {
                        const keyByExtension = path.extname(asset) === ".map" ? `${key}SourceMap` : key;
                        result[keyByExtension] = prefix + asset;
                    }
                } else {
                    result[key] = prefix + assetsByChunkName[key];
                }
            }
        }
        return result;
    }
}

module.exports = DotNetEntryAssetsFilePlugin;