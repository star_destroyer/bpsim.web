import CancellationToken from "../Cancellations/cancellationToken";
import PromiseHelper from "../Utility/PromiseHelper";
import {LocalStorageManager} from "../LocalStorageManager";

export class HttpError extends Error {
    readonly status: number;

    constructor(status: number, statusText: string) {
        super(statusText);
        this.status = status;
        Object.setPrototypeOf(this, HttpError.prototype);
    }
}

export interface RequestResult<T> {
    success: boolean;
    message: string | null;
    content: T | null
}

export default class ApiHelper {
    static async get<T>(url: string, cancellation?: CancellationToken): Promise<RequestResult<T>> {
        return this.send<T>(
            url,
            {
                method: "GET",
                credentials: "include",
                mode: "cors"
            },
            cancellation);
    }

    static async post<T>(url: string, data?: any, cancellation?: CancellationToken): Promise<RequestResult<T>> {
        return this.send<T>(
            url,
            {
                method: "POST",
                credentials: "include",
                mode: "cors",
                headers: {
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            },
            cancellation);
    }

    static async put<T>(url: string, data?: any, cancellation?: CancellationToken): Promise<RequestResult<T>> {
        return this.send<T>(
            url,
            {
                method: "PUT",
                credentials: "include",
                mode: "cors",
                headers: {
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            },
            cancellation);
    }

    static async delete<T>(url: string, data?: any, cancellation?: CancellationToken): Promise<RequestResult<T>> {
        return this.send<T>(
            url,
            {
                method: "DELETE",
                credentials: "include",
                mode: "cors",
                headers: {
                    "Accept": "application/json, text/plain, */*",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            },
            cancellation);
    }

    private static async send<T>(input: RequestInfo, init: RequestInit, cancellation?: CancellationToken): Promise<RequestResult<T>> {
        const token = LocalStorageManager.get("token");
        if (token) {
            init.headers = {...init.headers, "Authorization": `Bearer ${token}`};
        }

        const promise = fetch(input, init);
        const cancellable = PromiseHelper.withCancellation(promise, cancellation || CancellationToken.none);
        const response = await PromiseHelper.resolveAfter(cancellable, 300);
        const contentType = this.parseContentType(response);
        const content = await this.parseContent<T>(response, contentType);

        if (response.ok) {
            return {
                success: true,
                message: null,
                content
            };
        }

        if (response.status >= 400 && response.status <= 499) {
            return {
                success: false,
                message: content as any,
                content: null
            };
        }

        throw new HttpError(response.status, content as any);
    }

    private static parseContentType(response: Response): string {
        const contentTypeHeader = response.headers.get("content-type") || "";
        return contentTypeHeader.split(";")[0];
    }

    private static async parseContent<T>(response: Response, contentType: string): Promise<T> {
        switch (contentType) {
            case "application/octet-stream":
                return await response.arrayBuffer() as any;
            case "application/json":
                return await response.json() as Promise<T>;
            default:
                return await response.text() as any;
        }
    }
}
