import ApiHelper, {RequestResult} from "./ApiHelper";

export class AuthApi {
    static authorize(login: string, password: string): Promise<RequestResult<string>> {
        return ApiHelper.post("/authorization", {login, password});
    }
}