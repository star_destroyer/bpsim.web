import ApiHelper, {RequestResult} from "./ApiHelper";
import {Model, ModelSummary} from "../Data/Model";
import {Contract} from "../Data/Contract";
import {CreateTaskParameters} from "../Data/Task";

export class ModelApi {
    static select(): Promise<RequestResult<Contract<ModelSummary>[]>> {
        return ApiHelper.get("/model");
    };

    static get(id: string): Promise<RequestResult<Contract<Model>>> {
        return ApiHelper.get(`/model/${id}`);
    }

    static create(name: string): Promise<RequestResult<Contract<Model>>> {
        return ApiHelper.post(`/model?name=${name}`);
    }

    static delete(id: string): Promise<RequestResult<void>> {
        return ApiHelper.delete(`/model/${id}`);
    }

    static save(id: string, data: Model): Promise<RequestResult<void>> {
        return ApiHelper.put(`/model/${id}`, data);
    }
}
