import ApiHelper, {RequestResult} from "./ApiHelper";
import {CreateTaskParameters, ImitationTask} from "../Data/Task";
import {Contract} from "../Data/Contract";

export class TaskApi {
    static select = (id: string): Promise<RequestResult<Contract<ImitationTask>[]>> => {
        return ApiHelper.get(`/model/${id}/task`);
    };

    static get = (id: string): Promise<RequestResult<Contract<ImitationTask>>> => {
        return ApiHelper.get(`/task/${id}`);
    };

    static start(id: string, data: CreateTaskParameters): Promise<RequestResult<string>> {
        return ApiHelper.post(`/model/${id}/task`, data);
    }
}
