import ApiHelper, {RequestResult} from "./ApiHelper";
import {User} from "../Data/User";

export default class UserApi {
    static register(login: string, password: string): Promise<RequestResult<string>> {
        return ApiHelper.post<string>("/user", {login, password});
    }

    static get(): Promise<RequestResult<User>> {
        return ApiHelper.get("/user");
    }
}