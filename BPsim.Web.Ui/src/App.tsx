import * as React from "react";
import {Redirect, Route, Switch} from "react-router";
import NotFoundPage from "./Components/NotFoundPage";
import {UserContextProvider} from "./Components/Authorization/UserContext";
import {StartPage} from "./Components/Authorization/StartPage";
import {ProtectedRoute} from "./Components/ProtectedRoute";
import {ModelListPage} from "./Components/Model/ModelBuilding/ModelListPage";
import {AppNavigationBar} from "./Components/AppNavigationBar";
import {AppProgressBar} from "./Components/AppProgressBar";
import {ModelPage} from "./Components/Model/ModelPage";
import {ModelContextProvider} from "./Components/Model/ModelContext";

require("./App.less");

export class App extends React.Component {
    render(): React.ReactNode {
        return (
            <UserContextProvider>
                <ModelContextProvider>
                    <>
                        <AppNavigationBar/>
                        <AppProgressBar/>
                        <Switch>
                            <ProtectedRoute path={"/model/:id"} component={ModelPage}/>
                            <ProtectedRoute path={"/model"} component={ModelListPage} exact/>
                            <Route path="/auth/login" component={StartPage}/>
                            <Route path="/auth/register" component={StartPage}/>
                            <Route path="/auth">
                                {() => <Redirect to="/auth/login"/>}
                            </Route>
                            <Route path={"/"}>
                                {() => <Redirect to={"/model"}/>}
                            </Route>
                            <Route component={NotFoundPage}/>
                        </Switch>
                    </>
                </ModelContextProvider>
            </UserContextProvider>
        );

    }
}
