import * as React from "react";
import {Button, H6, Menu, Navbar, NavbarDivider, NavbarGroup, NavbarHeading, Popover, Tooltip} from "@blueprintjs/core";
import {UserContextValue, withUser} from "./Authorization/UserContext";
import {Link} from "react-router-dom";
import {ModelContextValue, withModel} from "./Model/ModelContext";
import Gapped from "./Layouts/Gapped";

export interface AppNavigationBarProps extends UserContextValue, ModelContextValue {

}

class AppNavigationBarInternal extends React.Component<AppNavigationBarProps> {
    render() {
        return (
            <Navbar
                className="pt-dark pt-fixed-top"
            >
                <div style={{width: "70%", margin: "0 auto"}}>
                    <NavbarGroup>
                        <NavbarHeading>
                            BPsim.Web
                        </NavbarHeading>
                        <NavbarDivider/>
                        {this.props.user && (
                            <Link to={"/"}>
                                <Tooltip content={"Models list"}>
                                    <Button icon={"align-justify"} minimal/>
                                </Tooltip>
                            </Link>
                        )}
                        {this.props.model && (
                            <>
                                <NavbarDivider/>
                                <Gapped gap={20} alignItems={"baseline"}>
                                    <Gapped>
                                        Model:
                                        {this.props.model.data.name}
                                    </Gapped>
                                    <>
                                        <Link to={`/model/${this.props.model.id}`}>
                                            <Button icon={"build"} minimal>Edit</Button>
                                        </Link>
                                        <Link to={`/model/${this.props.model.id}/imitation`}>
                                            <Button icon={"play"} minimal>Imitation</Button>
                                        </Link>
                                    </>
                                </Gapped>
                            </>
                        )}
                    </NavbarGroup>
                    {this.props.user && (
                        <>
                            <NavbarGroup align={"right"}>
                                <Popover position={"bottom"}>
                                    <Button icon={"user"} minimal/>
                                    <Menu>
                                        <H6 className={"bp3-menu-header"}>{this.props.user.login}</H6>
                                        <Menu.Item text={"Logout"} icon={"log-out"}
                                                   onClick={() => this.props.logout()}/>
                                    </Menu>
                                </Popover>
                            </NavbarGroup>
                        </>
                    )}
                </div>
            </Navbar>
        );
    }
}

export const AppNavigationBar = withModel(withUser(AppNavigationBarInternal));
