import * as React from "react";
import {UserContextValue, withUser} from "./Authorization/UserContext";
import {ProgressBar} from "@blueprintjs/core";

class AppProgressBarInternal extends React.Component<UserContextValue> {
    render(): React.ReactNode {
        if(!this.props.loading) {
            return null;
        }

        return (
            <ProgressBar/>
        )
    }
}

export const AppProgressBar = withUser(AppProgressBarInternal);