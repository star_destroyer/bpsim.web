import * as React from "react";
import {Button, FormGroup} from "@blueprintjs/core";
import Gapped from "./Layouts/Gapped";

export interface ArrayEditFormProps<TItem> {
    title: string;
    array: TItem[];
    renderItem: (item: TItem, index: number) => React.ReactNode;
    onAdd: () => void;
    onDelete: (item: TItem, index: number) => void;
}

export class ArrayEditForm<TItem> extends React.Component<ArrayEditFormProps<TItem>> {
    render(): React.ReactNode {
        return (
            <FormGroup label={this.renderAddButton()}>
                <Gapped vertical gap={5}>
                    {this.props.array.map((x, i) => (
                        <Gapped gap={5} key={i} alignItems={"baseline"}>
                            {this.props.renderItem(x, i)}
                            <Button icon="trash" onClick={() => this.props.onDelete(x, i)} minimal/>
                        </Gapped>
                    ))}
                </Gapped>
            </FormGroup>
        );
    }

    private renderAddButton = (): React.ReactNode => {
        return (
            <Gapped alignItems={"baseline"}>
                <span>{this.props.title}</span>
                <Button icon={"add"} minimal onClick={this.props.onAdd}/>
            </Gapped>
        );
    };
}
