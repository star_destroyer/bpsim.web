import * as React from "react";
import {Button, InputGroup, FormGroup} from "@blueprintjs/core";
import {IconNames} from "@blueprintjs/icons";
import {UserContextValue, withUser} from "./UserContext";
import PasswordInput from "./PasswordInput";

export interface LoginFormState {
    login: string;
    password: string;
}

class LoginFormInternal extends React.Component<UserContextValue, LoginFormState> {
    state: LoginFormState = {
        login: "",
        password: ""
    };

    render() {
        return (
            <form>
                <FormGroup label={"Login"} disabled={this.props.loading}>
                    <InputGroup
                        leftIcon={IconNames.PERSON}
                        placeholder="Input your login"
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                            this.setState({login: e.currentTarget.value})}
                        disabled={this.props.loading}
                    />
                </FormGroup>
                <FormGroup label="Password" disabled={this.props.loading}>
                    <PasswordInput
                        value={this.state.password}
                        onChanged={password => this.setState({password})}
                        disabled={this.props.loading}
                    />
                </FormGroup>
                <Button
                    onClick={this.handleLoginClicked}
                    disabled={this.props.loading}
                >
                    Login
                </Button>
            </form>
        );
    }

    private handleLoginClicked = (): void => {
        this.props.login(this.state.login, this.state.password);
    };
}

export const LoginForm = withUser(LoginFormInternal);