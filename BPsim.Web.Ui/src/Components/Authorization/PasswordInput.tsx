import * as React from "react";
import {Button, InputGroup, Intent, Tooltip} from "@blueprintjs/core";
import {IconNames} from "@blueprintjs/icons";

export interface PasswordInputProps {
    value: string;
    onChanged: (value: string) => void;
    disabled?: boolean;
}

type InputType = "text" | "password";

interface PasswordInputState {
    inputType: InputType;
}

export default class PasswordInput extends React.Component<PasswordInputProps, PasswordInputState> {
    state: PasswordInputState = {
        inputType: "password"
    };

    render(): React.ReactNode {
        return (
            <InputGroup
                value={this.props.value}
                placeholder={"Input your password"}
                type={this.state.inputType}
                leftIcon={IconNames.LOCK}
                onChange={(e: React.FormEvent<HTMLInputElement>) => this.props.onChanged(e.currentTarget.value)}
                rightElement={this.renderRightIcon()}
                disabled={this.props.disabled}
            />
        );
    }

    private renderRightIcon = (): JSX.Element => {
        const isPassword = this.state.inputType === "password";
        const tooltipContent = isPassword ? "Show password" : "Hide password";
        const icon = isPassword ? IconNames.EYE_OPEN : IconNames.EYE_OFF;
        return (
            <Tooltip content={tooltipContent}>
                <Button
                    small
                    icon={icon}
                    onClick={this.handleInputTypeChanged}
                    disabled={this.props.disabled}
                />
            </Tooltip>
        );
    };

    private handleInputTypeChanged = (): void => {
        this.setState(state => ({inputType: state.inputType === "password" ? "text" : "password"}));
    };
}