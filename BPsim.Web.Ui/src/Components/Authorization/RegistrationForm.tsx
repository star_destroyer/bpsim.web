import * as React from "react";
import {Button, FormGroup, InputGroup} from "@blueprintjs/core";
import PasswordInput from "./PasswordInput";
import {UserContextValue, withUser} from "./UserContext";
import {IconNames} from "@blueprintjs/icons";
import UserApi from "../../Api/UserApi";

type RegistrationFormStatus = "clear" | "fetch" | "error";

interface RegistrationFormState {
    name: string;
    password: string;
    status: RegistrationFormStatus,
    message: string | null;
}

class RegistrationFormInternal extends React.Component<UserContextValue, RegistrationFormState> {
    state: RegistrationFormState = {
        name: "",
        password: "",
        status: "clear",
        message: null
    };

    render(): React.ReactNode {
        const disabled = this.state.status === "fetch" || this.props.loading;
        return (
            <form>
                <FormGroup label={"Login"} disabled={disabled}>
                    <InputGroup
                        leftIcon={IconNames.PERSON}
                        value={this.state.name}
                        placeholder={"Input your login"}
                        onChange={this.handleNameChanged}
                        disabled={disabled}
                    />
                </FormGroup>
                <FormGroup label={"Password"} disabled={disabled}>
                    <PasswordInput
                        value={this.state.password}
                        onChanged={this.handlePasswordChanged}
                        disabled={disabled}
                    />
                </FormGroup>
                <Button onClick={this.handleRegisterClicked} disabled={disabled}>Register</Button>
                {this.state.message}
            </form>
        );
    }

    private handleNameChanged = (e: React.FormEvent<HTMLInputElement>): void => {
        const value = e.currentTarget.value;
        this.setState({name: value});
    };

    private handlePasswordChanged = (password: string): void => {
        this.setState({password});
    };

    private handleRegisterClicked = (): void => {
        this.setState({status: "fetch"}, async () => {
            const result = await UserApi.register(this.state.name, this.state.password);
            if (result.success) {
                this.props.login(this.state.name, this.state.password);
            } else {
                this.setState({status: "error", message: result.message});
            }
        });
    };
}

export const RegistrationForm = withUser(RegistrationFormInternal);
