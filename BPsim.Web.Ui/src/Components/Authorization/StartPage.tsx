import * as React from "react";
import {LoginForm} from "./LoginForm";
import {RegistrationForm} from "./RegistrationForm";
import {UserContextValue, withUser} from "./UserContext";
import {Card, Tab, Tabs} from "@blueprintjs/core";
import {CenterLayout} from "../Layouts/CenterLayout";
import {RouteComponentProps} from "react-router";

type StartPageTabs = "login" | "register";

interface StartPageInternalProps extends UserContextValue, RouteComponentProps {
}


class StartPageInternal extends React.Component<StartPageInternalProps> {
    render() {
        const active = this.extractTabFromPath();
        return (
            <CenterLayout>
                <div style={{width: 500}}>
                    <Card>
                        <Tabs animate selectedTabId={active} onChange={this.handleTabChanged}>
                            <Tab id={"login"} title={"Login"} panel={<LoginForm/>} disabled={this.props.loading}/>
                            <Tab id={"register"} title={"Registration"} panel={<RegistrationForm/>}
                                 disabled={this.props.loading}/>
                        </Tabs>
                    </Card>
                </div>
            </CenterLayout>
        );
    }

    private handleTabChanged = (id: StartPageTabs) => {
        this.props.history.push(`/auth/${id}`)
    };

    private extractTabFromPath = (): StartPageTabs => {
        return this.props.match.path.split("/")[2] as StartPageTabs;
    };
}

export const StartPage = withUser(StartPageInternal);