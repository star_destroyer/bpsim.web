import * as React from "react";
import {User} from "../../Data/User";
import {Omit} from "../../Utility/Types";
import {AuthApi} from "../../Api/AuthApi";
import {LocalStorageManager} from "../../LocalStorageManager";
import UserApi from "../../Api/UserApi";

export interface UserContextValue {
    user: User | null;
    loading: boolean;
    login: (name: string, password: string) => void;
    logout: () => void;
}

export const UserContext = React.createContext<UserContextValue>(
    {
        user: null,
        loading: false,
        login: () => undefined,
        logout: () => undefined
    });

interface UserContextProviderState {
    user: User | null;
    loading: boolean;
}

export class UserContextProvider extends React.Component<{}, UserContextProviderState> {
    state: UserContextProviderState = {
        user: null,
        loading: true
    };

    componentDidMount(): void {
        this.loadUser();
    }

    render(): React.ReactNode {
        const value: UserContextValue = {
            user: this.state.user,
            loading: this.state.loading,
            login: this.handleLogin,
            logout: this.handleLogout
        };
        return (
            <UserContext.Provider value={value}>
                {this.props.children}
            </UserContext.Provider>
        );
    }

    private handleLogin = (name: string, password: string): void => {
        this.setState({loading: true}, async () => {
            const result = await AuthApi.authorize(name, password);
            if (!result.success) {
                this.setState({loading: false});
            } else {
                LocalStorageManager.save("token", result.content);
                this.loadUser();
            }
        });
    };

    private handleLogout = (): void => {
        LocalStorageManager.remove("token");
        this.setState({
            user: null,
            loading: false
        });
    };

    private loadUser = (): void => {
        if (LocalStorageManager.get("token") == null) {
            this.setState({loading: false})
        } else {
            this.setState({loading: true}, async () => {
                const result = await UserApi.get();
                if (!result.success) {
                    LocalStorageManager.remove("token");
                    this.setState({loading: false});
                }
                this.setState({user: result.content, loading: false});
            });
        }
    };
}

export const withUser = <TProps extends UserContextValue>(component: React.ComponentType<TProps>) => {
    return React.forwardRef((props: Omit<TProps, keyof UserContextValue>, ref) => (
        <UserContext.Consumer>
            {value => React.createElement(component, {...props as any, ...value, ref})}
        </UserContext.Consumer>
    ));
};
