import * as React from "react";
import GraphView, { GraphEdge, GraphNode } from "react-digraph";
import { edgeTypes, nodeTypes } from "./Shapes";
import { renderDefs, renderNode } from "./GraphRendering";

export interface GraphProps {
    nodes: GraphNode<number>[];
    edges: GraphEdge<number>[];
    selected: GraphNode<number> | GraphEdge<number> | {};
    onSelect: (id: number) => void;
    onUpdate: (id: number, update: (node: Node) => Node) => void;
    onDelete: (id: number) => void;
}

export interface GraphState {

}

class Graph extends React.Component<GraphProps, GraphState> {
    render() {
        return (
            <GraphView
                style={{margin: "50px 0 0 0"}}
                nodeKey={"id"}
                emptyType={"operation"}
                nodes={this.props.nodes}
                edges={this.props.edges}
                selected={this.props.selected}
                nodeTypes={nodeTypes}
                nodeSubtypes={{}}
                edgeTypes={edgeTypes}
                getViewNode={this.getViewNode}
                onSelectNode={this.handleSelectElement}
                onCreateNode={this.handleCreateNode}
                onUpdateNode={this.handleUpdateNode}
                onDeleteNode={this.handleDeleteNode}
                onSelectEdge={this.handleSelectElement}
                onCreateEdge={this.handleCreateEdge}
                onSwapEdge={this.handleSwapEdge}
                onDeleteEdge={this.handleDeleteEdge}
                enableFocus={false}
                graphControls={false}
                gridDot={5}
                light="#FFFFFF"
                primary="#137CBD"
                gridSpacing={100}
                renderNode={(graph, g, node) => renderNode(graph, g, node)}
                renderDefs={graph => renderDefs(graph)}
            />
        );
    }

    private getViewNode = (id: number) => this.props.nodes.find(x => x.id === id);

    private handleSelectElement = (element: GraphNode<number> | GraphEdge<number>) =>
        element && this.props.onSelect(element.id)

    private handleCreateNode = (x: number, y: number) => {
        // const node: GraphNode = {
        //     id: Date().toString(),
        //     x: x,
        //     y: y,
        //     type: "operation",
        // };
        // this.setState((state: GraphState) => ({nodes: [...state.model.nodes, node]}));
    }

    private handleUpdateNode = (graphNode: GraphNode<number>) => {
        this.props.onUpdate(graphNode.id, node => ({
            ...node,
            x: graphNode.x,
            y: graphNode.y
        }));
    }

    private handleDeleteNode = (node: GraphNode<number>) => this.props.onDelete(node.id);

    private handleCreateEdge = (source: GraphNode<number>, target: GraphNode<number>) => {
        // if (source.id === target.id) {
        //     return;
        // }
        // const edge: GraphEdge = {
        //     source: source.id,
        //     target: target.id,
        //     type: "simple"
        // };
        // this.setState((state: GraphState) => ({edges: [...state.edges, edge]}));
    }

    private handleSwapEdge = (source: GraphNode<number>, target: GraphNode<number>, edge: GraphEdge<number>) => {
        // this.setState((state: GraphState) => ({
        //     edges: state.edges.map(x => x.source === edge.source ? {target: target.id, ...x} : x)
        // }));
    }

    private handleDeleteEdge = (edge: GraphEdge<number>) => {
        // this.setState((state: GraphState) => ({edges: state.edges.filter(x => x.source === edge.source)}));
    }
}
