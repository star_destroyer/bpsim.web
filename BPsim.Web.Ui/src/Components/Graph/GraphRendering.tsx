import * as React from "react";
import GraphView, { GraphNode } from "react-digraph";
import * as d3 from "d3";

export function renderNode<TId>(graphView: GraphView<TId>, domNode: SVGGElement, graphNode: GraphNode<TId>) {
    // For new nodes, add necessary child domNodes
    if (!domNode.hasChildNodes()) {
        d3.select(domNode).append("use").classed("shape", true)
            .attr("x", -graphView.props.nodeSize / 2)
            .attr("y", -graphView.props.nodeSize / 2)
            .attr("width", graphView.props.nodeSize)
            .attr("height", graphView.props.nodeSize);
    }

    let style = graphView.getNodeStyle(graphNode, graphView.props.selected);

    d3.select(domNode)
        .attr("style", style);

    d3.select(domNode).select("use.shape")
        .attr("xlink:href", (x: GraphNode<TId>) => graphView.props.nodeTypes[x.type].shapeId);

    d3.select(domNode).selectAll("text").remove();

    d3.select(domNode).append("text")
        .attr("text-anchor", "middle")
        .attr("fill", "black")
        .attr("stroke-width", "0")
        .attr("font-size", "20px")
        .attr("font-family", "monospace")
        .attr("dy", 50)
        .text(graphNode.title ? graphNode.title : " ");

    d3.select(domNode).attr("transform", graphView.getNodeTransformation);
}

export function renderDefs<TId>(graph: GraphView<TId>): React.ReactNode {
    const {
        edgeArrowSize,
        gridSpacing,
        gridDot,
        nodeTypes,
        nodeSubtypes,
        edgeTypes
    } = graph.props;

    let defIndex = 0;
    let graphConfigDefs: React.ReactNode[] = [];

    Object.keys(nodeTypes).forEach(type => {
        defIndex += 1;
        graphConfigDefs.push(React.cloneElement(nodeTypes[type].shape, { key: defIndex }));
    });

    Object.keys(nodeSubtypes).forEach(type => {
        defIndex += 1;
        graphConfigDefs.push(React.cloneElement(nodeSubtypes[type].shape, { key: defIndex }));
    });

    Object.keys(edgeTypes).forEach(type => {
        defIndex += 1;
        graphConfigDefs.push(React.cloneElement(edgeTypes[type].shape, { key: defIndex }));
    });

    return (
        <defs>
            {graphConfigDefs}

            <marker
                id="end-arrow"
                key="end-arrow"
                viewBox={`0 -${edgeArrowSize / 2} ${edgeArrowSize} ${edgeArrowSize}`}
                refX={`${edgeArrowSize / 2}`}
                markerWidth={`${edgeArrowSize}`}
                markerHeight={`${edgeArrowSize}`}
                orient="auto"
            >
                <path d={`M0,-${edgeArrowSize / 2}L${edgeArrowSize},0L0,${edgeArrowSize / 2}`} />
            </marker>

            <pattern
                id="grid"
                key="grid"
                width={gridSpacing}
                height={gridSpacing}
                patternUnits="userSpaceOnUse"
            >
                <circle
                    cx={gridSpacing / 2}
                    cy={gridSpacing / 2}
                    r={gridDot}
                    fill="lightgray"
                />
            </pattern>

            <filter id="dropshadow">
                <feOffset in="SourceAlpha" dx={1} dy={1} />
                <feGaussianBlur stdDeviation="7" />
                <feComponentTransfer>
                    <feFuncA type="linear" slope="0.3" />
                </feComponentTransfer>
                <feMerge>
                    <feMergeNode />
                    <feMergeNode in="SourceGraphic" />
                </feMerge>
            </filter>

            <filter id="dropshadow1" key="dropshadow" height="130%">
                <feGaussianBlur in="SourceAlpha" stdDeviation="3" />
                <feOffset dx="2" dy="2" result="offsetblur" />
                <feComponentTransfer>
                    <feFuncA type="linear" slope="0.1" />
                </feComponentTransfer>
                <feMerge>
                    <feMergeNode />
                    <feMergeNode in="SourceGraphic" />
                </feMerge>
            </filter>

        </defs>
    );
}