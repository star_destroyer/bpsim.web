/* tslint:disable */
import * as React from "react";
import {ElementTypes} from "react-digraph";

export const OperationShape = (
    <g id="operation" transform="translate(25,0)">
        <rect
            width="100"
            height="100"
            stroke="#5C7080"
            strokeWidth={2}
            rx="5" ry="5"
        />
    </g>
);

export const AgentShape = (
    <g id="agent" transform="translate(25,0)">
        <circle
            cx="50"
            cy="50"
            r="50"
            stroke="#5C7080"
            strokeWidth={2}
        />
    </g>
);

export const SimpleEdgeShape = (
    <symbol viewBox="0 0 50 50" id="simpleEdge"/>
);

export const edgeTypes: ElementTypes = {
    simple: {
        shape: SimpleEdgeShape,
        shapeId: "#simple"
    }
};

export const nodeTypes: ElementTypes = {
    agent: {
        shapeId: "#agent",
        shape: AgentShape,
    },
    operation: {
        shapeId: "#operation",
        shape: OperationShape,
    }
};
