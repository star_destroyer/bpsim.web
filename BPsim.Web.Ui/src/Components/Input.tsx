import * as React from "react";
import {Hotkey, Hotkeys, IInputGroupProps, InputGroup} from "@blueprintjs/core";
import {HotkeysTarget} from "@blueprintjs/core/lib/esnext";
import {HTMLInputProps} from "@blueprintjs/core/src/common/props";

export interface InputProps extends IInputGroupProps {
    onApply?: () => void;
    onCancel?: () => void;
}

@HotkeysTarget
export class Input extends React.Component<InputProps & HTMLInputProps> {
    render() {
        return <InputGroup {...this.props}/>;
    }

    renderHotkeys() {
        return (
            <Hotkeys>
                <Hotkey combo="enter" label={"apply"} onKeyDown={this.props.onApply} allowInInput/>
                <Hotkey label={"cancel"} combo={"esc"} onKeyDown={this.props.onCancel} allowInInput/>
            </Hotkeys>
        );
    }
}