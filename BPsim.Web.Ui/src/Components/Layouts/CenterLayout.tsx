import * as React from "react";

const styles = require("./CenterLayout.less");

export interface CenterLayoutProps {
    style?: React.CSSProperties;
}

export class CenterLayout extends React.Component<CenterLayoutProps> {
    render() {
        return (
            <div style={this.props.style} className={styles.container}>
                {this.props.children}
            </div>
        );
    }
}