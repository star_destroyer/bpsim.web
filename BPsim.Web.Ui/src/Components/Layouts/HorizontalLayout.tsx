import * as React from "react";
import classNames = require("classnames");

const styles = require("./HorizontalLayout.less");

export type ContentSide = "left" | "center" | "right";

export interface HorizontalLayoutProps {
    marginTop: boolean
    side: ContentSide
}

export class HorizontalLayout extends React.Component<HorizontalLayoutProps> {
    static defaultProps: Partial<HorizontalLayoutProps> = {
        marginTop: false,
        side: "center",
    };

    render(): React.ReactNode {
        const className = classNames({
            [styles.container]: true,
            [styles.marginTop]: this.props.marginTop,
            [styles.left]: this.props.side === "left",
            [styles.center]: this.props.side === "center",
            [styles.right]: this.props.side === "right",
        });

        return (
            <div className={className}>
                {this.props.children}
            </div>
        );
    }
}