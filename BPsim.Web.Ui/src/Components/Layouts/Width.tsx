import * as React from "react";

export interface WidthProps {
    minWidth?: React.CSSProperties["minWidth"];
    width?: React.CSSProperties["width"];
}

export class Width extends React.Component<WidthProps> {
    render(): React.ReactNode {
        return (
            <div style={{...this.props}}>
                {this.props.children}
            </div>
        );
    }
}
