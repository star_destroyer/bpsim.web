import * as React from "react";
import {ModelContextValue, withModel} from "../ModelContext";
import {Button, Icon, Spinner, Tooltip} from "@blueprintjs/core";
import {ModelApi} from "../../../Api/ModelApi";

type State = "none" | "saving" | "done";

interface ModelBackgroundSaverState {
    state: State
}

const SAVE_INTERVAL = 10000; // 10s
const SUCCESS_MESSAGE_INTERVAL = 5000; // 5s

class ModelBackgroundSaverInternal extends React.Component<ModelContextValue, ModelBackgroundSaverState> {
    private interval: NodeJS.Timeout;

    state: ModelBackgroundSaverState = {
        state: "none"
    };

    componentDidMount(): void {
        this.registerWork();
    }

    componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    render(): React.ReactNode {
        switch (this.state.state) {
            case "none":
                return <Button icon="floppy-disk" onClick={this.save} minimal/>;
            case "saving":
                return <Spinner size={20}/>;
            case "done":
                return (
                    <Tooltip content="Model saved">
                        <Button icon="tick" intent={"success"} onClick={this.save} minimal/>
                    </Tooltip>
                );
        }
    }

    private registerWork = (): void => {
        this.interval = setInterval(() => {
           this.save();
        }, SAVE_INTERVAL);
    };

    private save = (): void => {
        clearInterval(this.interval);
        this.setState({state: "saving"}, async () => {
            await ModelApi.save(this.props.model.id, this.props.model.data);
            this.setState({state: "done"}, () => {
                setTimeout(() => this.setState({state: "none"}), SUCCESS_MESSAGE_INTERVAL);
                this.registerWork();
            });
        });
    }
}

export const ModelBackgroundSaver = withModel(ModelBackgroundSaverInternal);
