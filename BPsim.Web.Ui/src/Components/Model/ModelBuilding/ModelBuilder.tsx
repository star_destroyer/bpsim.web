import * as React from "react";
import {ModelTree} from "./ModelTree/ModelTree";

export class ModelBuilder extends React.Component {
    render(): React.ReactNode {
        return (
            <ModelTree/>
        );
    }
}
