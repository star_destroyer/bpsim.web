import * as React from "react";
import {ModelContextValue, withModel} from "../ModelContext";
import {Omit} from "../../../Utility/Types";
import {Model} from "../../../Data/Model";
import {Button, Popover} from "@blueprintjs/core";
import Gapped from "../../Layouts/Gapped";
import {removeObjectKey} from "../../../Utility/ObjectHelper";

export interface ModelItemDeletePopoverProps extends ModelContextValue {
    children: React.ReactNode;
    type: keyof Omit<Model, "name">;
    name: string;
}

class ModelItemDeletePopoverInternal extends React.Component<ModelItemDeletePopoverProps> {
    render(): React.ReactNode {
        return (
            <Popover position={"right"} canEscapeKeyClose>
                {this.props.children}
                <div style={{padding: 10}}>
                    <Gapped vertical gap={10}>
                        Are you sure ?
                        <Button icon={"trash"} intent="danger" onClick={this.handleDeleteClick}>Yes</Button>
                    </Gapped>
                </div>
            </Popover>
        );
    }

    private handleDeleteClick = () => {
        const {name, type} = this.props;
        const values = removeObjectKey(this.props.model.data[type], name);
        this.props.changeModel({...this.props.model.data, [type]: values});
    };
}

export const ModelItemDeletePopover = withModel(ModelItemDeletePopoverInternal);
