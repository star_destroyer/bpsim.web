import * as React from "react";
import {Button, HTMLTable} from "@blueprintjs/core";
import {ModelApi} from "../../../Api/ModelApi";
import {ModelSummary} from "../../../Data/Model";
import {HorizontalLayout} from "../../Layouts/HorizontalLayout";
import {Contract} from "../../../Data/Contract";
import {RouteComponentProps} from "react-router";
import {Input} from "../../Input";

export interface ModelListPageState {
    loading: boolean;
    models: Contract<ModelSummary>[]
    input: {
        value: string
    }
}

export class ModelListPage extends React.Component<RouteComponentProps, ModelListPageState> {
    state: ModelListPageState = {
        loading: true,
        models: [],
        input: null
    };

    componentDidMount() {
        this.loadModels();
    }

    render() {
        return (
            <HorizontalLayout marginTop>
                <HTMLTable striped interactive>
                    <thead>
                    <tr>
                        <th style={{width: 700}}>Name</th>
                        <th style={{width: 150}}>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.input ? this.renderInputTr() : this.renderAddTr()}
                    {this.state.models.map((model) => (
                        <tr key={model.id} onClick={() => this.handleModelClicked(model.id)}>
                            <td>
                                {model.data.name}
                            </td>
                            <td>
                                <Button
                                    icon={"trash"}
                                    minimal
                                    onClick={(e: React.SyntheticEvent) => this.handleDeleteClicked(e, model.id)}
                                />
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </HTMLTable>
            </HorizontalLayout>
        );
    }

    private renderAddTr = (): React.ReactNode => {
        return (
            <tr key={"add"}>
                <td colSpan={2}>
                    <Button
                        icon={"add"}
                        text="New model"
                        onClick={this.handleAddClicked}
                        disabled={!!this.state.input}
                        minimal
                    />
                </td>
            </tr>
        );
    };

    private renderInputTr = (): React.ReactNode => {
        return (
            <tr key={"input"}>
                <td>
                    <Input
                        placeholder={"Input model name"}
                        value={this.state.input.value}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.setState({input: {value: e.target.value}})}
                        autoFocus
                        onApply={this.handleCreateClicked}
                        onCancel={() => this.setState({input: null})}
                    />
                </td>
                <td>
                    <Button
                        icon={"add"}
                        minimal
                        onClick={this.handleCreateClicked}
                    />
                    <Button
                        icon={"delete"}
                        minimal
                        onClick={() => this.setState({input: null})}
                    />
                </td>
            </tr>
        );
    };

    private handleModelClicked = (id: string): void => {
        this.props.history.push(`/model/${id}`);
    };

    private handleDeleteClicked = (event: React.SyntheticEvent, id: string): void => {
        event.stopPropagation();
        this.setState({loading: true}, async () => {
            const result = await ModelApi.delete(id);
            this.setState(state => ({loading: false, models: state.models.filter(x => x.id !== id)}));
        });
    };

    private handleAddClicked = (): void => {
        this.setState({
            input: {value: null}
        });
    };

    private handleCreateClicked = async (): Promise<void> => {
        const result = await ModelApi.create(this.state.input.value);
        const content = result.content;
        this.props.history.push(`/model/${content.id}`);
    };

    private loadModels = (): void => {
        this.setState({loading: true}, async () => {
            const result = await ModelApi.select();
            this.setState({loading: false, models: result.content});
        });
    };
}
