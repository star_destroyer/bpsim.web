import * as React from "react";
import {Button, FormGroup, InputGroup} from "@blueprintjs/core";
import {HorizontalLayout} from "../../Layouts/HorizontalLayout";
import Gapped from "../../Layouts/Gapped";
import {ArrayEditForm} from "../../ArrayEditForm";
import {ModelContextValue, withModel} from "../ModelContext";
import {toObject} from "../../../Utility/ArrayHelper";

export interface OrderInfo {
    name: string;
    fields: Array<[string, string]>;
}

export interface ModelOrderEditFormProps extends ModelContextValue {
    info?: OrderInfo;
}

interface ModelOrderEditFormState {
    info: OrderInfo;
}

class ModelOrderEditFormInternal extends React.Component<ModelOrderEditFormProps, ModelOrderEditFormState> {
    state: ModelOrderEditFormState = {
        info: this.props.info || {name: "", fields: []}
    };

    render(): React.ReactNode {
        return (
            <div>
                <FormGroup label={"Name"}>
                    <InputGroup
                        value={this.state.info.name}
                        onChange={this.handleNameChanged}
                        placeholder="Input order name"
                    />
                </FormGroup>
                <ArrayEditForm
                    title="Fields"
                    array={this.state.info.fields}
                    renderItem={this.renderField}
                    onAdd={this.handleAddItem}
                    onDelete={this.handleFieldDelete}
                />
                <HorizontalLayout side={"right"}>
                    <Gapped>
                        <Button
                            icon="floppy-disk"
                            minimal
                            onClick={this.handleSave}
                        >
                            Save
                        </Button>
                        <Button
                            icon="eraser"
                            minimal
                            onClick={this.handleClear}
                        >
                            Clear
                        </Button>
                    </Gapped>
                </HorizontalLayout>
            </div>
        );
    }

    private renderField = ([name, value]: [string, string], i: number): React.ReactNode => (
        <Gapped gap={5} alignItems={"baseline"}>
            <InputGroup
                value={name}
                placeholder="Input key"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.handleFieldChanged(i, e.target.value, value)}
            />
            <InputGroup
                value={value}
                placeholder="Input value"
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.handleFieldChanged(i, name, e.target.value)}
            />
        </Gapped>
    );

    private handleNameChanged = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const name = event.target.value;
        this.setState(state => ({info: {...state.info, name}}));
    };

    private handleAddItem = (): void => {
        this.setState(state => ({
            info: {
                ...state.info,
                fields: [
                    ["", ""],
                    ...this.state.info.fields
                ]
            }
        }));
    };

    private handleFieldChanged = (index: number, key: string, value: string): void => {
        const fields: Array<[string, string]> = this.state.info.fields.map((x, i) => i === index ? [key, value] : x);
        this.setState(state => ({
            info: {...state.info, fields}
        }));
    };

    private handleFieldDelete = (_: unknown, index: number): void => {
        const fields: Array<[string, string]> = this.state.info.fields.filter((_, i) => i !== index);
        this.setState(state => ({
            info: {...state.info, fields}
        }));
    };

    private handleSave = (): void => {
        const {name, fields} = this.state.info;
        const fieldObject = toObject(fields, x => x[0], x => x[1]);

        const orders = {...this.props.model.data.orders, [name]: fieldObject};
        this.props.changeModel({...this.props.model.data, orders});
        this.handleClear();
    };

    private handleClear = (): void => {
        this.setState({info: {name: "", fields: []}});
    };
}

export const ModelOrderEditForm = withModel(ModelOrderEditFormInternal);
