import * as React from "react";
import {ModelOrderEditForm, OrderInfo} from "./ModelOrderEditForm";
import {Popover} from "@blueprintjs/core";
import {OrderFields} from "../../../Data/Model";
import {mapObject} from "../../../Utility/ObjectHelper";

export interface ModelOrderPopoverProps {
    name?: string;
    fields?: OrderFields;
    children: React.ReactNode;
}

export class ModelOrderPopover extends React.Component<ModelOrderPopoverProps> {
    render(): React.ReactNode {
        const {name, fields} = this.props;
        const info: OrderInfo | null = name != null && fields != null
            ? {name, fields: mapObject(fields, (key: string, value) => [key, value])}
            : null;

        return (
            <Popover position={"right"} canEscapeKeyClose minimal>
                {this.props.children}
                <div style={{padding: 15, minWidth: 400}}>
                    <ModelOrderEditForm info={info}/>
                </div>
            </Popover>
        );
    }
}

