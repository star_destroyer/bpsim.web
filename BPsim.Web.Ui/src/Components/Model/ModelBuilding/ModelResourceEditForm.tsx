import * as React from "react";
import {Button, FormGroup, InputGroup, NumericInput} from "@blueprintjs/core";
import {HorizontalLayout} from "../../Layouts/HorizontalLayout";
import Gapped from "../../Layouts/Gapped";
import {ModelContextValue, withModel} from "../ModelContext";
import {ResourceParameters} from "../../../Data/Model";

export interface ResourceInfo {
    name: string;
    values: [string, string, string];
}

export interface ModelResourceEditFormProps extends ModelContextValue {
    info?: ResourceInfo;
}

interface ModelResourceEditFormState {
    info: ResourceInfo;
}

class ModelResourceEditFormInternal extends React.Component<ModelResourceEditFormProps, ModelResourceEditFormState> {
    state: ModelResourceEditFormState = {
        info: this.props.info || ModelResourceEditFormInternal.getEmptyInfo()
    };

    render(): React.ReactNode {
        return (
            <div>
                <FormGroup label={"Name"}>
                    <InputGroup
                        value={this.state.info.name}
                        onChange={this.handleNameChanged}
                        placeholder="Input resource name"
                    />
                </FormGroup>
                <FormGroup label={"Initial value"}>
                    <NumericInput
                        value={this.state.info.values[0]}
                        onValueChange={(n, s) => this.handleValuesChanged(s, 0)}
                    />
                </FormGroup>
                <FormGroup label={"Min value"}>
                    <NumericInput
                        value={this.state.info.values[1]}
                        onValueChange={(n, s) => this.handleValuesChanged(s, 1)}
                    />
                </FormGroup>
                <FormGroup label={"Max value"}>
                    <NumericInput
                        value={this.state.info.values[2]}
                        onValueChange={(n, s) => this.handleValuesChanged(s, 2)}
                    />
                </FormGroup>
                <HorizontalLayout side={"right"}>
                    <Gapped>
                        <Button
                            icon="floppy-disk"
                            minimal
                            onClick={this.handleSaveClicked}
                        >
                            Save
                        </Button>
                        <Button
                            icon="eraser"
                            minimal
                            onClick={this.handleClearClicked}
                        >
                            Clear
                        </Button>
                    </Gapped>
                </HorizontalLayout>
            </div>
        );
    }

    private handleNameChanged = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const name = event.target.value;
        this.setState(state => ({
            info: {...state.info, name}
        }));
    };

    private handleValuesChanged = (value: string, index: 0 | 1 | 2): void => {
        this.setState(state => {
            const {name, values} = state.info;

            const getValues = (): [string, string, string] => {
                switch (index) {
                    case 0:
                        return [value, this.min(values[1], value), this.max(values[2], value)];
                    case 1:
                        return [this.max(values[0], value), value, this.max(values[2], value)];
                    case 2:
                        return [this.min(values[0], value), this.min(values[1], value), value];
                }
            };
            return {info: {name, values: getValues()}};
        });
    };

    private handleSaveClicked = (): void => {
        const {name, values} = this.state.info;
        const resources = {...this.props.model.data.resources, [name]: values};
        this.props.changeModel({...this.props.model.data, resources});
        this.handleClearClicked();
    };

    private handleClearClicked = (): void => {
        this.setState({info: ModelResourceEditFormInternal.getEmptyInfo()});
    };

    private min = (a: string, b: string): string => {
        const aNumber = parseFloat(a);
        const bNumber = parseFloat(b);
        if (!aNumber) {
            return b;
        }

        if (!bNumber) {
            return a;
        }

        return aNumber <= bNumber ? a : b;
    };

    private max = (a: string, b: string): string => {
        const aNumber = parseFloat(a);
        const bNumber = parseFloat(b);
        if (!aNumber) {
            return b;
        }

        if (!bNumber) {
            return a;
        }

        return aNumber >= bNumber ? a : b;
    };

    private static getEmptyInfo = (): ResourceInfo => ({name: "", values: ["", "", ""]});
}

export const ModelResourceEditForm = withModel(ModelResourceEditFormInternal);
