import * as React from "react";
import {ModelResourceEditForm, ResourceInfo} from "./ModelResourceEditForm";
import {Popover} from "@blueprintjs/core";

export interface ModelResourcePopoverProps {
    name?: string;
    parameters?: [string, string, string]
    children: React.ReactNode;
}

export class ModelResourcePopover extends React.Component<ModelResourcePopoverProps> {
    render(): React.ReactNode {
        const {name, parameters} = this.props;

        const info: ResourceInfo | null = name != null && parameters != null
            ? {name, values: parameters}
            : null;

        return (
            <Popover position={"right"} canEscapeKeyClose minimal>
                {this.props.children}
                <div style={{padding: 15}}>
                    <ModelResourceEditForm info={info}/>
                </div>
            </Popover>
        );
    }
}
