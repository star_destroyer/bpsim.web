import * as React from "react";
import {ModelContextValue, withModel} from "../../ModelContext";
import {ITreeNode, Tree} from "@blueprintjs/core";
import {ModelBackgroundSaver} from "../ModelBackgroundSaver";
import {getResourceNode} from "./ResourceTreeNode";
import {getOrdersNode} from "./OrderTreeNode";
import {getNodesNode} from "./NodeTreeNode";
import {removeObjectKey} from "../../../../Utility/ObjectHelper";
import {Model} from "../../../../Data/Model";
import {Omit} from "../../../../Utility/Types";

class ModelTreeInternal extends React.Component<ModelContextValue> {
    render(): React.ReactNode {
        if (this.props.loading) {
            return null;
        }

        return (
            <div style={{width: 500}}>
                <Tree
                    contents={this.getContent()}
                />
            </div>
        );
    }

    private getContent = (): ITreeNode[] => {
        const model = this.props.model.data;

        const children: ITreeNode[] = [
            getResourceNode(model),
            getOrdersNode(model),
            getNodesNode(model)
        ];

        return [
            {
                id: "root",
                label: model.name,
                icon: "application",
                childNodes: children,
                isExpanded: true,
                hasCaret: false,
                secondaryLabel: <ModelBackgroundSaver/>
            }
        ];
    };
}

export const ModelTree = withModel(ModelTreeInternal);
