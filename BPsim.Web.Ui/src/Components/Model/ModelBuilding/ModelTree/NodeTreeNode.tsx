import {isOperation, Model} from "../../../../Data/Model";
import {Button, ITreeNode} from "@blueprintjs/core";
import * as React from "react";
import {ModelNodePopover} from "../NodeForms/ModelNodePopover";
import {mapObject} from "../../../../Utility/ObjectHelper";
import Gapped from "../../../Layouts/Gapped";
import {ModelItemDeletePopover} from "../ModelItemDeletePopover";

const renderNodeAdd = (): JSX.Element => {
    return (
        <ModelNodePopover>
            <Button
                icon="add"
                minimal
            />
        </ModelNodePopover>
    );
};

const renderNodeActions = (name: string) => {
    return (
        <Gapped>
            <ModelNodePopover name={name}>
                <Button icon="edit" minimal/>
            </ModelNodePopover>
            <ModelItemDeletePopover name={name} type={"nodes"}>
                <Button icon={"trash"} minimal/>
            </ModelItemDeletePopover>
        </Gapped>
    );
};

export const getNodesNode = (model: Model): ITreeNode => {
    const nodes = model.nodes;
    const content: ITreeNode[] = mapObject(nodes, (key: string, value) => ({
        id: key,
        label: key,
        icon: isOperation(value) ? "widget" : "person",
        secondaryLabel: renderNodeActions(key)
    }));

    return {
        id: "nodes",
        label: "Nodes",
        icon: "layout-balloon",
        isExpanded: true,
        hasCaret: false,
        childNodes: content,
        secondaryLabel: renderNodeAdd()
    };
};
