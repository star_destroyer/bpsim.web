import {Model, OrderFields} from "../../../../Data/Model";
import {Button, ITreeNode} from "@blueprintjs/core";
import {ModelOrderPopover} from "../ModelOrderPopover";
import * as React from "react";
import Gapped from "../../../Layouts/Gapped";
import {mapObject} from "../../../../Utility/ObjectHelper";
import {ModelItemDeletePopover} from "../ModelItemDeletePopover";

const renderOrderAdd = (): JSX.Element => {
    return (
        <ModelOrderPopover>
            <Button
                icon="add"
                minimal
            />
        </ModelOrderPopover>
    );
};

const renderOrderActions = (name: string, fields: OrderFields) => {
    return (
        <Gapped>
            <ModelOrderPopover name={name} fields={fields}>
                <Button icon="edit" minimal/>
            </ModelOrderPopover>
            <ModelItemDeletePopover name={name} type={"orders"}>
                <Button icon={"trash"} minimal/>
            </ModelItemDeletePopover>
        </Gapped>
    );
};

export const getOrdersNode = (model: Model): ITreeNode => {
    const orders = model.orders;
    const content: ITreeNode[] = mapObject(orders, (key: string, value) => ({
        id: key,
        label: key,
        icon: "document",
        secondaryLabel: renderOrderActions(key, value)

    }));

    return {
        id: "orders",
        label: "Orders",
        icon: "projects",
        isExpanded: true,
        hasCaret: false,
        childNodes: content,
        secondaryLabel: renderOrderAdd()
    };
};
