import {Model, ResourceParameters} from "../../../../Data/Model";
import {Button, ITreeNode} from "@blueprintjs/core";
import * as React from "react";
import {ModelResourcePopover} from "../ModelResourcePopover";
import Gapped from "../../../Layouts/Gapped";
import {mapObject} from "../../../../Utility/ObjectHelper";
import {ModelItemDeletePopover} from "../ModelItemDeletePopover";

const renderResourceAdd = (): JSX.Element => {
    return (
        <ModelResourcePopover>
            <Button
                icon="add"
                minimal
            />
        </ModelResourcePopover>
    );
};

const renderResourceActions = (name: string, parameters: ResourceParameters) => {
    return (
        <Gapped>
            <ModelResourcePopover name={name} parameters={parameters}>
                <Button icon="edit" minimal/>
            </ModelResourcePopover>
            <ModelItemDeletePopover name={name} type={"resources"}>
                <Button icon={"trash"} minimal/>
            </ModelItemDeletePopover>
        </Gapped>
    );
};

export const getResourceNode = (model: Model): ITreeNode => {
    const resources = model.resources;
    const content: ITreeNode[] = mapObject(resources, (key: string, value) => ({
        id: key,
        label: key,
        icon: "cube",
        secondaryLabel: renderResourceActions(key, value)
    }));

    return {
        id: "resources",
        label: "Resources",
        icon: "box",
        isExpanded: true,
        hasCaret: false,
        childNodes: content,
        secondaryLabel: renderResourceAdd()
    };
};
