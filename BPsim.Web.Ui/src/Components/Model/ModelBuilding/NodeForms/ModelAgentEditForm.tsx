import * as React from "react";
import {Knowledge, Rule, RuleType} from "../../../../Data/Model";
import Gapped from "../../../Layouts/Gapped";
import {ArrayEditForm} from "../../../ArrayEditForm";
import {RuleInput} from "../RuleInput";
import {ModelAgentKnowledgeEditForm} from "./ModelAgentKnowledgeEditForm";
import {AgentInfo, getEmptyKnowledgeInfo, KnowledgeInfo} from "./NodeFormTypes";
import {Width} from "../../../Layouts/Width";

export interface ModelAgentEditFormProps {
    agent: AgentInfo;
    onChange: (agent: AgentInfo) => void;
}

export class ModelAgentEditForm extends React.Component<ModelAgentEditFormProps> {
    render(): React.ReactNode {
        return (
            <Gapped gap={10}>
                <Width minWidth={260}>
                    <ArrayEditForm
                        title={"Global rules"}
                        array={this.props.agent.globalRules}
                        renderItem={this.renderGlobalRules}
                        onAdd={this.handleGlobalRuleAdd}
                        onDelete={this.handleGlobalRuleDelete}
                    />
                </Width>
                <Width minWidth={295}>
                    <ArrayEditForm
                        title={"Knowledge"}
                        array={this.props.agent.knowledges}
                        renderItem={this.renderKnowledge}
                        onAdd={this.handleKnowledgeAdd}
                        onDelete={this.handleKnowledgeDelete}
                    />
                </Width>
            </Gapped>
        );
    }

    private renderGlobalRules = (item: Rule, index: number): React.ReactNode => (
        <RuleInput rule={item} onChange={r => this.handleGlobalRuleChange(r, index)}/>
    );

    private renderKnowledge = (item: KnowledgeInfo, index: number): React.ReactNode => (
        <ModelAgentKnowledgeEditForm knowledge={item} onChange={k => this.handleKnowledgeChange(k, index)}/>
    );

    private handleGlobalRuleChange = (rule: Rule, index: number) => {
        const globalRules = this.props.agent.globalRules.map((x, i) => index === i ? rule : x);
        this.props.onChange({...this.props.agent, globalRules});
    };

    private handleGlobalRuleAdd = () => {
        const globalRules: Rule[] = [...this.props.agent.globalRules, {type: RuleType.Expression, expression: ""}];
        this.props.onChange({...this.props.agent, globalRules});
    };

    private handleGlobalRuleDelete = (item: Rule, index: number) => {
        const globalRules = this.props.agent.globalRules.filter((_, i) => i !== index);
        this.props.onChange({...this.props.agent, globalRules});
    };

    private handleKnowledgeAdd = () => {
        const knowledges = [...this.props.agent.knowledges, getEmptyKnowledgeInfo()];
        this.props.onChange({...this.props.agent, knowledges});
    };

    private handleKnowledgeDelete = (item: KnowledgeInfo, index: number) => {
        const knowledges = this.props.agent.knowledges.filter((_, i) => i !== index);
        this.props.onChange({...this.props.agent, knowledges});
    };

    private handleKnowledgeChange = (item: KnowledgeInfo, index: number) => {
        const knowledges = this.props.agent.knowledges.map((x, i) => i === index ? item : x);
        this.props.onChange({...this.props.agent, knowledges});
    };
}
