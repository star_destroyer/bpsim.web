import * as React from "react";
import {ExpressionRule, Rule, RuleType} from "../../../../Data/Model";
import {FormGroup, InputGroup} from "@blueprintjs/core";
import {ArrayEditForm} from "../../../ArrayEditForm";
import {RuleInput} from "../RuleInput";
import {KnowledgeInfo} from "./NodeFormTypes";
import {Width} from "../../../Layouts/Width";

export interface ModelAgentKnowledgeEditFormProps {
    knowledge: KnowledgeInfo;
    onChange: (knowledge: KnowledgeInfo) => void;
}

export class ModelAgentKnowledgeEditForm extends React.Component<ModelAgentKnowledgeEditFormProps> {
    render(): React.ReactNode {
        return (
            <Width minWidth={260}>
                <FormGroup label={"Name"}>
                    <InputGroup
                        value={this.props.knowledge.name}
                        onChange={this.handleNameChange}
                        placeholder={"Input knowledge name"}
                    />
                    <ArrayEditForm
                        title={"Rules"}
                        array={this.props.knowledge.rules}
                        renderItem={this.renderKnowledgeRule}
                        onAdd={this.handleRuleAdd}
                        onDelete={this.handleRuleDelete}
                    />
                </FormGroup>
            </Width>
        );
    }

    private renderKnowledgeRule = (item: Rule, index: number): React.ReactNode => (
        <RuleInput rule={item} onChange={r => this.handleRuleChange(r, index)}/>
    );

    private handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.value;
        this.props.onChange({...this.props.knowledge, name});
    };

    private handleRuleChange = (item: Rule, index: number) => {
        const rules = this.props.knowledge.rules.map((x, i) => i === index ? item : x);
        this.props.onChange({...this.props.knowledge, rules});
    };

    private handleRuleAdd = () => {
        const rules: Rule[] = [...this.props.knowledge.rules, {type: RuleType.Expression, expression: ""}];
        this.props.onChange({...this.props.knowledge, rules});
    };

    private handleRuleDelete = (item: Rule, index: number) => {
        const rules = this.props.knowledge.rules.filter((_, i) => i !== index);
        this.props.onChange({...this.props.knowledge, rules});
    };
}
