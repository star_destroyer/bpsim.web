import * as React from "react";
import {Agent, ImitationNode, isOperation, Operation, Rule} from "../../../../Data/Model";
import {Button, FormGroup, InputGroup, Radio, RadioGroup} from "@blueprintjs/core";
import Gapped from "../../../Layouts/Gapped";
import {HorizontalLayout} from "../../../Layouts/HorizontalLayout";
import {ModelOperationEditForm} from "./ModelOperationEditForm";
import {ModelAgentEditForm} from "./ModelAgentEditForm";
import {ModelContextValue, withModel} from "../../ModelContext";
import {removeObjectKey} from "../../../../Utility/ObjectHelper";
import {
    AgentInfo, getEmptyAgentInfo,
    getEmptyImitationNodeInfo, getEmptyOperationInfo, ImitationNodeData,
    ImitationNodeInfo,
    isOperationInfo,
    OperationInfo,
    toInfo, toNode
} from "./NodeFormTypes";

export interface ModelNodeEditFormProps extends ModelContextValue {
    name?: string;
    node?: ImitationNode
}

interface ModelNodeEditFormState {
    info: ImitationNodeInfo;
}

class ModelNodeEditFormInternal extends React.Component<ModelNodeEditFormProps, ModelNodeEditFormState> {
    state: ModelNodeEditFormState = {
        info: this.props.name != null && this.props.node != null ? toInfo(this.props.name, this.props.node) : getEmptyImitationNodeInfo()
    };

    render(): React.ReactNode {
        const operation = isOperationInfo(this.state.info.data);
        return (
            <Gapped>
                <div>
                    <FormGroup label={"Name"}>
                        <InputGroup
                            value={this.state.info.name}
                            onChange={this.handleNameChange}
                            placeholder="Input node name"
                        />
                    </FormGroup>
                    <RadioGroup
                        label={"Type"}
                        onChange={this.handleTypeChange}
                        selectedValue={operation ? "operation" : "agent"}
                        inline
                    >
                        <Radio value={"operation"} label={"Operation"}/>
                        <Radio value={"agent"} label={"Agent"}/>
                    </RadioGroup>
                    <HorizontalLayout side={"right"}>
                        <Gapped>
                            <Button
                                icon="floppy-disk"
                                minimal
                                onClick={this.handleSaveClicked}
                            >
                                Save
                            </Button>
                            <Button
                                icon="eraser"
                                minimal
                                onClick={this.handleClearClicked}
                            >
                                Clear
                            </Button>
                        </Gapped>
                    </HorizontalLayout>
                </div>
                {operation
                    ? <ModelOperationEditForm
                        operation={this.state.info.data as OperationInfo}
                        onChange={this.handleNodeChanged}
                    />
                    : <ModelAgentEditForm
                        agent={this.state.info.data as AgentInfo}
                        onChange={this.handleNodeChanged}
                    />
                }
            </Gapped>
        );
    }

    private handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const name = event.target.value;
        this.setState(state => ({
            info: {...state.info, name}
        }));
    };

    private handleTypeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const type = event.target.value;
        const data = type === "operation" ? getEmptyOperationInfo() : getEmptyAgentInfo();
        this.handleNodeChanged(data);
    };

    private handleNodeChanged = (data: ImitationNodeData) => {
        this.setState(state => ({
            info: {...state.info, data}
        }));
    };

    private handleSaveClicked = (): void => {
        const [name, node] = toNode(this.state.info);
        const nodes = this.props.name
            ? removeObjectKey(this.props.model.data.nodes, this.props.name)
            : this.props.model.data.nodes;

        const next = {...nodes, [name]: node};
        this.props.changeModel({...this.props.model.data, nodes: next});
        this.handleClearClicked();
    };

    private handleClearClicked = (): void => {
        this.setState({info: getEmptyImitationNodeInfo()});
    };

}

export const ModelNodeEditForm = withModel(ModelNodeEditFormInternal);
