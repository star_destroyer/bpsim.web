import * as React from "react";
import {Popover} from "@blueprintjs/core";
import {ModelNodeEditForm} from "./ModelNodeEditForm";
import {ModelContextValue, withModel} from "../../ModelContext";

export interface ModelNodePopoverProps extends ModelContextValue {
    name?: string
    children: React.ReactNode;
}

class ModelNodePopoverInternal extends React.Component<ModelNodePopoverProps> {
    render(): React.ReactNode {
        const node = this.props.name != null ? this.props.model.data.nodes[this.props.name] : null;

        return (
            <Popover position={"right"} canEscapeKeyClose minimal>
                {this.props.children}
                <div style={{padding: 15}}>
                    <ModelNodeEditForm name={this.props.name} node={node}/>
                </div>
            </Popover>
        );
    }
}

export const ModelNodePopover = withModel(ModelNodePopoverInternal);
