import * as React from "react";
import {Rule} from "../../../../Data/Model";
import {FormGroup, InputGroup} from "@blueprintjs/core";
import {ArrayEditForm} from "../../../ArrayEditForm";
import {RuleInput} from "../RuleInput";
import Gapped from "../../../Layouts/Gapped";
import {OperationInfo} from "./NodeFormTypes";

type Side = "inRules" | "outRules";

export interface ModelOperationEditFormProps {
    operation: OperationInfo;
    onChange: (operation: OperationInfo) => void;
}

export class ModelOperationEditForm extends React.Component<ModelOperationEditFormProps> {
    render(): React.ReactNode {
        return (
            <div>
                <FormGroup label={"Duration"}>
                    <InputGroup
                        value={this.props.operation.duration}
                        onChange={this.handleDurationChanged}
                    />
                    <Gapped gap={10}>
                        <div style={{minWidth: 235}}>
                            <ArrayEditForm
                                title={"In Rules"}
                                array={this.props.operation.inRules}
                                renderItem={(x, i) => this.renderItem("inRules", x, i)}
                                onAdd={() => this.handleAdd("inRules")}
                                onDelete={(_, i) => this.handleRemove("inRules", i)}
                            />
                        </div>
                        <div style={{minWidth: 235}}>
                            <ArrayEditForm
                                title={"Out Rules"}
                                array={this.props.operation.outRules}
                                renderItem={(x, i) => this.renderItem("outRules", x, i)}
                                onAdd={() => this.handleAdd("outRules")}
                                onDelete={(_, i) => this.handleRemove("outRules", i)}
                            />
                        </div>
                    </Gapped>
                </FormGroup>
            </div>
        );
    }

    private renderItem = (side: Side, item: Rule, i: number): React.ReactNode => (
        <RuleInput rule={item} onChange={x => this.handleRuleChange(side, x, i)}/>
    );

    private handleRuleChange = (side: Side, rule: Rule, index: number) => {
        const rules = this.props.operation[side].map((x, i) => i === index ? rule : x);
        this.props.onChange({...this.props.operation, [side]: rules});
    };

    private handleAdd = (side: Side) => {
        const rules = [...this.props.operation[side], {type: "ExpressionRule", expression: ""}];
        this.props.onChange({...this.props.operation, [side]: rules});
    };

    private handleRemove = (side: Side, index: number) => {
        const rules = this.props.operation[side].filter((_, i) => i !== index);
        this.props.onChange({...this.props.operation, [side]: rules});
    };

    private handleDurationChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
        const duration = event.target.value;
        this.props.onChange({...this.props.operation, duration});
    };
}
