import {Agent, ImitationNode, isOperation, Operation, Rule} from "../../../../Data/Model";
import {mapObject} from "../../../../Utility/ObjectHelper";
import {toObject} from "../../../../Utility/ArrayHelper";

export interface KnowledgeInfo {
    name: string;
    rules: Rule[];
}

export interface AgentInfo {
    globalRules: Rule[];
    knowledges: KnowledgeInfo[];
}

export interface OperationInfo extends Operation {

}

export type ImitationNodeData = OperationInfo | AgentInfo;

export interface ImitationNodeInfo {
    name: string;
    data: ImitationNodeData;
}

export function isOperationInfo(data: ImitationNodeData): data is OperationInfo {
    return data.hasOwnProperty("duration");
}

export const toInfo = (name: string, node: ImitationNode): ImitationNodeInfo => {
    if (isOperation(node)) {
        return {name, data: node};
    } else {
        const knowledges: KnowledgeInfo[] = mapObject(node.knowledges, (name: string, value) => ({
            name,
            rules: value.rules
        }));
        return {
            name,
            data: {
                globalRules: node.globalRules,
                knowledges
            }
        };
    }
};

export const toNode = (info: ImitationNodeInfo): [string, ImitationNode] => {
    if (isOperationInfo(info.data)) {
        return [info.name, info.data];
    } else {
        const agent: Agent = {
            globalRules: info.data.globalRules,
            knowledges: toObject(info.data.knowledges, x => x.name, x => ({rules: x.rules}))
        };
        return [info.name, agent];
    }
};

export const getEmptyKnowledgeInfo = (): KnowledgeInfo => ({name: "", rules: []});
export const getEmptyAgentInfo = (): AgentInfo => ({globalRules: [], knowledges: []});
export const getEmptyOperationInfo = (): OperationInfo => ({duration: "0", inRules: [], outRules: []});
export const getEmptyImitationNodeInfo = (): ImitationNodeInfo => ({name: "", data: getEmptyOperationInfo()});

