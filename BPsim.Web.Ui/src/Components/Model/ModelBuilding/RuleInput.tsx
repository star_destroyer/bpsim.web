import * as React from "react";
import {Rule, RuleType} from "../../../Data/Model";
import {ModelContextValue, withModel} from "../ModelContext";
import {IconName, InputGroup} from "@blueprintjs/core";

const createOrderRulePattern = /^#create\("(?<name>[\w\s]+)"\)$/g;
const removeOrderRulePattern = /^#remove\("(?<name>[\w\s]+)"\)$/g;
const selectOrderRulePattern = /^#select\("(?<name>[\w\s]+)":"(?<target>[\w\s]+)"\)$/g;
const sendOrderRulePattern = /^#send\("(?<name>[\w\s]+)":"(?<target>[\w\s]+)"\)$/g;

export interface RuleInputProps extends ModelContextValue {
    rule: Rule;
    onChange: (rule: Rule) => void;
}

class RuleInputInternal extends React.Component<RuleInputProps> {
    render(): React.ReactNode {
        const rule = RuleInputInternal.stringifyRule(this.props.rule);
        return (
            <InputGroup
                leftIcon={this.renderIcon()}
                value={rule}
                onChange={this.handleInputChange}
                placeholder={"Input rule"}
            />
        );
    }

    private renderIcon = (): IconName => {
        switch (this.props.rule.type) {
            case RuleType.Expression:
                return "variable";
            case RuleType.CreateOrder:
                return "document-open";
            case RuleType.RemoveOrder:
                return "document-share";
            case RuleType.SelectOrder:
                return "saved";
            case RuleType.SendOrder:
                return "flows";

        }
    };

    private handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const input = e.target.value;
        const rule = RuleInputInternal.parseRule(input);
        this.props.onChange(rule);
    };

    private static stringifyRule = (rule: Rule | null): string => {
        switch (rule.type) {
            case RuleType.Expression:
                return rule.expression;
            case RuleType.CreateOrder:
                return `#create("${rule.orderName}")`;
            case RuleType.RemoveOrder:
                return `#remove("${rule.orderName}")`;
            case RuleType.SelectOrder:
                return `#select("${rule.orderName}":"${rule.targetName}")`;
            case RuleType.SendOrder:
                return `#send("${rule.orderName}":"${rule.targetName}")`;
        }
    };

    private static parseRule = (input: string): Rule => {
        let match = createOrderRulePattern.exec(input);
        if (match) {
            const orderName = match.groups["name"];
            return {type: RuleType.CreateOrder, orderName};
        }
        match = removeOrderRulePattern.exec(input);
        if (match) {
            const orderName = match.groups["name"];
            return {type: RuleType.RemoveOrder, orderName};
        }
        match = selectOrderRulePattern.exec(input);
        if (match) {
            const orderName = match.groups["name"];
            const targetName = match.groups["target"];
            return {type: RuleType.SelectOrder, orderName, targetName};
        }
        match = sendOrderRulePattern.exec(input);
        if (match) {
            const orderName = match.groups["name"];
            const targetName = match.groups["target"];
            return {type: RuleType.SendOrder, orderName, targetName};
        }

        return {type: RuleType.Expression, expression: input};
    };
}

export const RuleInput = withModel(RuleInputInternal);
