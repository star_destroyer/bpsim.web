import * as React from "react";
import {Model} from "../../Data/Model";
import {ModelApi} from "../../Api/ModelApi";
import {Contract} from "../../Data/Contract";
import {Omit} from "../../Utility/Types";

export interface ModelContextValue {
    loading: boolean;
    model: Contract<Model> | null;
    loadModel: (id: string) => void;
    changeModel: (model: Model) => void;
}

const ModelContext = React.createContext<ModelContextValue>({
    loading: false,
    model: null,
    loadModel: () => undefined,
    changeModel: () => undefined
});

interface ModelContextProviderState {
    loading: boolean;
    model: Contract<Model> | null;
}

export class ModelContextProvider extends React.Component<{}, ModelContextProviderState> {
    state: ModelContextProviderState = {
        loading: true,
        model: null
    };

    render(): React.ReactNode {
        return (
            <ModelContext.Provider value={{...this.state, changeModel: this.handleChanged, loadModel: this.handleLoad}}>
                {this.props.children}
            </ModelContext.Provider>
        );
    }

    private handleLoad = (id: string): void => {
        this.loadData(id);
    };

    private handleChanged = (model: Model): void => {
        this.setState(state => ({model: {...state.model, data: model}}));
    };

    private loadData = (id: string): void => {
        this.setState({loading: true}, async () => {
            const result = await ModelApi.get(id);
            this.setState({loading: false, model: result.content});
        });
    };
}

export const withModel = <TProp extends ModelContextValue>(component: React.ComponentType<TProp>) => {
    return React.forwardRef((props: Omit<TProp, keyof ModelContextValue>, ref) => {
        return (
            <ModelContext.Consumer>
                {(value: ModelContextValue) => React.createElement(component, {...props as any, ...value, ref})}
            </ModelContext.Consumer>
        );
    });
};
