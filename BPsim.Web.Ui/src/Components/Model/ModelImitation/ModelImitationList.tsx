import * as React from "react";
import {ModelContextValue, withModel} from "../ModelContext";
import {TaskState} from "../../../Data/Task";
import {TaskApi} from "../../../Api/TaskApi";
import {HorizontalLayout} from "../../Layouts/HorizontalLayout";
import {Button, HTMLTable, Icon, Spinner} from "@blueprintjs/core";
import Gapped from "../../Layouts/Gapped";
import {StartTaskDialog} from "./StartTaskDialog";

const StateForPolling: TaskState[] = [
    TaskState.Open,
    TaskState.Pending,
    TaskState.Scheduled
];

interface TaskInfo {
    id: string;
    state: TaskState;
}

export interface ModelImitationListProps extends ModelContextValue {
}

export interface ModelImitationListState {
    loading: boolean;
    tasks: TaskInfo[];
    modal: boolean;
}

class ModelImitationListInternal extends React.Component<ModelImitationListProps, ModelImitationListState> {
    private interval: NodeJS.Timeout = null;

    state: ModelImitationListState = {
        loading: false,
        tasks: [],
        modal: false
    };

    componentDidMount(): void {
        this.registerPolling();
        if (this.props.model) {
            this.loadTasks();
        }
    }

    componentDidUpdate(prevProps: Readonly<ModelImitationListProps>): void {
        if (this.props.model && !prevProps.model) {
            this.loadTasks();
        }
    }

    componentWillUnmount(): void {
        clearInterval(this.interval);
    }

    render(): React.ReactNode {
        if (this.props.loading || this.state.loading) {
            return null;
        }

        return (
            <HorizontalLayout marginTop>
                <StartTaskDialog
                    open={this.state.modal}
                    onStart={this.handleStart}
                    onClose={() => this.setState({modal: false})}
                />
                <Gapped gap={5} vertical>
                    <Button icon={"play"} onClick={() => this.setState({modal: true})}>Start</Button>
                    <HTMLTable striped>
                        <thead>
                        <tr>
                            <th style={{width: 700}}>Name</th>
                            <th style={{width: 50}}>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.tasks.map((x, i) => (
                            <tr key={x.id}>
                                <td>
                                    {x.state === TaskState.Successfull
                                        ? (
                                            <a href={`/analysis/${x.id}.xlsx`}>
                                                {x.id}
                                            </a>
                                        ) : x.id
                                    }
                                </td>
                                <td>
                                    {this.renderStatue(x.state)}
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </HTMLTable>
                </Gapped>
            </HorizontalLayout>
        );
    }

    private renderStatue = (status: TaskState): React.ReactNode => {
        switch (status) {
            case TaskState.Successfull:
                return <Icon icon={"tick-circle"} intent={"success"}/>;
            case TaskState.Failed:
                return <Icon icon={"delete"} intent={"danger"}/>;
            default:
                return <Spinner size={16}/>;
        }
    };

    private handleStart = (id: string): void => {
        this.setState({modal: false}, async () => {
            const result = await TaskApi.get(id);
            if (!result.success) {
                throw new Error(result.message);
            }
            const task = result.content;
            this.setState(state => ({
                tasks: [{
                    id: task.id,
                    state: task.data.state
                }, ...state.tasks]
            }));
        });
    };

    private registerPolling = () => {
        this.interval = setInterval(async () => {
            if (this.props.loading || this.state.loading) {
                return;
            }

            const reloadTasks = this.state.tasks.filter(x => StateForPolling.includes(x.state));

            if (reloadTasks.length === 0) {
                return;
            }

            const requests = reloadTasks.map(async x => await TaskApi.get(x.id));
            Promise.all(requests).then(results => this.setState(state => {
                const tasks = state.tasks.map(x => {
                    const task = results.find(y => y.content.id == x.id);
                    return task ? {id: task.content.id, state: task.content.data.state} : x;
                });
                return {tasks};
            }));
        }, 500);
    };

    private loadTasks = (): void => {
        this.setState({loading: true}, async () => {
            const result = await TaskApi.select(this.props.model.id);
            if (!result.success) {
                throw new Error(result.message);
            }

            const tasks = result.content.map(x => ({id: x.id, state: x.data.state, loading: false}));
            this.setState({tasks: tasks, loading: false});
        });
    };
}

export const ModelImitationList = withModel(ModelImitationListInternal);
