import * as React from "react";
import {TaskConfiguration} from "../../../Data/Task";
import {Button, Dialog, FormGroup} from "@blueprintjs/core";
import {RuleInput} from "../ModelBuilding/RuleInput";
import {ExpressionRule, RuleType} from "../../../Data/Model";
import Gapped from "../../Layouts/Gapped";
import {HorizontalLayout} from "../../Layouts/HorizontalLayout";
import {TaskApi} from "../../../Api/TaskApi";
import {ModelContextValue, withModel} from "../ModelContext";

export interface StartTaskDialogProps extends ModelContextValue {
    open: boolean;
    onStart: (id: string) => void;
    onClose: () => void;
}

interface StartTaskDialogState {
    configuration: TaskConfiguration;
}

class StartTaskDialogInternal extends React.Component<StartTaskDialogProps, StartTaskDialogState> {
    state: StartTaskDialogState = {
        configuration: {endingRule: ""}
    };

    render(): React.ReactNode {
        return (
            <Dialog isOpen={this.props.open}>
                <div style={{margin: 10}}>
                    <Gapped vertical gap={10}>
                        <FormGroup label={"End Rule"}>
                            <RuleInput
                                rule={{type: RuleType.Expression, expression: this.state.configuration.endingRule}}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <HorizontalLayout side={"right"}>
                            <Gapped>
                                <Button onClick={this.handleStart}>Start</Button>
                                <Button onClick={this.props.onClose}>Close</Button>
                            </Gapped>
                        </HorizontalLayout>
                    </Gapped>
                </div>
            </Dialog>
        );
    }

    private handleStart = async () => {
        const result = await TaskApi.start(this.props.model.id, {configuration: this.state.configuration});
        if (result.success) {
            this.props.onStart(result.content);
        }
    };

    private handleChange = (rule: ExpressionRule): void => {
        this.setState({configuration: {endingRule: rule.expression}});
    };
}

export const StartTaskDialog = withModel(StartTaskDialogInternal);
