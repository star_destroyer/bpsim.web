import * as React from "react";
import {RouteComponentProps, Switch} from "react-router";
import {ModelTree} from "./ModelBuilding/ModelTree/ModelTree";
import {ModelContextValue, withModel} from "./ModelContext";
import {ProtectedRoute} from "../ProtectedRoute";
import {ModelBuilder} from "./ModelBuilding/ModelBuilder";
import {ModelImitationList} from "./ModelImitation/ModelImitationList";

interface RouterParameters {
    id: string;
}

export interface ModelPageProps extends RouteComponentProps<RouterParameters>, ModelContextValue {
}

class ModelPageInternal extends React.Component<ModelPageProps> {

    componentDidMount(): void {
        const id = this.props.match.params.id;
        const model = this.props.model;
        if (!id || !model || id !== model.id) {
            this.props.loadModel(id);
        }
    }

    render(): React.ReactNode {
        return (
            <Switch>
                <ProtectedRoute path={"/model/:id/imitation"} exact>
                    <ModelImitationList/>
                </ProtectedRoute>
                <ProtectedRoute path={"/model/:id"} exact>
                    <ModelBuilder/>
                </ProtectedRoute>
            </Switch>
        )
    }
}

export const ModelPage = withModel(ModelPageInternal);
