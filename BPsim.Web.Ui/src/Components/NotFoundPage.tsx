import * as React from "react";

const NotFoundPage: React.SFC = () => {
    return (
        <div>По этому пути пока ничего нет</div>
    );
};

export default NotFoundPage;
