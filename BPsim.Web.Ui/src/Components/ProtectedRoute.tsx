import * as React from "react";
import {Redirect, Route, RouteProps} from "react-router";
import {UserContextValue, withUser} from "./Authorization/UserContext";

interface ProtectedRouteInternalProps extends UserContextValue, RouteProps {
}

class ProtectedRouteInternal extends React.Component<ProtectedRouteInternalProps> {
    render(): React.ReactNode {
        if (this.props.loading) {
            return null;
        }

        return this.props.user
            ? <Route {...this.props}/>
            : <Redirect to={"/auth"}/>;
    }
}

export const ProtectedRoute = withUser(ProtectedRouteInternal);