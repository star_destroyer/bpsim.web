export interface Contract<TData> {
    id: string;

    revision: number;

    timestamp: Date;

    data: TData;
}