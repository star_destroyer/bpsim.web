export interface ModelSummary {
    name: string;
}

export enum RuleType {
    Expression,
    CreateOrder,
    RemoveOrder,
    SelectOrder,
    SendOrder
}

export interface ExpressionRule {
    type: RuleType.Expression;
    expression: string
}

export interface CreateOrderRule {
    type: RuleType.CreateOrder;
    orderName: string;
}

export interface RemoveOrderRule {
    type: RuleType.RemoveOrder;
    orderName: string;
}

export interface SelectOrderRule {
    type: RuleType.SelectOrder;
    orderName: string;
    targetName: string;
}

export interface SendOrderRule {
    type: RuleType.SendOrder;
    orderName: string;
    targetName: string;
}

export type Rule = ExpressionRule | CreateOrderRule | RemoveOrderRule | SelectOrderRule | SendOrderRule;

export interface Operation {
    inRules: Rule[];
    outRules: Rule[];
    duration: string;
}

export interface Knowledge {
    rules: Rule[];
}
export interface Knowledges {
    [name: string]: Knowledge;
}

export interface Agent {
    globalRules: Rule[];
    knowledges: Knowledges;
}

export type ImitationNode = Operation | Agent;

export interface Nodes {
    [name: string]: ImitationNode;
}

export function isOperation(node: ImitationNode): node is Operation {
    return node.hasOwnProperty("duration");
}

export type ResourceParameters = [string, string, string];

export interface Resources {
    [name: string]: ResourceParameters
}

export interface OrderFields {
    [name: string]: string;
}

export interface Orders {
    [name: string]: OrderFields;
}

export interface Model {
    name: string;
    resources: Resources;
    orders: Orders;
    nodes: Nodes;
}
