export interface TaskConfiguration {
    endingRule: string;
}

export interface CreateTaskParameters {
    configuration: TaskConfiguration;
}

export enum TaskState {
    Open = 0,
    Scheduled = 1,
    Pending = 2,
    Failed = 3,
    Successfull = 4,
    Aborted = 5,
    Deleted = 6
}

export interface ImitationTask {
    state: TaskState;
    configuration: TaskConfiguration;
    modelId: string;
}
