export const toObject = <TItem, TValue = TItem>(source: TItem[], keySelector: (item: TItem) => string | number, valueSelector?: (item: TItem) => TValue): { [key: string]: TValue } => {
    const result: { [key: string]: TValue } = {};
    for (let item of source) {
        const key = keySelector(item);
        if (result[key] !== undefined) {
            throw new Error(`Duplicate key: ${key}`);
        }
        result[key] = valueSelector ? valueSelector(item) : item as any;
    }
    return result;
};
