export function mapObject<TSource, TKey extends keyof TSource, T>(
    source: TSource,
    callback: (key: TKey, value: TSource[TKey], index: number) => T
): T[] {
    return Object.keys(source).map((key: any, index: number) => callback(key, source[key as TKey], index));
}

export function removeObjectKey<TSource, TKey extends keyof TSource>(source: TSource, key: TKey): TSource {
    const next = {...source};
    delete next[key];
    return next;
}
