declare module "react-digraph" {
    import * as React from "react";
    import {CSSProperties} from "react";

    export interface ElementTypes {
        [type: string]: ElementType;
    }

    export interface ElementType {
        shapeId: string;
        typeText?: string;
        shape: React.ReactElement<any>;
    }

    export interface GraphNode<TId> {
        id: TId;
        title?: string;
        x: number;
        y: number;
        type: string;
    }

    export interface GraphEdge<TId> {
        id: TId;
        source: string;
        target: string;
        type: string;
    }

    export interface GraphViewProps<TId> {
        nodeKey: string;
        emptyType: string;
        nodes: GraphNode<TId>[];
        edges: GraphEdge<TId>[];
        selected: GraphNode<TId> | GraphEdge<TId> | {};
        nodeTypes: { [type: string]: ElementType };
        nodeSubtypes: { [type: string]: ElementType };
        edgeTypes: { [type: string]: ElementType };
        getViewNode: (id: TId) => GraphNode<TId>;
        onSelectNode: (node: GraphNode<TId> | null) => void;
        onCreateNode: (x: number, y: number) => void;
        onUpdateNode: (node: GraphNode<TId>) => void;
        onDeleteNode: (node: GraphNode<TId>) => void;
        onSelectEdge: (edge: GraphEdge<TId>) => void;
        onCreateEdge: (source: GraphNode<TId>, target: GraphNode<TId>) => void;
        onSwapEdge: (source: GraphNode<TId>, target: GraphNode<TId>, edge: GraphEdge<TId>) => void;
        onDeleteEdge: (edge: GraphEdge<TId>) => void;
        canDeleteNode?: (node: GraphNode<TId>) => boolean;
        canCreateEdge?: (source: GraphNode<TId>, target: GraphNode<TId>) => boolean;
        canDeleteEdge?: (edge: GraphEdge<TId>) => boolean;
        renderEdge?: (graph: GraphView<TId>, g: SVGGElement, edge: GraphEdge<TId>, index: number) => void;
        renderNode?: (graph: GraphView<TId>, g: SVGGElement, edge: GraphNode<TId>, index: number) => void;
        renderDefs?: (svg: GraphView<TId>) => React.ReactNode;
        renderBackground?: (graph: GraphView<TId>) => SVGRectElement;
        readOnly?: boolean;
        enableFocus?: boolean;
        maxTitleChars?: number;
        transitionTime?: number;
        primary?: string;
        light?: string;
        dark?: string;
        background?: string;
        style?: CSSProperties;
        gridSize?: number;
        gridSpacing?: number;
        gridDot?: number;
        minZoom?: number;
        maxZoom?: number;
        nodeSize?: number;
        edgeHandleSize?: number;
        edgeArrowSize?: number;
        zoomDelay?: number;
        zoomDur?: number;
        graphControls?: boolean;
    }

    export default class GraphView<TId> extends React.Component<GraphViewProps<TId>> {
        public getNodeStyle(node: GraphNode<TId>, selected: GraphNode<TId> | GraphEdge<TId> | {}): string;

        public handleZoomToFit(): void;

        public getNodeTransformation: string;
    }
}