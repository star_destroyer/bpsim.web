const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const DotNetEntryAssetsFilePlugin = require("./dotNetEntryAssetsFilePlugin");

const configType = process.env.NODE_ENV || "development";
const isDevelopmentConfig = configType === "development";
const isDevServer = process.argv[1].indexOf("webpack-dev-server") >= 0;

const bundleFolderPath = path.resolve(__dirname, `../BPsim.Web.Front.API/wwwroot/`);
const devServerPort = 9999;
const publicPath = isDevServer ? `http://localhost:${devServerPort}/` : "/";

console.log(`Configuration type = ${configType}`);

module.exports = {
    entry: {
        "BPsim.Web": "./src/index.tsx"
    },
    output: {
        filename: "bundle.js",
        path: bundleFolderPath
    },

    devtool: isDevelopmentConfig ? "inline-source-map" : false,

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json", ".css"],
        modules: [
            'node_modules'
        ]
    },

    module: {
        rules: [
            {
                test: /\.less$/,
                use: [
                    "style-loader",
                    {loader: "css-loader", options: {modules: "global"}},
                    "less-loader"
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg|ttf|woff|woff2|eot)$/i,
                loader: 'url-loader'
            },
            {
                test: /\.(ts|tsx)$/,
                loader: "ts-loader"
            },
            {
                test: /\.css$/,
                loader: "css-loader",
                options: {
                    modules: true
                }
            }
        ]
    },

    mode: configType,

    devServer: {
        "port": devServerPort,
        historyApiFallback: true,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new DotNetEntryAssetsFilePlugin(bundleFolderPath, "webpack-assets.json", publicPath)
    ]
};