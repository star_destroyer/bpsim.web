Set-Location .\BPsim.Web.Imitation.Service
.\start.ps1
Set-Location ..

Set-Location .\BPsim.Web.Auth
.\start.ps1
Set-Location ..

Set-Location .\BPsim.Web.Analysis.Service
.\start.ps1
Set-Location ..

Set-Location .\BPsim.Web.Front.API
.\start.ps1
Set-Location ..